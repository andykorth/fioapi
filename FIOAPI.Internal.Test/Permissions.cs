﻿using FIOAPI.Utils;

namespace FIOAPI.Internal.Test
{
    public class Permissions
    {
        [Fact]
        public void VerifyPermissionCounts()
        {
            // Grab the number of bools on the DB model
            var dbPermission = new DB.Model.Permission();
            var numDBPermissions = dbPermission
                .GetType()
                .GetProperties()
                .Where(p => p.PropertyType == typeof(bool))
                .Count();

            // Grab the number of bools in the permissions payload
            int numPermissionsPayload = 0;
            var permissionsPayload = new Payloads.Permission.Permissions();

            // Check top-level bools (there really shouldn't be any, but just in case)
            numPermissionsPayload += permissionsPayload
                .GetType()
                .GetProperties()
                .Where(p => p.PropertyType == typeof(bool))
                .Count();

            // Check sub-object bools
            var subObjects = permissionsPayload
                .GetType()
                .GetProperties()
                .ToList();
            foreach (var subObject in subObjects)
            {
                if (!subObject.PropertyType.IsPrimitive)
                {
                    numPermissionsPayload += subObject
                        .GetType()
                        .GetProperties()
                        .Where(p => p.PropertyType == typeof(bool))
                        .Count();
                }
            }

            Assert.Equal(numDBPermissions, numPermissionsPayload);

            // Now check Perm enum (-1 for none)
            var permCount = Enum.GetNames(typeof(Perm)).Count() - 1;
            Assert.Equal(permCount, numDBPermissions);
        }
    }
}
