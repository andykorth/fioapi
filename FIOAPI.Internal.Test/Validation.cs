using System.ComponentModel.DataAnnotations;

using FIOAPI.Attributes;
using FIOAPI.DB.Model;
using FIOAPI.Extensions;

namespace FIOAPI.Internal.Test
{
    public class Validation
    {
        class StringValidateTest : IValidation
        {
            [StringLength(4, MinimumLength = 2)]
            public string Value { get; set; } = "";
        }

        [Fact]
        public void StringValidation()
        {
            var test = new StringValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = "AB";
            Assert.True(test.PassesValidation());

            test.Value = "ABAB";
            Assert.True(test.PassesValidation());

            test.Value = "ABABA";
            Assert.False(test.PassesValidation());
        }

        class GuidValidateTest : IValidation
        {
            [GuidValid]
            public Guid Value { get; set; } = Guid.Empty;
        }

        [Fact]
        public void GuidValidate()
        {
            var test = new GuidValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = Guid.NewGuid();
            Assert.True(test.PassesValidation());
        }

        class RangeValidateTest : IValidation
        {
            [Range(1, 2)]
            public int Value { get; set; } = 0;
        }

        [Fact]
        public void RangeValidate()
        {
            var test = new RangeValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = 1;
            Assert.True(test.PassesValidation());

            test.Value = 2;
            Assert.True(test.PassesValidation());

            test.Value = 3;
            Assert.False(test.PassesValidation());
        }

        class ListValidateTest : IValidation
        {
            [NotEmpty]
            public List<string> Value { get; set; } = new List<string>();
        }

        [Fact]
        public void ListValidate()
        {
            var test = new ListValidateTest();

            Assert.False(test.PassesValidation());

            test.Value.Add("Foo");
            Assert.True(test.PassesValidation());

            test.Value.Clear();
            Assert.False(test.PassesValidation());
        }

        class RequiredValidateTest : IValidation
        {
            public List<string> Value { get; set; } = null!;
        }

        [Fact]
        public void RequiredValidate()
        {
            var test = new RequiredValidateTest();

            Assert.False(test.PassesValidation());

            test.Value = new List<string>();
            Assert.True(test.PassesValidation());
        }

        class CustomValidationTest : IValidation
        {
            public int Value { get; set; }

            public override void CustomValidation(ref List<string> Errors, string Context)
            {
                base.CustomValidation(ref Errors, Context);
                if (Value > 5 && Value < 10)
                {
                    Errors.Add("Value is > 5 and < 10");
                }
            }
        }

        [Fact]
        public void CustomValidate()
        {
            var test = new CustomValidationTest();
            Assert.True(test.PassesValidation());

            test.Value = 5;
            Assert.True(test.PassesValidation());

            test.Value = 9;
            Assert.False(test.PassesValidation());
        }

        class RequiredAndNotEmptyTest : IValidation
        {
            [NotEmpty]
            public List<string> Value { get; set; } = null!;
        }

        [Fact]
        public void RequiredAndNotEmptyValidate()
        {
            var test = new RequiredAndNotEmptyTest();
            Assert.False(test.PassesValidation());

            test.Value = new List<string>();
            Assert.False(test.PassesValidation());

            test.Value.Add("A");
            Assert.True(test.PassesValidation());
        }

        class ComplexTest : IValidation
        {
            [NotEmpty]
            public List<RequiredAndNotEmptyTest>? Test1 { get; set; }

            public CustomValidationTest Test2 { get; set; } = new CustomValidationTest();

            public RequiredValidateTest? Test3 { get; set; } = null;
        }

        [Fact]
        public void ComplexValidate()
        {
            var test = new ComplexTest();

            test.Test1 = new List<RequiredAndNotEmptyTest>
            {
                new RequiredAndNotEmptyTest()
            };
            test.Test1[0].Value = new();
            test.Test1[0].Value!.Add("A");

            test.Test3 = new RequiredValidateTest();
            test.Test3.Value = new();
            Assert.True(test.PassesValidation());

            test.Test3.Value = null!;
            Assert.False(test.PassesValidation());

            test.Test3.Value = new();
            test.Test2.Value = 6;
            Assert.False(test.PassesValidation());

            test.Test2.Value = 0;
            test.Test1[0].Value!.Clear();
            Assert.False(test.PassesValidation());

            test.Test1[0].Value = null!;
            Assert.False(test.PassesValidation());
        }

        class RecursiveInnerTestClass : IValidation
        {
            [StringLength(10, MinimumLength = 5)]
            public string test { get; set; } = null!;

            public RecursiveTestClass Parent { get; set; } = null!;
        }

        class RecursiveTestClass : IValidation
        {
            public RecursiveInnerTestClass Inner { get; set; } = null!;
        }

        [Fact]
        public void RecursiveTest()
        {
            var test = new RecursiveTestClass();
            var inner = new RecursiveInnerTestClass()
            {
                test = "FAIL",
                Parent = test
            };
            test.Inner = inner;

            Assert.False(test.PassesValidation());

            test.Inner.test = "PASSES";
            Assert.True(test.PassesValidation());
        }

        class APEXIDTestClass : IValidation
        {
            [APEXID]
            public string? test { get; set; }
        }

        [Fact]
        public void APEXIDTest()
        {
            var test = new APEXIDTestClass();

            // null should succeed since it's not required
            test.test = null;
            Assert.True(test.PassesValidation());

            // Test not 32 characters
            test.test = new String('0', 33);
            Assert.False(test.PassesValidation());
            test.test = new String('0', 31);
            Assert.False(test.PassesValidation());

            // Test not uppercase
            test.test = new String('F', 32);
            Assert.False(test.PassesValidation());

            // Test invalid character
            test.test = new String('f', 31) + "g";
            Assert.False(test.PassesValidation());

            test.test = "0123456789abcdef0123456789abcdef";
            Assert.True(test.PassesValidation());
        }

        class NaturalIdTestClass : IValidation
        {
            [NaturalID]
            public string? naturalId { get; set; }
        }

        [Fact]
        public void NaturalIdTest()
        {
            var test = new NaturalIdTestClass();

            // null should succeed since it's not required
            test.naturalId = null;
            Assert.True(test.PassesValidation());

            test.naturalId = "1";
            Assert.False(test.PassesValidation());

            test.naturalId = "123";
            Assert.False(test.PassesValidation());

            test.naturalId = "HRT-123";
            Assert.False(test.PassesValidation());

            test.naturalId = "UV-123A";
            Assert.False(test.PassesValidation());

            test.naturalId = "ABC";
            Assert.True(test.PassesValidation());

            test.naturalId = "UV-123";
            Assert.True(test.PassesValidation());

            test.naturalId = "UV-123z";
            Assert.True(test.PassesValidation());
        }

        class TickerTestClass : IValidation
        {
            [Ticker]
            public string? test { get; set; }
        }

        [Fact]
        public void TickerTest()
        {
            var test = new TickerTestClass();

            // null should succeed since it's not required
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string should fail
            test.test = "";
            Assert.False(test.PassesValidation());

            // Lowercase anything should fail
            test.test = "rat";
            Assert.False(test.PassesValidation());
            test.test = "w";
            Assert.False(test.PassesValidation());
            test.test = "cogc";
            Assert.False(test.PassesValidation());

            // Anything more than 4 characters should fail
            test.test = "LMATCX";
            Assert.False(test.PassesValidation());

            // Anything between 1 and 4 characters uppercase should succeed
            test.test = "W";
            Assert.True(test.PassesValidation());
            test.test = "DW";
            Assert.True(test.PassesValidation());
            test.test = "RAT";
            Assert.True(test.PassesValidation());
            test.test = "LOCM";
            Assert.True(test.PassesValidation());
        }

        public class LowercaseTestClass : IValidation
        {
            [Lowercase]
            public string? test { get; set; }
        }

        [Fact]
        public void LowercaseTest()
        {
            var test = new LowercaseTestClass(); 

            // Null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is fine
            test.test = "";
            Assert.True(test.PassesValidation());

            test.test = "Hello";
            Assert.False(test.PassesValidation());

            test.test = "hello";
            Assert.True(test.PassesValidation());
        }

        public class UppercaseTestClass : IValidation
        {
            [Uppercase]
            public string? test { get; set; }
        }

        [Fact]
        public void UppercaseTest() 
        {
            var test = new UppercaseTestClass();

            // Null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is fine
            test.test = "";
            Assert.True(test.PassesValidation());

            test.test = "hELLO";
            Assert.False(test.PassesValidation());

            test.test = "HELLO";
            Assert.True(test.PassesValidation());
        }

        public class CurrencyCodeTestClass : IValidation
        {
            [CurrencyCode]
            public string? test { get; set; }
        }

        [Fact]
        public void CurrencyCodeTest()
        {
            var test = new CurrencyCodeTestClass();

            // Null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is bad
            test.test = "";
            Assert.False(test.PassesValidation());

            // Single char
            test.test = "E";
            Assert.False(test.PassesValidation());

            // 3 characters, but not a valid one
            test.test = "AAA";
            Assert.False(test.PassesValidation());

            // Check against each currency
            Constants.ValidCurrencies.ForEach(vc =>
            {
                test.test = vc;
                Assert.True(test.PassesValidation());
            });
        }

        public class FXTickerTestClass : IValidation
        {
            [FXTicker]
            public string? test { get; set; }
        }

        [Fact]
        public void FXTickerTest()
        {
            var test = new FXTickerTestClass();

            // null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is bad
            test.test = "";
            Assert.False(test.PassesValidation());

            // Wrong number of chars
            test.test = "ABCABCABCABC";
            Assert.False(test.PassesValidation());

            // Invalid ticker both sides
            test.test = "AAA/BBB";
            Assert.False(test.PassesValidation());

            // Invalid ticker left side
            test.test = $"AAA/{Constants.ValidCurrencies[0]}";
            Assert.False(test.PassesValidation());

            // Invalid ticker right-side
            test.test = $"{Constants.ValidCurrencies[0]}/AAA";
            Assert.False(test.PassesValidation());

            // dash instead of /
            test.test = $"{Constants.ValidCurrencies[0]}-{Constants.ValidCurrencies[0]}";
            Assert.False(test.PassesValidation());

            // Match tickers on both sides
            test.test = $"{Constants.ValidCurrencies[0]}/{Constants.ValidCurrencies[0]}";
            Assert.False(test.PassesValidation());

            // Test all other combinations
            foreach (var left in Constants.ValidCurrencies)
            {
                foreach (var right in Constants.ValidCurrencies)
                {
                    test.test = $"{left}/{right}";
                    if (left == right)
                    {
                        Assert.False(test.PassesValidation());
                    }
                    else
                    {
                        Assert.True(test.PassesValidation());
                    }
                }
            }
            
        }

        [Fact]
        public void RunValidationAsIfTypeAndPropertyName()
        {
            List<string> Errors = new();

            var TestUserName = "Saganaki";
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserName, typeof(User), nameof(User.UserName), ref Errors, "TestUserName");
            Assert.NotEmpty(Errors);

            Errors.Clear();
            TestUserName = "saganaki";
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserName, typeof(User), nameof(User.UserName), ref Errors, "TestUserName");
            Assert.Empty(Errors);

            Errors.Clear();
            var TestUserNames = new List<string>
            {
                "Saganaki",
                "Kovus"
            };
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNames, typeof(User), nameof(User.UserName), ref Errors, "TestUserNames");
            Assert.Equal(2, Errors.Count);

            Errors.Clear();
            TestUserNames = TestUserNames.ConvertAll(u => u.ToLower());
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNames, typeof(User), nameof(User.UserName), ref Errors, "TestUserNames");
            Assert.Empty(Errors);

            var TestUserNamesArray = new string[]
            {
                "Saganaki",
                "Kovus"
            };
            Errors.Clear();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNamesArray, typeof(User), nameof(User.UserName), ref Errors, "TestUserNamesArray");
            Assert.Equal(2, Errors.Count);

            TestUserNamesArray[0] = "saganaki";
            TestUserNamesArray[1] = "kovus";
            Errors.Clear();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNamesArray, typeof(User), nameof(User.UserName), ref Errors, "TestUserNamesArray");
            Assert.Empty(Errors);
        }
    }
}