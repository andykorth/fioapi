﻿# FIOAPI
This is the starting point for a new, improved FIO API.

## FIOAPI
This is the API code itself. Most dev work starts [HERE](FIOAPI/README.md).

## FIOAPI Tests
- Documentation is [HERE](FIOAPI/TESTING.md)
- `FIOAPI.Integration.Test` - This is the integration test project.
   - This will spin up an actual instance of the server for the test project to interact with
   - It is expected that any new functionality will have appropriate tests implemented for it
- `FIOAPI.Internal.Test` - This is the internal (simple unit) test project.
   - This is for general systemic tests that doesn't require server interaction