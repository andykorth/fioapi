# Database
Currently, there are two database solutions available for use.

- SQLite - The default option. Used for development.
- Postgres - Used for server in the wild

## Database Definitions
- **Model**: A C# object that represents the table schema (the columns) of a table.
- **Migration**: An "upgrade" to the database. Managed by EF (Entity Framework) core.

### Creating a model
1. Create a new class under the DB/Model directory. For example's sake, let's call the class `Widget`
2. Inherit from IValidation
3. By convention, create key by naming it `<ClassName>Id` (example case would be: `WidgetId`)
   1. Mark it with the `[Key]` attribute
   2. Mark it with the `[JsonIgnore]` attribute unless you want it to be visible to the end user
   3. Mark it as auto-generated or not:
      - **Auto-Generated**: Add `[DatabaseGenerated(DatabaseGenerated.Identity)]` attribute.
	  - **Manually-Generated**: Add `DatabaseGenerated(DatabaseGenerated.None)]` attribute. Set other attributes as you see fit.
4. If you wish to add sub-objects that are also stored in the database, create a `List<Model>` property and mark it as virtual
   - Example: `public virtual List<SubObjectModel> SubObjects = new List<SubObjectModel>();`
5. If the model *is* a sub-object, make sure to have the last two fields be:
   - `public <IdType> ParentModelId { get; set; }` and ensure the attributes (except the `[Key]` and `[DatabaseGenerated]`) are the same
   - `public virtual ParentModel ParentModel { get; set; } = null!`
6. If you intend to query a field on that model often (a good example being looking up by Ticker), create an index of that field
   - On the **class**, add `[Index(nameof(FieldName))]`
7. [Apply as many validation attributes as you can](VALIDATION.md#validation-attributes)
8. Add the model to the `FIODBContext` class.
9. [Create a migration for the table](#creating-a-migration)

### Modifying a model
If you modify a model in a way that affects how it is represented in the database, you need to [create a migration](#creating-a-migration).

Examples of modifications that require a migration:
1. Adding a field
2. Removing a field
3. Modifying a field:
   1. Changing the type
   2. Changing the maximum in a StringLength attribute
   3. Changing the nullability

### Creating a migration
Migrations are operations on the database to facilitate upgrades. This is all handled by EF Core. The TL;DR of this is that when you modify/add anything that is stored in the database, a migration is required. This sounds complicated, but really it's not.

1. Ensure FIOAPI builds
2. At the command line in this folder, run: `add-migration.bat <NameOfMigrationNoSpaces>`
3. Run FIOAPI with `--migrate --run-after-migrate`

## DBAccess object
The DBAccess object provides a simple way to access the database in code. For 99.9% of cases, you either use `DBAccess.GetReader()` or `DBAccess.GetWriter()`. Usage is as follows:

```csharp
// Reading example - If you attempt to save changes to the database using a reader, it will throw an exception
using (var reader = DBAccess.GetReader())
{
	var FoodProcessor = await reader.DB.Buildings
		.Where(b => b.Ticker == "FP")			// Find all results where the Ticker is equal to `FP`
		.Include(b => b.Costs)					// Include the costs sub-object. If not present, the sub-objects will not load
		.Include(b => b.Recipes)				// Include Recipes
			.ThenInclude(r => r.Inputs)			// Include the Recipe's inputs
		.Include(b => b.Recipes)				// We need to do this again for outputs
			.ThenInclude(r => r.Outputs)		// Include the Recipe's outputs
		.AsSplitQuery()							// Instead of an expensive JOIN, do split queries (this is an optimiation--should only be done when doing more than one `.Include`
		.FirstOrDefaultAsync();					// Get the first result from the query or null (default) if no entries exist
}
return Ok(FoodProcessor);


// Writing example
using (var writer = DBAccess.GetWriter())
{
	Model.Building newBuilding = new();
	newBuilding.Ticker = "FP";
	// ... Fill in all relevant fields
	
	// Run validation prior to inserting into the database
	var Errors = new List<string>();
	if (!newBuilding.Validate(ref Errors))
	{
		return BadRequest(Errors);
	}
	
	writer.DB.Buildings.Add(newBuilding);
	await writer.DB.SaveChangesAsync();
}

return Ok();
```
