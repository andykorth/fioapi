// ==UserScript==
// @name         FIOAPI Debug Post
// @namespace    https://apex.prosperousuniverse.com/
// @version      1.0
// @description  FIOAPI Debug
// @author       Saganaki
// @match        https://apex.prosperousuniverse.com/
// @grant        none
// @run-at       document-start
// @connect      *
// ==/UserScript==

// HOW TO USE:
// 1) Run FIOAPI is Debug
// 2) Run this script in tampermonkey
// 3) Refresh APEX and click around
// 4) Inspect {root}/FIOAPI/DebugPayloads/

const api_url = "https://localhost:7182";

function send_xhttp_request(url, data)
{
    let http = new XMLHttpRequest();
    http.onreadystatechange = function()
    {
        if (this.readyState === XMLHttpRequest.DONE)
        {
            let status = this.status;
            if (status === 0 || (status >= 200 && status < 400))
            {
                // The request has been completed successfully
            }
            else
            {
                // Oh no! There has been an error with the request!
            }
        }
    };
    http.withCredentials = false;
    http.open("POST", url, true);
    http.setRequestHeader("Content-type", "application/json");
    if (data !== null)
    {
        http.send(JSON.stringify(data));
    }
}


let OrigWebSocket = window.WebSocket;
let callWebSocket = OrigWebSocket.apply.bind(OrigWebSocket);
let wsAddListener = OrigWebSocket.prototype.addEventListener;
wsAddListener = wsAddListener.call.bind(wsAddListener);
window.WebSocket = function WebSocket(url, protocols)
{
    let ws;
    if (!(this instanceof WebSocket))
    {
        // Called without 'new' (browsers will throw an error).
        ws = callWebSocket(this, arguments);
    }
    else if (arguments.length === 1)
    {
        ws = new OrigWebSocket(url);
    }
    else if (arguments.length >= 2)
    {
        ws = new OrigWebSocket(url, protocols);
    }
    else
    { // No arguments (browsers will throw an error)
        ws = new OrigWebSocket();
    }

    wsAddListener(ws, 'message', function(event)
    {
            let outmsg = '';
            // Do stuff with event.data (received data).
            let re_event = /^[0-9:\s]*(?<event>\[\s*"event".*\])[\s0-9:\.]*/m;
            let result = event.data.match(re_event);
            if (result && result.groups && result.groups.event)
            {
                //console.log("Event found");
                let eventdata = JSON.parse(result.groups.event)[1];
                send_xhttp_request(api_url + "/debug", eventdata);
            }
    });
    return ws;
}.bind();
window.WebSocket.prototype = OrigWebSocket.prototype;
window.WebSocket.prototype.constructor = window.WebSocket;

let wsSend = OrigWebSocket.prototype.send;
wsSend = wsSend.apply.bind(wsSend);
OrigWebSocket.prototype.send = function(data)
{
    // TODO: Do something with the sent data if you wish.
    // console.log("Sent message");
    return wsSend(this, arguments);
};
