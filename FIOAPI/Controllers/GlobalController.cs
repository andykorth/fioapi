﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Global Controller
    /// </summary>
    [ApiController]
    [Route("/global")]
    public class GlobalController : ControllerBase
    {
        /// <summary>
        /// PUTs the countries payload (MESG_COUNTRY_REGISTRY_COUNTRIES) to FIOAPI
        /// </summary>
        /// <param name="MesgCountriesPayload">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("countries")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an administrator")]
        public async Task<IActionResult> PutCountries([FromBody] Payloads.Global.MESG_COUNTRY_REGISTRY_COUNTRIES MesgCountriesPayload)
        {
            var Errors = new List<string>();
            if (!MesgCountriesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = MesgCountriesPayload.payload.message.payload;
            if (data != null)
            {
                List<Model.Country> Countries = new();
                foreach (var country in data.countries)
                {
                    Model.Country Country = new();

                    Country.CountryId = country.id;
                    Country.Code = country.code;
                    Country.Name = country.name;
                    Country.NumericCode = country.currency.numericCode;
                    Country.CurrenyCode = country.currency.code;
                    Country.CurrencyName = country.currency.name;
                    Country.CurrencyDecimals = country.currency.decimals;

                    if (!Country.Validate(ref Errors))
                    {
                        return BadRequest(Errors);
                    }

                    Countries.Add(Country);
                }

                using (var writer = DBAccess.GetWriter())
                {
                    await writer.DB.Countries.UpsertRange(Countries)
                        .On(c => new { c.CountryId })
                        .RunAsync();
                }

                return Ok();
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all countries
        /// </summary>
        /// <returns>A list of Country model objects</returns>
        [HttpGet("countries")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Country>), "application/json")]
        public async Task<IActionResult> GetCountries()
        {
            List<Model.Country> Countries = new();
            using (var reader = DBAccess.GetReader())
            {
                Countries = await reader.DB.Countries.ToListAsync();
            }

            return Ok(Countries);
        }

        /// <summary>
        /// PUTs the simulation data payload (MESG_SIMULATION_DATA) to FIOAPI
        /// </summary>
        /// <param name="MesgSimulationDataPayload">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("simulationData")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an administrator")]
        public async Task<IActionResult> PutSimulationData([FromBody] Payloads.Global.MESG_SIMULATION_DATA MesgSimulationDataPayload)
        {
            List<string> Errors = new();
            if (!MesgSimulationDataPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var simData = MesgSimulationDataPayload.payload.message.payload;

            Model.SimulationData SimulationData = new();
            SimulationData.SimulationInterval = simData.simulationInterval;
            SimulationData.FlightSTLFactor = simData.flightSTLFactor;
            SimulationData.FlightFTLFactor = simData.flightFTLFactor;
            SimulationData.PlanetaryMotionFactor = simData.planetaryMotionFactor;
            SimulationData.ParsecLength = simData.parsecLength;

            if (!SimulationData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var existingSimData = await writer.DB.SimulationData.FirstOrDefaultAsync();
                if (existingSimData != null)
                {
                    writer.DB.SimulationData.Remove(existingSimData);
                }
                writer.DB.SimulationData.Add(SimulationData);
                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves Simulation Data
        /// </summary>
        /// <returns>The simulation data object</returns>
        [HttpGet("simulationdata")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(Model.SimulationData), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "No simulation data present")]
        public async Task<IActionResult> GetSimulationData()
        {
            Model.SimulationData? SimulationData = null;

            using (var reader = DBAccess.GetReader())
            {
                SimulationData = await reader.DB.SimulationData.FirstOrDefaultAsync();
            }

            if (SimulationData != null)
            {
                return Ok(SimulationData);
            }
            
            return NoContent();
        }

        /// <summary>
        /// PUTs the comex exchange data payload (PATH_COMMODITY_EXCHANGES) to FIOAPI
        /// </summary>
        /// <param name="ComexExchangePayload">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("comexexchanges")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an administrator")]
        public async Task<IActionResult> PutComexExchanges([FromBody] Payloads.Global.PATH_COMMODITY_EXCHANGES ComexExchangePayload)
        {
            List<string> Errors = new();
            if (!ComexExchangePayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var payload = ComexExchangePayload.payload.message.payload;
            List<Model.ComexExchange> ComexExchanges = new();

            foreach (var body in payload.body)
            {
                Model.ComexExchange ComexExchange = new();

                ComexExchange.ComexExchangeId = body.id;
                ComexExchange.Code = body.code;
                ComexExchange.Name = body.name;
                ComexExchange.Decimals = body.currency.decimals;
                ComexExchange.NumericCode = body.currency.numericCode;
                ComexExchange.NaturalId = body.address.lines.Last().entity.naturalId;

                if (!ComexExchange.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                ComexExchanges.Add(ComexExchange);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.ComexExchanges.UpsertRange(ComexExchanges)
                    .On(ce => ce.ComexExchangeId)
                    .RunAsync();
            }
            
            return Ok();
        }

        /// <summary>
        /// Retrieves comex exchanges
        /// </summary>
        /// <returns>A list of comex exchanges</returns>
        [HttpGet("comexexchanges")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.ComexExchange>), "application/json")]
        public async Task<IActionResult> GetComexExchanges()
        {
            List<Model.ComexExchange> ComexExchanges = new();

            using (var reader = DBAccess.GetReader())
            {
                ComexExchanges = await reader.DB.ComexExchanges.ToListAsync();
            }
            
            return Ok(ComexExchanges);
        }

        /// <summary>
        /// Puts a single station
        /// </summary>
        /// <param name="StationPayload">The PATH_STATION payload</param>
        /// <returns></returns>
        [HttpPut("station")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an administrator")]
        public async Task<IActionResult> PutStation([FromBody] Payloads.Global.PATH_STATION StationPayload)
        {
            List<string> Errors = new();
            if (!StationPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var payload = StationPayload.payload.message.payload.body;

            Model.Station Station = new();
            Station.StationId = payload.id;
            Station.NaturalId = payload.naturalId;
            Station.Name = payload.name;
            Station.CurrencyCode = payload.currency.code;
            Station.CountryId = payload.country.id;
            Station.CountryCode = payload.country.code;
            Station.CountryName = payload.country.name;
            Station.GoverningEntityId = payload.governingEntity.id;
            Station.GoverningEntityCode = payload.governingEntity.code;
            Station.GoverningEntityName = payload.governingEntity.name;

            if (!Station.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Stations.Upsert(Station)
                    .On(s => s.StationId)
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves all stations
        /// </summary>
        /// <returns>OK on success</returns>
        [HttpGet("stations")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Station>), "application/json")]
        public async Task<IActionResult> GetStations()
        {
            List<Model.Station> Stations = new();

            using (var reader = DBAccess.GetReader())
            {
                Stations = await reader.DB.Stations.ToListAsync();
            }
             
            return Ok(Stations);
        }

        /// <summary>
        ///  PUTS the workforceneeds payload
        /// </summary>
        /// <param name="WorkforceNeedsPayload">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("workforceneeds")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an administrator")]
        public async Task<IActionResult> PutWorkforceNeeds([FromBody] Payloads.Global.MESG_WORKFORCE_WORKFORCES WorkforceNeedsPayload)
        {
            List<string> Errors = new();
            if (!WorkforceNeedsPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            List<Model.WorkforceRequirement> WorkforceRequirements = new List<Model.WorkforceRequirement>();
            var payload = WorkforceNeedsPayload.payload.message.payload;
            foreach (var workforce in payload.workforces)
            {
                Model.WorkforceRequirement WorkforceRequirement = new();
                WorkforceRequirement.Level = workforce.level;
                foreach (var need in workforce.needs)
                {
                    if (need.unitsPer100 > 0.0)
                    {
                        Model.WorkforceRequirementNeed WorkforceRequirementNeed = new();
                        WorkforceRequirementNeed.Essential = need.essential;
                        WorkforceRequirementNeed.MaterialName = need.material.name;
                        WorkforceRequirementNeed.MaterialTicker = need.material.ticker;
                        WorkforceRequirementNeed.MaterialId = need.material.id;
                        WorkforceRequirementNeed.UnitsPer100 = need.unitsPer100;
                        WorkforceRequirement.Needs.Add(WorkforceRequirementNeed);
                    }
                }

                if (!WorkforceRequirement.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                WorkforceRequirements.Add(WorkforceRequirement);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.WorkforceRequirements.DeleteAsync();
                writer.DB.WorkforceRequirements.AddRange(WorkforceRequirements);
                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves all workforce needs
        /// </summary>
        /// <returns>OK on success</returns>
        [HttpGet("workforceneeds")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.WorkforceRequirement>), "application/json")]
        public async Task<IActionResult> GetWorkforceNeeds()
        {
            List<Model.WorkforceRequirement> requirements;
            using (var reader = DBAccess.GetReader())
            {
                requirements = await reader.DB.WorkforceRequirements
                    .Include(wr => wr.Needs)
                    .ToListAsync();
            }

            return Ok(requirements);
        }
    }
}
