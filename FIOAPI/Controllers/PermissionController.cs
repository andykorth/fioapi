﻿using FIOAPI.Payloads.Permission;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Permission endpoint
    /// </summary>
    [ApiController]
    [Route("/permission")]
    public class PermissionController : ControllerBase
    {
        /// <summary>
        /// Retrieves grantor permissions
        /// </summary>
        /// <returns>Grantor permission for the current logged in user</returns>
        /// <remarks>Note that this will NOT include groups you are in</remarks>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully retrieved grantor permission", typeof(List<PermissionResponse>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authorized")]
        public async Task<IActionResult> GetPermissionsAsync()
        {
            var thisUser = this.GetUserName();
            using (var reader = DBAccess.GetReader())
            {
                var permissions = await reader.DB.Permissions
                    .Where(p => p.GrantorUserName == thisUser)
                    .Select(p => PermissionResponse.FromDBPermission(p))
                    .ToListAsync();
                return Ok(permissions);
            }
        }

        /// <summary>
        /// Retrieves granted permissions
        /// </summary>
        /// <returns>Grantee permissions for the current logged in user</returns>
        /// <remakrs>Note that this will NOT include groups</remakrs>
        [HttpGet("granted")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully retrieved granted permissions", typeof(List<PermissionResponse>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authorized")]
        public async Task<IActionResult> GetGrantedAsync()
        {
            var thisUser = this.GetUserName();
            using (var reader = DBAccess.GetReader())
            {
                var permissions = await reader.DB.Permissions
                    .Where(p => p.GranteeUserName == thisUser || p.GranteeUserName == "*")
                    .Select(p => Permissions.FromDBPermission(p))
                    .ToListAsync();
                return Ok(permissions);
            }
        }

        /// <summary>
        /// Retrieves the provided GroupId's permissions
        /// </summary>
        /// <param name="GroupId">GroupId</param>
        /// <returns>200 on success</returns>
        [HttpGet("group/{GroupId:int}")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully retrieved permissions for GroupId", typeof(Permissions), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid GroupId specified")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authorized")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found or you are not a member of the group")]
        public async Task<IActionResult> GetGroupAsync(int GroupId)
        {
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            var thisUser = this.GetUserName();
            using (var reader = DBAccess.GetReader())
            {
                var CanSeeGroup = await reader.DB.Groups
                    .Where(g => g.GroupId == GroupId)
                    .Where(g => g.GroupOwner == thisUser || g.Admins.Any(a => a.UserName == thisUser || g.Users.Any(u => u.UserName == thisUser)))
                    .AnyAsync();
                if (CanSeeGroup)
                {
                    var perm = await reader.DB.Permissions
                        .Where(p => p.GroupId == GroupId)
                        .Select(p => Permissions.FromDBPermission(p))
                        .FirstOrDefaultAsync();
                    return Ok(perm);
                }
                else
                {
                    return NotFound();
                }
            }
        }

        /// <summary>
        /// Grants permission to another user for your data
        /// </summary>
        /// <param name="payload">The grant payload</param>
        /// <returns>200 on success</returns>
        /// <remarks>You can use this to update an existing permission</remarks>
        [HttpPut("grant")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully granted permissions")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "Username not found")]
        public async Task<IActionResult> GrantAsync([FromBody] Grant payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            var thisUser = this.GetUserName()!;

            var NewPerm = payload.Permissions.ToDBPermission();
            NewPerm.GrantorUserName = thisUser;
            NewPerm.GranteeUserName = payload.UserName.ToLower();

            NewPerm.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            using (var writer = DBAccess.GetWriter())
            {
                // Make sure the GranteeUserName is valid
                if (NewPerm.GranteeUserName != "*")
                {
                    var IsValidUser = await writer.DB.Users
                        .AnyAsync(u => u.UserName == NewPerm.GranteeUserName);
                    if (!IsValidUser)
                    {
                        return NotFound();
                    }
                }

                await writer.DB.Permissions
                   .Upsert(NewPerm)
                   .On(p => new { p.GrantorUserName, p.GranteeUserName })
                   .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Revokes the specified Grantee permission
        /// </summary>
        /// <param name="Grantee">The grantee to revoke</param>
        /// <returns>Ok on success</returns>
        [HttpDelete("revoke/{Grantee}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully granted permissions")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "Grantee username not found")]
        public async Task<IActionResult> RevokeAsync(string Grantee)
        {
            Grantee = Grantee.ToLower();

            var Errors = new List<string>();
            if (Grantee != "*")
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(Grantee, typeof(DB.Model.User), nameof(Model.User.UserName), ref Errors, nameof(Grantee));
                if (Errors.Count > 0)
                {
                    return BadRequest(Errors);
                }
            }            

            using (var writer = DBAccess.GetWriter())
            {
                var existingPerm = writer.DB.Permissions
                    .Where(p => p.GrantorUserName == this.GetUserName()! && p.GranteeUserName == Grantee)
                    .FirstOrDefault();
                if (existingPerm != null)
                {
                    writer.DB.Permissions.Remove(existingPerm);
                    await writer.DB.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
        }
    }
}
