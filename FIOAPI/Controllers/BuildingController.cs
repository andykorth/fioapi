﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Building controller
    /// </summary>
    [ApiController]
    [Route("/building")]
    public class BuildingController : ControllerBase
    {
        /// <summary>
        /// PUTs the building payload (MESG WORLD_REACTOR_DATA) to FIOAPI
        /// </summary>
        /// <param name="MesgWorldReactorDataPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutBuilding([FromBody] Payloads.Building.MESG_WORLD_REACTOR_DATA MesgWorldReactorDataPayload)
        {
            var Errors = new List<string>();
            if (!MesgWorldReactorDataPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = MesgWorldReactorDataPayload.payload.message.payload;
            if (data != null)
            {
                Model.Building Building = new();
                Building.BuildingId = data.id;
                Building.Name = data.name;
                Building.Ticker = data.ticker;
                Building.Expertise = data.expertise;
                foreach (var workforceCapacity in data.workforceCapacities)
                {
                    switch (workforceCapacity.level)
                    {
                        case "PIONEER":
                            Building.Pioneers = workforceCapacity.capacity;
                            break;
                        case "SETTLER":
                            Building.Settlers = workforceCapacity.capacity;
                            break;
                        case "TECHNICIAN":
                            Building.Technicians = workforceCapacity.capacity;
                            break;
                        case "ENGINEER":
                            Building.Engineers = workforceCapacity.capacity;
                            break;
                        case "SCIENTIST":
                            Building.Scientists = workforceCapacity.capacity;
                            break;
                    }
                }

                Building.AreaCost = data.areaCost;

                foreach (var buiCost in data.buildingCosts)
                {
                    Model.BuildingCost BuildingCost = new()
                    {
                        BuildingCostId = $"{Building.BuildingId}-{buiCost.material.ticker}",
                        Name = buiCost.material.name,
                        Ticker = buiCost.material.ticker.ToUpper(),
                        Weight = buiCost.material.weight,
                        Volume = buiCost.material.volume,
                        Amount = buiCost.amount,
                        BuildingId = Building.BuildingId,
                        Building = Building
                    };

                    Building.Costs.Add(BuildingCost);
                }

                foreach (var recipe in data.recipes)
                {
                    Model.BuildingRecipe BuildingRecipe = new();

                    foreach (var input in recipe.inputs)
                    {
                        Model.BuildingRecipeInput Input = new()
                        {
                            Id = input.material.id,
                            Name = input.material.name,
                            Ticker = input.material.ticker.ToUpper(),
                            Weight = input.material.weight,
                            Volume = input.material.volume,
                            Amount = input.amount,
                            BuildingRecipe = BuildingRecipe
                        };

                        BuildingRecipe.Inputs.Add(Input);
                    }
                    BuildingRecipe.Inputs = BuildingRecipe.Inputs
                        .OrderBy(i => i.Ticker)
                        .ToList();

                    foreach (var output in recipe.outputs)
                    {
                        Model.BuildingRecipeOutput Output = new()
                        {
                            Id = output.material.id,
                            Name = output.material.name,
                            Ticker = output.material.ticker.ToUpper(),
                            Weight = output.material.weight,
                            Volume = output.material.volume,
                            Amount = output.amount,
                            BuildingRecipe = BuildingRecipe,
                        };

                        BuildingRecipe.Outputs.Add(Output);
                    }
                    BuildingRecipe.Outputs = BuildingRecipe.Outputs
                        .OrderBy(o => o.Ticker)
                        .ToList();

                    BuildingRecipe.DurationMs = recipe.duration.millis;

                    BuildingRecipe.BuildingId = Building.BuildingId;
                    BuildingRecipe.Building = Building;

                    BuildingRecipe.GenerateRecipeId();

                    BuildingRecipe.Inputs.ForEach(i =>
                    {
                        i.BuildingRecipeId = BuildingRecipe.BuildingRecipeId;
                        i.BuildingRecipeInputId = $"{i.BuildingRecipeId}-{i.Ticker}";
                    });
                    BuildingRecipe.Outputs.ForEach(o =>
                    {
                        o.BuildingRecipeId = BuildingRecipe.BuildingRecipeId;
                        o.BuildingRecipeOutputId = $"{o.BuildingRecipeId}-{o.Ticker}";
                    });

                    Building.Recipes.Add(BuildingRecipe);
                }

                Building.UserNameSubmitted = this.GetUserName()!;
                Building.Timestamp = DateTime.UtcNow;

                Errors.Clear();
                if (!Building.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                using (var writer = DBAccess.GetWriter())
                {
                    await writer.DB.Buildings.Upsert(Building)
                    .On(bm => new { bm.BuildingId })
                    .RunAsync();

                    await writer.DB.BuildingCosts.UpsertRange(Building.Costs)
                        .On(bbc => new { bbc.BuildingCostId })
                        .RunAsync();

                    await writer.DB.BuildingRecipes.UpsertRange(Building.Recipes)
                        .On(r => new { r.BuildingRecipeId })
                        .RunAsync();

                    var inputs = Building.Recipes.SelectMany(r => r.Inputs);
                    await writer.DB.BuildingRecipeInputs.UpsertRange(inputs)
                        .On(bri => new { bri.BuildingRecipeInputId })
                        .RunAsync();

                    var outputs = Building.Recipes.SelectMany(r => r.Outputs);
                    await writer.DB.BuildingRecipeOutputs.UpsertRange(outputs)
                        .On(bro => new { bro.BuildingRecipeOutputId })
                        .RunAsync();
                }

                return Ok();
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all buildings
        /// </summary>
        /// <param name="include_costs">If building costs should be included [default: true]</param>
        /// <param name="include_recipes">if recipes should be included [default: true]</param>
        /// <returns>Ok</returns>
        [HttpGet("")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Building>), "application/json")]
        public async Task<IActionResult> GetAllBuildings(
            [FromQuery(Name = "include_costs")] bool include_costs = true,
            [FromQuery(Name = "include_recipes")] bool include_recipes = true)
        {
            List<Model.Building> Buildings = new();
            using (var reader = DBAccess.GetReader())
            {
                var BuildingQuery = reader.DB.Buildings.AsQueryable();
                if (include_costs)
                {
                    BuildingQuery = BuildingQuery.Include(b => b.Costs);
                }

                if (include_recipes)
                {
                    BuildingQuery = BuildingQuery
                        .Include(b => b.Recipes)
                            .ThenInclude(r => r.Inputs)
                        .Include(b => b.Recipes)
                            .ThenInclude(r => r.Outputs);
                }

                Buildings = await BuildingQuery.ToListAsync();
            }

            return Ok(Buildings);
        }

        /// <summary>
        /// Retrieves the specified building by ticker
        /// </summary>
        /// <param name="BuildingTicker">The Building ticker</param>
        /// <param name="include_costs">If building costs should be included [default: true]</param>
        /// <param name="include_recipes">if recipes should be included [default: true]</param>
        /// <returns>The building</returns>
        [HttpGet("{BuildingTicker}")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(Model.Building), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Not found")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Ticker specified outside the valid range")]
        public async Task<IActionResult> GetBuilding(
            string BuildingTicker,
            [FromQuery(Name = "include_costs")] bool include_costs = true,
            [FromQuery(Name = "include_recipes")] bool include_recipes = true)
        {
            BuildingTicker = BuildingTicker.Trim().ToUpper();

            if (string.IsNullOrEmpty(BuildingTicker))
            {
                return await GetAllBuildings();
            }

            List<string> Errors = new();

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(BuildingTicker, typeof(Building), nameof(Model.Building.Ticker), ref Errors, nameof(BuildingTicker));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            Model.Building? Building;
            using (var reader = DBAccess.GetReader())
            {
                var BuildingQuery = reader.DB.Buildings.AsQueryable();

                if (include_costs)
                {
                    BuildingQuery = BuildingQuery
                        .Include(b => b.Costs);
                }

                if (include_recipes)
                {
                    BuildingQuery = BuildingQuery
                        .Include(b => b.Recipes)
                            .ThenInclude(r => r.Inputs)
                        .Include(b => b.Recipes)
                            .ThenInclude(r => r.Outputs);
                }

                Building = await BuildingQuery
                    .Where(b => b.Ticker == BuildingTicker)
                    .FirstOrDefaultAsync();
            }

            if (Building != null)
            {
                return Ok(Building);
            }

            return NoContent();
        }

        private const string Habitation = "HABITATION";
        private const string PlanetaryProject = "PLANETARYPROJECT";
        private const string CorporationProject = "CORPORATIONPROJECT";

        /// <summary>
        /// Retrieves all core base buildings
        /// </summary>
        /// <param name="include_costs">If building costs should be included [default: true]</param>
        /// <returns>All core base buildings</returns>
        [HttpGet("corebasebuildings")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Building>), "application/json")]
        public async Task<IActionResult> GetCoreBaseBuildingsAsync([FromQuery(Name = "include_costs")] bool include_costs = true)
        {
            List<Model.Building> Buildings = new();
            using (var reader = DBAccess.GetReader())
            {
                var BuildingQuery = reader.DB.Buildings
                    .Where(b => !b.Name.ToUpper().Contains(PlanetaryProject) && !b.Name.ToUpper().Contains(CorporationProject))
                    .Where(b => b.Pioneers == 0 && b.Settlers == 0 && b.Technicians == 0 && b.Engineers == 0 && b.Scientists == 0);

                if (include_costs)
                {
                    BuildingQuery = BuildingQuery.Include(b => b.Costs);
                }

                Buildings = await BuildingQuery.ToListAsync();
            }

            return Ok(Buildings);
        }

        /// <summary>
        /// Retrieves all habitation buildings
        /// </summary>
        /// <param name="include_costs">If building costs should be included [default: true]</param>
        /// <returns>All habitation buildings</returns>
        [HttpGet("habitationbuildings")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Building>), "application/json")]
        public async Task<IActionResult> GetHabitationBuildingsAsync([FromQuery(Name = "include_costs")] bool include_costs = true)
        {
            List<Model.Building> Buildings = new();
            using (var reader = DBAccess.GetReader())
            {
                var BuildingQuery = reader.DB.Buildings
                    .Where(b => !b.Name.ToUpper().Contains(Habitation));

                if (include_costs)
                {
                    BuildingQuery = BuildingQuery.Include(b => b.Costs);
                }

                Buildings = await BuildingQuery.ToListAsync();
            }

            return Ok(Buildings);
        }

        /// <summary>
        /// Retrieves all planetary project buildings
        /// </summary>
        /// <param name="include_costs">If building costs should be included [default: true]</param>
        /// <returns>All planetary project buildings</returns>
        [HttpGet("planetaryprojectbuildings")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Building>), "application/json")]
        public async Task<IActionResult> GetPlanetaryProjectBuildingsAsync([FromQuery(Name = "include_costs")] bool include_costs = true)
        {
            List<Model.Building> Buildings = new();
            using (var reader = DBAccess.GetReader())
            {
                var BuildingQuery = reader.DB.Buildings
                    .Where(b => !b.Name.ToUpper().Contains(PlanetaryProject));

                if (include_costs)
                {
                    BuildingQuery = BuildingQuery.Include(b => b.Costs);
                }

                Buildings = await BuildingQuery.ToListAsync();
            }

            return Ok(Buildings);
        }

        /// <summary>
        /// Retrieves all corporation project building
        /// </summary>
        /// <param name="include_costs">If building costs should be included [default: true]</param>
        /// <returns>All corporation project buildings</returns>
        [HttpGet("corporationprojectbuildings")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Building>), "application/json")]
        public async Task<IActionResult> GetCorporationProjectBuildingsAsync([FromQuery(Name = "include_costs")] bool include_costs = true)
        {
            List<Model.Building> Buildings = new();
            using (var reader = DBAccess.GetReader())
            {
                var BuildingQuery = reader.DB.Buildings
                    .Where(b => !b.Name.ToUpper().Contains(CorporationProject));

                if (include_costs)
                {
                    BuildingQuery = BuildingQuery.Include(b => b.Costs);
                }

                Buildings = await BuildingQuery.ToListAsync();
            }

            return Ok(Buildings);
        }
    }
}
