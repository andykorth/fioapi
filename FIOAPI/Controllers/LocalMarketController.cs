﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// LocalMarketController
    /// </summary>
    [ApiController]
    [Route("/localmarket")]
    public class LocalMarketController : ControllerBase
    {
        /// <summary>
        /// Puts the PATH_LOCALMARKET_AD payload
        /// </summary>
        /// <param name="LocalMarketAdData">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutLocalMarketAd([FromBody] Payloads.LocalMarket.PATH_LOCALMARKET_AD LocalMarketAdData)
        {
            List<string> Errors = new();
            if (!LocalMarketAdData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = LocalMarketAdData.payload.message.payload;

            if (!data.body.Any())
            {
                return Ok();
            }

            List<LocalMarketAd> LocalMarketAds = new();

            foreach (var ad in data.body)
            {
                LocalMarketAd LMAd = new();
                LMAd.LocalMarketAdId = ad.id;
                LMAd.LocalMarketId = ad.localMarketId;
                LMAd.NaturalId = ad.address.lines.Last().entity.naturalId;
                LMAd.AdNaturalId = ad.naturalId;
                LMAd.Type = ad.type;
                LMAd.Weight = ad.cargoWeight;
                LMAd.Volume = ad.cargoVolume;
                LMAd.CreatorId = ad.creator.id;
                LMAd.CreatorName = ad.creator.name;
                LMAd.CreatorCode = ad.creator.code;
                LMAd.MaterialTicker = ad.quantity?.material.ticker;
                LMAd.MaterialAmount = ad.quantity?.amount;
                LMAd.Price = ad.price.amount;
                LMAd.Currency = ad.price.currency;
                LMAd.FulfillmentDays = ad.advice;
                LMAd.MinimumRating = ad.minimumRating == "PENDING" ? "P" : ad.minimumRating;
                LMAd.CreationTime = ad.creationTime.timestamp.FromUnixTime();
                LMAd.Expiry = ad.expiry.timestamp.FromUnixTime();
                LMAd.OriginNaturalId = ad.origin?.lines.Last().entity.naturalId;
                LMAd.DestinationNaturalId = ad.destination?.lines.Last().entity.naturalId;

                if (!LMAd.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                LocalMarketAds.Add(LMAd);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.LocalMarketAds
                    .Where(lma => lma.LocalMarketId == LocalMarketAds.First().LocalMarketId)
                    .DeleteAsync();

                await writer.DB.LocalMarketAds
                    .AddRangeAsync(LocalMarketAds);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves LM ads
        /// </summary>
        /// <param name="LocalMarketNaturalId">LM natural id</param>
        /// <param name="include_buys">If we should emit buys</param>
        /// <param name="include_sells">If we should emit sells</param>
        /// <param name="include_shipments">If we should emit shipments</param>
        /// <returns>OK</returns>
        [HttpGet("{LocalMarketNaturalId}")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<LocalMarketAd>), "application/json")]
        public async Task<IActionResult> GetLocalMarketAds(
            string LocalMarketNaturalId,
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true,
            [FromQuery(Name = "include_shipments")] bool include_shipments = true)
        {
            List<LocalMarketAd> Results = new();
            using (var reader = DBAccess.GetReader())
            {
                var LMQuery = reader.DB.LocalMarketAds.AsQueryable();
                if (!include_buys)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != "COMMODITY_BUYING");
                }
                if (!include_sells)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != "COMMODITY_SELLING");
                }
                if (!include_shipments)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != "COMMODITY_SHIPPING");
                }

                LMQuery.Where(lma => lma.NaturalId == LocalMarketNaturalId);

                Results = await LMQuery.ToListAsync();
            }

            return Ok(Results);
        }
    }
}
