﻿using FIOAPI.DB.Model;
using FIOAPI.Payloads.Admin;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Admin endpoint
    /// </summary>
    [ApiController]
    [Route("/admin")]
    public class AdminController : ControllerBase
    {
        /// <summary>
        /// Checks to see if the current user is an administrator
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.AdminRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "You are admin")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "You are not admin")]
        public IActionResult Get()
        {
            return Ok();
        }

        /// <summary>
        /// Checks to see if the provided username is a user
        /// </summary>
        /// <param name="UserName">UserName</param>
        /// <returns>Ok if users is present</returns>
        [HttpGet("isuser/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Is a valid user")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Not a valid user")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid UserName", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> IsUser(string UserName)
        {
            List<string> Errors = new();
            UserName = UserName.ToLower();

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var reader = DBAccess.GetReader())
            {
                var IsValidUser = await reader.DB.Users.AnyAsync(u => u.UserName == UserName);
                return IsValidUser ? Ok() : NoContent();
            }
        }

        /// <summary>
        /// Creates an account
        /// </summary>
        /// <param name="payload">The account creation payload</param>
        /// <returns>200 on success</returns>
        [HttpPost("createaccount")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully created account")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin or using read-only api key")]
        public async Task<IActionResult> CreateAccountAsync([FromBody] CreateAccount payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            var NewUser = new User()
            {
                UserName = payload.UserName.ToLower(),
                DisplayUserName = payload.UserName,
                PasswordHash = SecurePasswordHasher.Hash(payload.Password),
                IsAdmin = payload.Admin
            };
            NewUser.RunValidation_Throw();

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Users.Upsert(NewUser)
                    .On(u => u.UserName)
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Deletes the account of the given username
        /// </summary>
        /// <param name="UserName">The username to delete</param>
        /// <returns>200 on success</returns>
        [HttpDelete("deleteaccount/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully created account")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Account by that name not found")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin or using read-only api key")]
        public async Task<IActionResult> DeleteAccountAsync(string UserName)
        {
            List<string> Errors = new();
            UserName = UserName.ToLower();

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
#if DEBUG
                var allUsers = await writer.DB.Users.ToListAsync();
#endif

                var user = await writer.DB.Users.FirstOrDefaultAsync(u => u.UserName == UserName);
                if (user != null)
                {
                    // @TODO: Clear all user data
                    writer.DB.Users.Remove(user);
                    await writer.DB.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return NoContent();
                }
            }
        }

        /// <summary>
        /// Clears all CX data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearcxdata")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all CX Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearCXDataAsync()
        {
            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// Clears all BUI data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearbuidata")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all BUI Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearBUIDataAsync()
        {
            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// Clears all MAT data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearmatdata")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all MAT Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearMATDataAsync()
        {
            // @TODO
            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// Starts a logging session
        /// </summary>
        /// <param name="UserName">The username to start a logging session for</param>
        /// <returns>OK</returns>
        [HttpPut("requestlogging/start/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Logging started")]
        public IActionResult StartRequestLogging(string UserName)
        {
            RequestLogger.Start(UserName);
            return Ok();
        }

        /// <summary>
        /// Cancels a logging session
        /// </summary>
        /// <param name="UserName">The username to cancel a logging session for</param>
        /// <returns></returns>
        [HttpPut("requestlogging/cancel/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Logging cancelled")]
        public IActionResult CancelRequestLogging(string UserName)
        {
            RequestLogger.Cancel(UserName);
            return Ok();
        }

        /// <summary>
        /// Finish a logging session and return the payload
        /// </summary>
        /// <param name="UserName">The username to finish a logging session for</param>
        /// <returns>The FinishedRequestPayload</returns>
        [HttpPut("requestlogging/finish/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Finish logging and receive the payload", typeof(FinishedRequestPayload), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "No active session for user found")]
        public IActionResult FinishRequestLogging(string UserName)
        {
            var result = RequestLogger.Finish(UserName);
            if (result != null)
            {
                return Ok(result);
            }

            return NoContent();
        }
    }
}
