﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// CompanyController
    /// </summary>
    [ApiController]
    [Route("/company")]
    public class CompanyController : ControllerBase
    {
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCompany([FromBody] Payloads.Company.PATH_COMPANY data_payload)
        {
            List<string> Errors = new();
            if (!data_payload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = data_payload.payload.message.payload.body;

            DB.Model.Company model = new();
            model.id = data.id;
            model.name = data.name;
            model.code = data.code;
            model.founded = data.founded.timestamp.FromUnixTime();
            model.UserName = data.user.username;
            model.CountryId = data.country.id;
            model.CountryCode = data.country.code;
            model.CountryName = data.country.name;

            if (data.corporation != null)
            {
                model.CorporationId = data.corporation.id;
                model.CorporationName = data.corporation.name;
                model.CorporationCode = data.corporation.code;
            }

            model.currentAPEXRepresentationLevel = data.currentRepresentationLevel;
            
            if (data.ratingReport != null)         
            {
                model.overallRating = data.ratingReport.overallRating;
                model.ratingContractCount = data.ratingReport.contractCount;
                if (data.ratingReport.earliestContract != null) {
                    model.ratingsEarliestContract = data.ratingReport.earliestContract.timestamp.FromUnixTime();
                }
            }

            if (data.reputation != null && data.reputation.Count > 0)
            {
                model.reputationEntityName = data.reputation.First().entityId.name;
                model.reputation = data.reputation.First().reputation;
            }

            // fill in the planets list
            model.Planets = new List<CompanyPlanet>();
            if (data.siteAddresses != null)
            {
                foreach(var address in data.siteAddresses)
                {
                    foreach (var planetAddress in address.lines) {
                        // Ignore system address lines, we don't need them. 
                        if (planetAddress.type == "PLANET") {
                            CompanyPlanet planet = new CompanyPlanet();

                            // For non-generated children ids, I tend to concatenate ParentId-ChildId. 
                            planet.CompanyPlanetId = $"{model.id}-{planetAddress.entity.id}";

                            planet.PlanetId = planetAddress.entity.id;
                            planet.PlanetNaturalId = planetAddress.entity.naturalId;
                            planet.PlanetName = planetAddress.entity.name;

                            planet.CompanyId = model.id;
                            planet.Company = model;

                            model.Planets.Add(planet);
                        }
                    }
                }
            }


            if (!model.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Companies
                    .Where(a => a.id == model.id)
                    .DeleteAsync();

                await writer.DB.Companies
                    .AddRangeAsync(model);

                // maybe it should remove all CompanyPlanets for a companyID then add them all? 
                // no idea how this code behaves with regard to removed bases.
                var companyPlanets = model.Planets;
                await writer.DB.CompanyPlanets.UpsertRange(companyPlanets)
                    .On(a => new { a.CompanyPlanetId })
                    .RunAsync();

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves company data
        /// </summary>
        /// <param name="CompanyCode">CompanyCode</param>
        /// <returns>OK</returns>
        [HttpGet("{CompanyCode}")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Company>), "application/json")]
        public async Task<IActionResult> GetCompanies(
            string CompanyCode)
        {
            List<Company> Results = new();
            using (var reader = DBAccess.GetReader())
            {
                var LMQuery = reader.DB.Companies.AsQueryable()
                            .Include(a => a.Planets)
                            .Where(a => a.code == CompanyCode);
                
                Results = await LMQuery.ToListAsync();
            }

            return Ok(Results);
        }
    }
}
