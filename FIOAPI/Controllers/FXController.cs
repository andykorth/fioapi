﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// FXController
    /// </summary>
    [ApiController]
    [Route("/fx")]
    public class FXController : ControllerBase
    {
        /// <summary>
        /// Puts the FX BrokerData payload
        /// </summary>
        /// <param name="BrokerData">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutFX([FromBody] Payloads.Currency.MESG_FOREX_BROKER_DATA BrokerData)
        {
            List<string> Errors = new();
            if (!BrokerData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = BrokerData.payload.message.payload;

            var fx = new FX();
            fx.FXID = data.id;
            fx.Ticker = data.ticker;
            fx.BaseCurrencyNumericCode = data.pair.@base.numericCode;
            fx.BaseCurrencyCode = data.pair.@base.code;
            fx.BaseCurrencyName = data.pair.@base.name;
            fx.BaseCurrencyDecimals = data.pair.@base.decimals;
            fx.QuoteCurrencyNumericCode = data.pair.quote.numericCode;
            fx.QuoteCurrencyCode = data.pair.quote.code;
            fx.QuoteCurrencyName = data.pair.quote.name;
            fx.QuoteCurrencyDecimals = data.pair.quote.decimals;
            fx.Decimals = data.pair.decimals;
            fx.Open = data.price.open.rate;
            fx.Close = data.price.close.rate;
            fx.Low = data.price.low.rate;
            fx.High = data.price.high.rate;
            fx.Previous = data.price.previous.rate;
            fx.Traded = data.price.traded.amount;
            fx.Volume = data.price.volume.amount;
            fx.Bid = data.bid?.rate;
            fx.Ask = data.ask?.rate;
            fx.Spread = data.spread?.rate;
            fx.LotSizeAmount = data.lotSize.amount;
            fx.LotCurrency = data.lotSize.currency;
            fx.FeesFactor = data.feesFactor;

            foreach (var buyOrder in data.buyingOrders)
            {
                var BuyOrder = new FXBuyOrder();
                BuyOrder.FXBuyOrderId = buyOrder.id;
                BuyOrder.UserId = buyOrder.trader.id;
                BuyOrder.UserName = buyOrder.trader.name;
                BuyOrder.UserCompanyCode = buyOrder.trader.code;
                BuyOrder.FXID = fx.FXID;
                BuyOrder.FX = fx;
                fx.BuyOrders.Add(BuyOrder);
            }

            foreach (var sellOrder in data.sellingOrders)
            {
                var SellOrder = new FXSellOrder();
                SellOrder.FXSellOrderId = sellOrder.id;
                SellOrder.UserId = sellOrder.trader.id;
                SellOrder.UserName = sellOrder.trader.name;
                SellOrder.UserCompanyCode = sellOrder.trader.code;
                SellOrder.FXID = fx.FXID;
                SellOrder.FX = fx;
                fx.SellOrders.Add(SellOrder);
            }

            if (!fx.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.FX
                    .Where(f => f.FXID == fx.FXID)
                    .DeleteAsync();

                await writer.DB.FX
                    .AddRangeAsync(fx);

                await writer.DB.FXBuyOrders
                    .AddRangeAsync(fx.BuyOrders);

                await writer.DB.FXSellOrders
                    .AddRangeAsync(fx.SellOrders);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves FX data
        /// </summary>
        /// <param name="include_buys">If buying orders should be included in the payload</param>
        /// <param name="include_sells">If selling orders should be included in the payload</param>
        /// <returns>OK</returns>
        [HttpGet("")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.FX>), "application/json")]
        public async Task<IActionResult> GetFX(
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true)
        {
            List<Model.FX> Results = new();
            using (var reader = DBAccess.GetReader())
            {
                var FXQuery = reader.DB.FX.AsQueryable();
                if (include_buys)
                {
                    FXQuery = FXQuery.Include(f => f.BuyOrders);
                }

                if (include_sells)
                {
                    FXQuery = FXQuery.Include(f => f.SellOrders);
                }

                Results = await FXQuery.ToListAsync();
            }

            return Ok(Results);
        }
    }
}
