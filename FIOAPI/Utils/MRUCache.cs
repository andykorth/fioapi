﻿namespace FIOAPI.Utils
{
    /// <summary>
    /// A Most Recently Used cache 
    /// </summary>
    /// <typeparam name="TKey">Key type</typeparam>
    /// <typeparam name="TValue">Value type</typeparam>
    public class MRUCache<TKey, TValue>
        where TKey : notnull
        where TValue : class
    {
        private readonly int capacity;

        /// <summary>
        /// Externally accessible LockObj
        /// </summary>
        public object LockObj
        {
            get; private set;
        } = new();

        private readonly Dictionary<TKey, LinkedListNode<MRUCacheItem<TKey, TValue>>> cache = new();
        private readonly LinkedList<MRUCacheItem<TKey, TValue>> mruList = new();

        /// <summary>
        /// Constructor that takes a capcity
        /// </summary>
        /// <param name="capacity">The number of entries to keep in the cache</param>
        /// <exception cref="ArgumentException"></exception>
        public MRUCache(int capacity)
        {
            if (capacity <= 0)
            {
                throw new ArgumentException("capacity must be larger than 0");
            }

            this.capacity = capacity;
        }

        /// <summary>
        /// Retrieves the top-most value
        /// </summary>
        /// <returns>The topmost value (or null)</returns>
        public TValue? Top()
        {
            lock (LockObj)
            {
                return mruList.First?.Value.value;
            }
        }

        /// <summary>
        /// Gets the provided value given a key
        /// </summary>
        /// <param name="key">The key to search</param>
        /// <returns>The value if present, default(T) otherwise</returns>
        public TValue? Get(TKey key)
        {
            lock (LockObj)
            {
                if (cache.TryGetValue(key, out var node))
                {
                    TValue value = node.Value.value;
                    // Mark it as MRU
                    mruList.Remove(node);
                    mruList.AddLast(node);
                    return value;
                }
            }

            return default;
        }

        /// <summary>
        /// Sets the given key and value pair in the cache
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void Set(TKey key, TValue value)
        {
            lock (LockObj)
            {
                if (cache.TryGetValue(key, out var item))
                {
                    mruList.Remove(item);
                }
                else if (cache.Count >= capacity)
                {
                    // Remove the first item (LRU) from the LinkedList
                    var firstNode = mruList.First;
                    mruList.RemoveFirst();
                    cache.Remove(firstNode!.Value.key);
                }

                var cacheItem = new MRUCacheItem<TKey, TValue>(key, value);
                var node = new LinkedListNode<MRUCacheItem<TKey, TValue>>(cacheItem);
                mruList.AddLast(node);
                cache[key] = node;
            }
        }

        /// <summary>
        /// Sets a list of key-value pairs
        /// </summary>
        /// <param name="keyValuePairs">List of key-value pairs</param>
        public void SetRange(List<Tuple<TKey, TValue>> keyValuePairs)
        {
            lock (LockObj)
            {
                keyValuePairs.ForEach(kvp =>
                {
                    Set(kvp.Item1, kvp.Item2);
                });
            }
        }

        /// <summary>
        /// Removes the given key from the cache
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>True if found, false otherwise</returns>
        public bool Remove(TKey key)
        {
            lock (LockObj)
            {
                if (cache.TryGetValue(key, out var item))
                {
                    mruList.Remove(item);
                    cache.Remove(item.Value.key);

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes all keys from the cache from the provided enumerable
        /// </summary>
        /// <param name="keys">Keys to remove</param>
        public void RemoveRange(IEnumerable<TKey> keys)
        {
            lock (LockObj)
            {
                foreach (var key in keys)
                {
                    Remove(key);
                }
            }
        }

        /// <summary>
        /// Clears the entirety of the cache
        /// </summary>
        public void Clear()
        {
            lock (LockObj)
            {
                mruList.Clear();
                cache.Clear();
            }
        }
    }

    internal class MRUCacheItem<TKey, TValue> where TValue : class
    {
        public MRUCacheItem(TKey key, TValue value)
        {
            this.key = key;
            this.value = value;
        }

        public TKey key;
        public TValue value;
    }
}
