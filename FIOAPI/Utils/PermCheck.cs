﻿using System;

namespace FIOAPI.Utils
{
    /// <summary>
    /// Permission enum
    /// </summary>
    [Flags]
    public enum Perm
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// Ship_Information
        /// </summary>
        Ship_Information = 1 << 0,

        /// <summary>
        /// Ship_Repair
        /// </summary>
        Ship_Repair = 1 << 1,

        /// <summary>
        /// Ship_Flight
        /// </summary>
        Ship_Flight = 1 << 2,

        /// <summary>
        /// Ship_Inventory
        /// </summary>
        Ship_Inventory = 1 << 3,

        /// <summary>
        /// Ship_FuelInventory
        /// </summary>
        Ship_FuelInventory = 1 << 4,

        /// <summary>
        /// Sites_Location
        /// </summary>
        Sites_Location = 1 << 5,

        /// <summary>
        /// Sites_Workforces
        /// </summary>
        Sites_Workforces = 1 << 6,

        /// <summary>
        /// Sites_Experts
        /// </summary>
        Sites_Experts = 1 << 7,

        /// <summary>
        /// Sites_Buildings
        /// </summary>
        Sites_Buildings = 1 << 8,

        /// <summary>
        /// Sites_Repair
        /// </summary>
        Sites_Repair = 1 << 9,

        /// <summary>
        /// Sites_Reclaimable
        /// </summary>
        Sites_Reclaimable = 1 << 10,

        /// <summary>
        /// Sites_ProductionLines
        /// </summary>
        Sites_ProductionLines = 1 << 11,

        /// <summary>
        /// Storage_Location
        /// </summary>
        Storage_Location = 1 << 12,

        /// <summary>
        /// Storage_Information
        /// </summary>
        Storage_Information = 1 << 13,

        /// <summary>
        /// Storage_Items
        /// </summary>
        Storage_Items = 1 << 14,

        /// <summary>
        /// Storage_ContractItems
        /// </summary>
        Storage_ContractItems = 1 << 15,

        /// <summary>
        /// Trade_Contract
        /// </summary>
        Trade_Contract = 1 << 16,

        /// <summary>
        /// Trade_CXOS
        /// </summary>
        Trade_CXOS = 1 << 17,

        /// <summary>
        /// Misc_ShipmentTracking
        /// </summary>
        Misc_ShipmentTracking = 1 << 18,

        /// <summary>
        /// Misc_LiquidCurrency
        /// </summary>
        Misc_LiquidCurrency = 1 << 19

    }

    /// <summary>
    /// Permissions checker
    /// </summary>
    public static class PermCheck
    {
        /// <summary>
        /// If we can see the data for the provided user information
        /// </summary>
        /// <param name="ThisUser">The current user</param>
        /// <param name="UserName">The user to look information up for</param>
        /// <param name="perm">Permission flags</param>
        /// <param name="dbAccess">[Optional] DBAccess object</param>
        /// <returns></returns>
        public static async Task<bool> CanSee(string ThisUser, string UserName, Perm perm, DBAccess? dbAccess = null)
        {
#if DEBUG
            if (ThisUser.ToLower() != ThisUser)
                throw new ArgumentException(nameof(ThisUser));

            if (UserName.ToLower() != UserName)
                throw new ArgumentException(nameof(UserName));
#endif

            DBAccess? reader = dbAccess;
            if (reader == null)
            {
                reader = DBAccess.GetReader();
            }

            bool CanSee = false;
            var PermissionModels = await reader.DB.Permissions
                .Where(p => p.GrantorUserName == UserName)
                .Where(p => p.GranteeUserName == "*" || p.GranteeUserName == ThisUser)
                .ToListAsync();
            if (PermissionModels.Any())
            {
                var permValues = Enum.GetValues(typeof(Perm))
                    .Cast<Perm>()
                    .Where(v => perm.HasFlag(v))
                    .ToList();

                if (permValues.Count > 0)
                {
                    foreach (var PermissionModel in PermissionModels)
                    {
                        bool PermModelCheck = true;
                        foreach (var permValue in permValues)
                        {
                            switch (permValue)
                            {
                                case Perm.Ship_Information:
                                    PermModelCheck &= PermissionModel.ShipInformation;
                                    break;
                                case Perm.Ship_Repair:
                                    PermModelCheck &= PermissionModel.ShipRepair;
                                    break;
                                case Perm.Ship_Flight:
                                    PermModelCheck &= PermissionModel.ShipFlight;
                                    break;
                                case Perm.Ship_Inventory:
                                    PermModelCheck &= PermissionModel.ShipInventory;
                                    break;
                                case Perm.Ship_FuelInventory:
                                    PermModelCheck &= PermissionModel.ShipFuelInventory;
                                    break;
                                case Perm.Sites_Location:
                                    PermModelCheck &= PermissionModel.SitesLocation;
                                    break;
                                case Perm.Sites_Workforces:
                                    PermModelCheck &= PermissionModel.SitesWorkforces;
                                    break;
                                case Perm.Sites_Experts:
                                    PermModelCheck &= PermissionModel.SitesExperts;
                                    break;
                                case Perm.Sites_Buildings:
                                    PermModelCheck &= PermissionModel.SitesBuildings;
                                    break;
                                case Perm.Sites_Repair:
                                    PermModelCheck &= PermissionModel.SitesRepair;
                                    break;
                                case Perm.Sites_Reclaimable:
                                    PermModelCheck &= PermissionModel.SitesReclaimable;
                                    break;
                                case Perm.Sites_ProductionLines:
                                    PermModelCheck &= PermissionModel.SitesProductionLines;
                                    break;
                                case Perm.Storage_Location:
                                    PermModelCheck &= PermissionModel.StorageLocation;
                                    break;
                                case Perm.Storage_Information:
                                    PermModelCheck &= PermissionModel.StorageInformation;
                                    break;
                                case Perm.Storage_Items:
                                    PermModelCheck &= PermissionModel.StorageItems;
                                    break;
                                case Perm.Storage_ContractItems:
                                    PermModelCheck &= PermissionModel.StorageContractItems;
                                    break;
                                case Perm.Trade_Contract:
                                    PermModelCheck &= PermissionModel.TradeContract;
                                    break;
                                case Perm.Trade_CXOS:
                                    PermModelCheck &= PermissionModel.TradeCXOS;
                                    break;
                                case Perm.Misc_ShipmentTracking:
                                    PermModelCheck &= PermissionModel.MiscShipmentTracking;
                                    break;
                                case Perm.Misc_LiquidCurrency:
                                    PermModelCheck &= PermissionModel.MiscLiquidCurrency;
                                    break;
                                default:
                                    throw new InvalidEnumArgumentException("Encountered unknown enum");
                            }
                        }

                        if (PermModelCheck)
                        {
                            CanSee = true;
                            break;
                        }
                    }
                }
            }

            if (dbAccess == null)
            {
                // We need to dispose the created reader
                reader.Dispose();
                reader = null;
            }

            return CanSee;
        }
    }
}
