﻿namespace FIOAPI.Utils
{
    /// <summary>
    /// A GlobalMutex class used to enforce code is only run in a single process
    /// </summary>
    public class GlobalMutex : IDisposable
    {
        private bool bHasHandle = false;
        private Mutex globalMutex;

        /// <summary>
        /// GlobalMutex constructor
        /// </summary>
        /// <param name="guid">guid of the mutex</param>
        /// <param name="mutexIdSuffix">A suffix to use on the mutex</param>
        /// <param name="timeoutMs">The timeout in seconds, specify negative value for infinite</param>        
        public GlobalMutex(string guid, string? mutexIdSuffix = null, int timeoutMs = 5000)
        {
            string mutexId = @"Global\" + guid;

            if (!string.IsNullOrEmpty(mutexIdSuffix))
            {
                mutexId += $"-{mutexIdSuffix}";
            }

            globalMutex = new Mutex(false, mutexId);

            try
            {
                if (timeoutMs < 0)
                {
                    bHasHandle = globalMutex.WaitOne(Timeout.Infinite, false);
                }
                else
                {
                    bHasHandle = globalMutex.WaitOne(timeoutMs, false);
                }
            }
            catch (AbandonedMutexException)
            {
                bHasHandle = true;
            }
        }

        /// <summary>
        /// If the mutex was acquired
        /// </summary>
        /// <returns>True if acquired, false otherwise</returns>
        public bool Acquired()
        {
            return bHasHandle;
        }

        #region IDisposable interface
        /// <summary>
        /// Dispose interface
        /// </summary>
        public void Dispose()
        {
            if (globalMutex != null)
            {
                if (bHasHandle)
                {
                    globalMutex.ReleaseMutex();
                    bHasHandle = false;
                }

                globalMutex.Close();
            }
        }
        #endregion
    }
}