﻿namespace FIOAPI
{
    /// <summary>
    /// Base class for implementing validation support
    /// </summary>
    public class IValidation
    {
        /// <summary>
        /// Runs validation
        /// </summary>
        /// <param name="ThrowOnValidationFailure">If we should throw on validation failure [default: false]</param>
        /// <returns>true on success, false otherwise</returns>
        /// <exception cref="InvalidOperationException">Thrown if ThrowOnValidationFailure is true and validation fails</exception>
        public bool RunValidation(bool ThrowOnValidationFailure = false)
        {
            try
            {
                var Errors = new List<string>();
                Validate(ref Errors);

                if (Errors.Count > 0)
                {
                    var ResultExceptionText = new StringBuilder();
                    ResultExceptionText.AppendLine("Validation failed with:");
                    Errors.ForEach((e) =>
                    {
                        ResultExceptionText.AppendLine($"\t {e}");
                    });

                    System.Diagnostics.Debug.WriteLine(ResultExceptionText.ToString());
                    Log.Warning(ResultExceptionText.ToString());
                    if (ThrowOnValidationFailure)
                    {
#if DEBUG
                        System.Diagnostics.Debugger.Break();
#endif // DEBUG
                        throw new InvalidOperationException(ResultExceptionText.ToString());
                    }
                }

                return Errors.Count == 0;
            }
            catch(Exception ex)
            {
#if DEBUG
                Log.Error(ex.ToString());
                System.Diagnostics.Debugger.Break();
#endif // DEBUG
                throw;
            }
        }

        /// <summary>
        /// Run Validation and throw on error
        /// </summary>
        public void RunValidation_Throw() => RunValidation(true);

        /// <summary>
        /// If it passes validation
        /// </summary>
        /// <returns></returns>
        public bool PassesValidation() => RunValidation(false);

        /// <summary>
        /// Run validation and return true if it passes, but throw an exception if there's a error which 
        /// can be read in the test running and then fixed by a human, rather than throwing away the
        /// validation error.
        /// </summary>
        /// <returns></returns>
        public bool PassesValidation_Throw() => RunValidation(true);

        /// <summary>
        /// An overridable CustomValidation step for an IValidation
        /// </summary>
        /// <param name="Errors">Errors to fill out</param>
        /// <param name="Context">The context (path of the variable)</param>
        public virtual void CustomValidation(ref List<string> Errors, string Context)
        {
            // Intentionally empty
        }

        private void InternalValidate(ref List<string> Errors, ref HashSet<object> ProcessedObjects, string Context)
        {
            // Prevent infinite recursion
            if (ProcessedObjects.Contains(this))
                return;

            ProcessedObjects.Add(this);

            var ActiveContext = Context;
            var propInfos = this.GetType().GetProperties();
            foreach (var propInfo in propInfos)
            {
                // Skip if we have the NoValidateAttribute on this
                bool SkipValidation = propInfo.GetCustomAttributes(true).Any(a => a is NoValidateAttribute);
                if (SkipValidation)
                {
                    continue;
                }

                var PropInfoContext = $"{Context}.{propInfo.Name}";
                ActiveContext = PropInfoContext;

                propInfo.RunValidationOnPropInfo(this, ref Errors, ActiveContext);

                var propObj = propInfo.GetValue(this);
                bool IsEnumerableOfIValidation = propObj != null
                    && propInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable))
                    && propInfo.PropertyType.GetGenericArguments().Any()
                    && propInfo.PropertyType.GetGenericArguments()[0].IsSubclassOf(typeof(IValidation));
                bool IsArrayOfIValidation = propObj != null
                    && propInfo.PropertyType.IsArray
                    && propInfo.PropertyType.GetElementType()!.IsSubclassOf(typeof(IValidation));
                if (IsEnumerableOfIValidation || IsArrayOfIValidation)
                {
                    // Enumerate and run validation
                    int elemIdx = 0;
                    var elements = (IEnumerable<IValidation>)propInfo.GetValue(this, null)!;
                    foreach (var element in elements)
                    {
                        ActiveContext = $"{PropInfoContext}[{elemIdx++}]";
                        element.InternalValidate(ref Errors, ref ProcessedObjects, ActiveContext);
                        element.CustomValidation(ref Errors, ActiveContext);
                    }
                }

                ActiveContext = PropInfoContext;
                var propValue = propInfo.GetValue(this);
                if (propValue != null && propValue is IValidation)
                {
                    var propValidation = (IValidation)propValue!;
                    propValidation.InternalValidate(ref Errors, ref ProcessedObjects, ActiveContext);
                    propValidation.CustomValidation(ref Errors, ActiveContext);
                }
            }

            ActiveContext = Context;
            CustomValidation(ref Errors, ActiveContext);
        }

        /// <summary>
        /// Runs validation work for a given model
        /// </summary>
        /// <param name="Errors">A list of errors</param>
        /// <returns>True if no errors, false otherwise</returns>
        public bool Validate(ref List<string> Errors)
        {
            HashSet<object> ProcessedObjects = new();

            try
            {
                InternalValidate(ref Errors, ref ProcessedObjects, "this");
                return Errors.Count == 0;
            }
            catch(Exception ex)
            {
#if DEBUG
                Log.Error(ex.ToString());
                System.Diagnostics.Debugger.Break();
#endif // DEBUG
                throw;
            }
        }
    }
}
