﻿using System.Diagnostics;
using System.Text.Json.Nodes;

#if DEBUG
namespace FIOAPI.FIODebug
{
    /// <summary>
    /// An object that represents a generic payload (usually retrieved via /debug).
    /// What this does is read in a JsonObject and will generate a FIOJsonObject from that data.
    /// It will internally sort all fields and then generate a unique string identifier based on the fields
    /// for this object.  This way, we can group payloads by this unique identifier, dump similar payloads into
    /// their respective identifier, compare versions of the game based on the unique identifier, etc.
    /// </summary>
    public class FIOJsonObject
    {
        /// <summary>
        /// Key name
        /// </summary>
        public string NodeKey { get; private set; } = "";

        /// <summary>
        /// ValueKind for this node
        /// </summary>
        public JsonValueKind ValueKind { get; private set; }

        /// <summary>
        /// The JsonObject
        /// </summary>
        public JsonObject JsonObj { get; private set; } = null!;

        /// <summary>
        /// Children
        /// </summary>
        public List<FIOJsonObject> Children { get; private set; } = new List<FIOJsonObject>();

        /// <summary>
        /// The full identifier for this node and all children nodes
        /// </summary>
        public string Identifier 
        { 
            get
            {
                if (_Identifier == null)
                {
                    var sb = new StringBuilder();
                    sb.Append(NodeKey);
                    sb.Append("{");
                    sb.Append(string.Join(";", Children.Select(c => c.Identifier)));
                    sb.Append("}");

                    _Identifier = sb.ToString();
                }
                
                return _Identifier;
            }
        }
        private string? _Identifier = null;

        /// <summary>
        /// A folder hash
        /// </summary>
        public string FolderHash
        {
            get
            {
                var hashCode = Identifier.GetHashCode();
                if (hashCode < 0)
                {
                    hashCode = -hashCode;
                }

                return $"{hashCode}";
            }
        }

        /// <summary>
        /// Loads a JsonObject recursively and generates each child node
        /// </summary>
        public void Load(JsonObject jsonObj)
        {
            NodeKey = "__root";
            ValueKind = JsonValueKind.Object;
            JsonObj = jsonObj;

            var fieldNames = jsonObj.Select(p => p.Key).ToList();
            foreach (var fieldName in fieldNames)
            {
                var field = new FIOJsonObject();
                field.LoadInternal(jsonObj[fieldName]!, fieldName, ref field);
                Children.Add(field);
            }

            Children = Children.OrderBy(c => c.NodeKey).ToList();
        }

        private void LoadInternal(JsonNode jsonNode, string NodeName, ref FIOJsonObject fioJsonObj)
        {
            NodeKey = NodeName;
            ValueKind = DetermineJsonValueKind(jsonNode);
            if (ValueKind == JsonValueKind.Object)
            {
                var jsonObj = jsonNode.AsObject();
                var fieldNames = jsonObj.Select(p => p.Key).ToList();
                foreach (var fieldName in fieldNames)
                {
                    var field = new FIOJsonObject();
                    field.LoadInternal(jsonObj[fieldName]!, fieldName, ref field);
                    Children.Add(field);
                }
                Children = Children.OrderBy(c => c.NodeKey).ToList();
            }
            else if (ValueKind == JsonValueKind.Array)
            {
                JsonArray jsonArray = jsonNode.AsArray();
                if (jsonArray.Any())
                {
                    JsonNode inner = jsonArray.First()!;
                    var field = new FIOJsonObject();
                    field.LoadInternal(inner, "__ARRAY", ref field);
                    Children.Add(field);
                }
                Children = Children.OrderBy(c => c.NodeKey).ToList();
            }
        }

        private static JsonValueKind DetermineJsonValueKind(JsonNode jsonNode)
        {
            if (jsonNode is JsonArray)
            {
                return JsonValueKind.Array;
            }
            else if (jsonNode is JsonObject)
            {
                return JsonValueKind.Object;
            }
            else if (jsonNode == null)
            {
                return JsonValueKind.Null;
            }
            else
            {
                Debug.Assert(jsonNode is JsonValue);
                return jsonNode.GetValue<JsonElement>().ValueKind;
            }
        }
    }
}
#endif // DEBUG