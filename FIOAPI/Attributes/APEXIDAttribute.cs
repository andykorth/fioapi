﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute signifying this is an APEX ID
    /// APEX ID is a lowercase 32 character hex string
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class APEXIDAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public APEXIDAttribute()
        {

        }
    }
}
