﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce a field is a ticker (1-4 characters inclusive, all uppercase)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class TickerAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TickerAttribute() { }
    }
}
