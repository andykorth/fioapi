﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce the Guid is valid
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class GuidValidAttribute : Attribute
    {
        /// <summary>
        /// GuidValidAttribute constructor
        /// </summary>
        public GuidValidAttribute() { }
    }
}
