﻿//#define DISABLE_ALL_CACHING

namespace FIOAPI.Attributes
{
    /// <summary>
    /// Specifies how long the response should be cached
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CacheAttribute : ResponseCacheAttribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CacheAttribute()
        {
#if DISABLE_ALL_CACHING
            NoStore = true;
#endif // DISABLE_ALL_CACHING

            // Disable caching when testing
            if (Globals.IsTesting)
            {
                NoStore = true;
            }

            Duration = 0;
            VaryByQueryKeys = new string[] { "*" };
        }

        /// <summary>
        /// How many days to cache for
        /// </summary>
        public int Days
        {
            get
            {
                return Duration / 24 / 60 / 60;
            }
            set
            {
                Duration += (value * 24 * 60 * 60);
            }
        }

        /// <summary>
        /// How many hours to cache for
        /// </summary>
        public int Hours
        {
            get
            {
                return Duration / 60 / 60;
            }
            set
            {
                Duration += (value * 60 * 60);
            }
        }

        /// <summary>
        /// How many minutes to cache for
        /// </summary>
        public int Minutes
        {
            get
            {
                return Duration / 60;
            }
            set
            {
                Duration += (value * 60);
            }
        }

        /// <summary>
        /// How many seconds to cache for
        /// </summary>
        public int Seconds
        {
            get
            {
                return Duration;
            }
            set
            {
                Duration += value;
            }
        }
    }
}
