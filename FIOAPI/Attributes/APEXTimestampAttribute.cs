﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to indicate that the provide long is a valid timestamp
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class APEXTimestampAttribute : Attribute
    {
        /// <summary>
        /// APEXTimestampAttribute constructor
        /// </summary>
        public APEXTimestampAttribute()
        {

        }
    }
}
