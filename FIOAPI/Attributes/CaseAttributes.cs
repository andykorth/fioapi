﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce the string is fully lowercase
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class LowercaseAttribute : Attribute
    {
        /// <summary>
        /// LowercaseAttribute Constructor
        /// </summary>
        public LowercaseAttribute() { }
    }

    /// <summary>
    /// An attribute to enforce the string is fully uppercase
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class UppercaseAttribute : Attribute
    {
        /// <summary>
        /// UppercaseAttribute Constructor
        /// </summary>
        public UppercaseAttribute() { }
    }
}
