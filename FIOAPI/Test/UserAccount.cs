﻿using Z.EntityFramework.Plus;

// @TODO: Restrict this class to only be able to be used when running tests!
namespace FIOAPI.Test
{
    /// <summary>
    /// A test user account class
    /// </summary>
    public class UserAccount : IDisposable
    {
        /// <summary>
        /// The username
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The display username
        /// </summary>
        public string DisplayUserName { get; set; }

        /// <summary>
        /// The password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// If the account is an admin
        /// </summary>
        public bool Admin { get; set; }

        /// <summary>
        /// A writable API key
        /// </summary>
        public string UserWriteAPIKey { get; set; }

        /// <summary>
        /// A read-only API key
        /// </summary>
        public string UserReadOnlyAPIKey { get; set; }

        /// <summary>
        /// Constructs a test UserAccount
        /// </summary>
        /// <param name="UserName">UserName</param>
        /// <param name="Password">Password</param>
        /// <param name="Admin">Admin</param>
        public UserAccount(string UserName, string Password, bool Admin = false)
        {
            this.UserName = UserName.ToLower();
            this.DisplayUserName = UserName;
            this.Password = Password;
            this.Admin = Admin;

            using (var writer = DBAccess.GetWriter())
            {
                var userModel = new Model.User()
                {
                    UserName = UserName.ToLower(),
                    DisplayUserName = UserName,
                    PasswordHash = SecurePasswordHasher.Hash(Password),
                    IsAdmin = Admin
                };
                userModel.RunValidation_Throw();
                writer.DB.Users.Add(userModel);

                // Write APIKey
                var WriteAPIKey = new Model.APIKey
                {
                    Key = Guid.NewGuid(),
                    UserName = UserName.ToLower(),
                    Application = "Write",
                    AllowWrites = true,
                    CreateTime = DateTime.UtcNow
                };
                WriteAPIKey.RunValidation_Throw();
                writer.DB.APIKeys.Add(WriteAPIKey);
                UserWriteAPIKey = WriteAPIKey.Key.ToString("N");

                // Read-only APIKey
                var ReadOnlyAPIKey = new Model.APIKey
                {
                    Key = Guid.NewGuid(),
                    UserName = UserName.ToLower(),
                    Application = "Read",
                    AllowWrites = false,
                    CreateTime = DateTime.UtcNow
                };
                ReadOnlyAPIKey.RunValidation_Throw();
                writer.DB.APIKeys.Add(ReadOnlyAPIKey);
                UserReadOnlyAPIKey = ReadOnlyAPIKey.Key.ToString("N");

                writer.DB.SaveChanges();
            }
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            using (var writer = DBAccess.GetWriter())
            {
                // Delete all the things
                // @TODO: Delete more
                // User.ResetAllData();
                var user = writer.DB.Users.First(u => u.UserName == UserName);
                writer.DB.Users.Remove(user);
                writer.DB.APIKeys.Where(ak => ak.UserName == UserName).Delete();
                writer.DB.Groups.Where(g => g.GroupOwner == UserName).Delete();
                writer.DB.SaveChanges();
            }
        }
    }
}
