# Controllers

Controllers in ASP.NET Core are simply "groupings of endpoints" or how you handle requests like `https://api.fnar.net/foo/bar`. You create a controller and then create methods on the controller with the appropriate attributes for relevant http routes.

## Adding a controller
For instructional purposes, we'll be creating a root `/widget` endpoint.

1. Create a new `WidgetController.cs` file in the `Controllers` directory
2. Create a class that derives off `ControllerBase`
3. Add the `[ApiController]` and `[Route("/widget")]` attributes to the class
4. Create a method for each endpoint
5. Add the appropriate `Http*` attribute to each method
6. The method must be of one of the two forms:
   1. `public IActionResult MethodName(...)`
   2. `public async Task<IActionResult> MethodNameAsync(...)`
7. Add as many [attributes](#attributes) as are relevant

## Attributes
- `HttpGet`, `HttpPost`, `HttpPut`, or `HttpDelete`
   - This is **required** if you wish the method to route
   - The parameter to the attribute is the path as well as any parameters in braces
   - Examples:
```csharp
[HttpGet("")] 				// Invoked for GET of https://api.fnar.net/widget
[HttpGet("list")] 			// Invoke for GET of https://api.fnar.net/widget/list
[HttpGet("item/{itemid}")]  // Invoke for GET of https://api.fnar.net/widget/item/{AnyString}. Note: {itemid: string} does not work
[HttpGet("id/{id: int}")]	// Invoke for GET of https://api.fnar.net/widget/id/{AnyInteger}
[HttpPut("omg/wtf")]		// Invoke for PUT of https://api.fnar.net/widget/omg/wtf
[HttpPost("wtf/bbq")]		// Invoke for POST of https://api.fnar.net/widget/wtf/bbq
[HttpDelete("bbq/bbbq")]	// Invoke for DELETE of https://api.fnar.net/widget/bbq/bbbq
```

- `AllowAnonymous` or `Authorize(Policy = AuthPolicy.[User|Admin][Read|Write])`
   - This is **required** for all endpoints.
   - AllowAnonymous allows unauthorized access
   - Authorize requires the specified auth policy be active for use.
   - If using a `UserWrite` policy, you must also add SwaggerResponse attributes (for documentation purposes) for 401 and 403.
      - `[SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]`
      - `[SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]`
   - If using a `Userread` policy, you only need to add a SwaggerResponse attribute for 401 (above)

- `HydrationTimeoutFilter`
   - This is necessary when receiving game-data payloads
   - This attribute indicates that the request body should be scanned for any instances of `Hydration Timeout [`
   - The above string is an indication that the servers are being slow and not "hydrating" the appropriate data, so the data should be discarded
   - It returns 200 (OK) if it encountered a HydrationTimeout and simply returns
   
- `SwaggerResponse`
   - A swagger response should be present for every possible status code the method can return.
   - Swagger response arguments, in order, are:
      - `StatusCodes` enum
	  - A string representing the summary of that status code
	  - [If applicable] The typeof the response
	     - `typeof(Model.Widget)` or `typeof(List<Model.Widget>)`
      - Response format as a string
	     - `"application/json"`
		 - `"text/plain"`
		 
- `Cache`
   - This indicates to CloudFlare that the response from this endpoint should be cached
   - ResponseCache **only** works with `GET` and `[AllowAnonymous]`
   - Examples:
      - `[Cache(Seconds = 45)]`
      - `[Cache(Minutes = 15)]`
	  - `[Cache(Hours = 4, Minutes = 30)]`
	  - `[Cache(Days = 1)]`
		 
## Accepting a body
If the endpoint should receive a body, it should be either a POST or PUT and you simply add a parameter that is prefixed with a `[FromBody]` attribute.  Example:
```csharp
[HttpPost("work/{id: int}")]
[Authorize(Policy = AuthPolicy.UserWrite)]
[SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
[SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
[SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
[SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
public async Task<IActionResult> WorkAsync(
	int id,
	[FromBody] Payloads.Widget.WORK_DATA WorkDataPayload)
{
	// Simply use WorkDataPayload object
}
```

## Accepting GET parameters
If the endpoint should accept GET parameters, it should be a GET and you add each parameter in the query that is prefixed with `[FromQuery(Name = "name")]. Example:
```csharp
[HttpGet("query/{type}")]
[Authorize(Policy = AuthPolicy.UserRead)]
[SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
[SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
public async Task<IActionResult> QueryAsync(
	string type,
	[FromQuery(Name = "substring_match")] string substring_match,
	[FromQuery(Name = "include_companies")] bool include_companies = true)
{
	// Use any of the parameters
}
```

## Comments
All methods should be prefixed with C# style commenting. These comments will be added to swagger documentation.

## Payload Validation
In pretty much all cases, any body class should be its own class and that class should inhertit from IValidation, adding as many attributes as makes sense to each field. After that, payloads should be validated and if they fail, the errors returned.  Example:
```csharp
[HttpPost("work/{id: int}")]
[Authorize(Policy = AuthPolicy.UserWrite)]
[SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
[SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
[SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
[SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
public async Task<IActionResult> WorkAsync(
	int id,
	[FromBody] Payloads.Widget.WORK_DATA WorkDataPayload)
{
	var Errors = new List<string>();
	if (id <= 0)
	{
		Errors.Add($"id with value '{id}' is <= 0, which is not permitted because bbqs are not 0 or negative.");
		return BadRequest(Errors);
	}
	
	if (!WorkDataPayload.Validate(ref Errors))
	{
		return BadRequest(Errors);
	}
	
	// Rest of logic
}
```