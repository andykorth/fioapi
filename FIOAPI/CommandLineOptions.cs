﻿#pragma warning disable 1591
namespace FIOAPI
{
    public class CommandLineOptions
    {
        [Option('m', "migrate", Required = false, HelpText = "Run database migrations")]
        public bool ApplyMigration { get; set; }

        [Option('r', "run-after-migrate", Required = false, HelpText = "Runs immediately after applying a migration")]
        public bool RunAfterMigrate { get; set; }

        [Option('d', "debug-database", Required = false, HelpText = "Debug database operations")]
        public bool DebugDatabase { get; set; }

        [Option('o', "override-dbtype", Required = false, HelpText = "Override the config file's database-type.  Used in migrations.")]
        public string OverrideDBType { get; set; } = "";

        [Option("applicationName", Required = false)]
        public string? applicationName { get; set; }

        [Option("contentRoot", Required = false)]
        public string? contentRoot { get; set; }

        [Option("environment", Required = false)]
        public string? environment { get; set; }

        [Option("urls", Required = false)]
        public string? urls { get; set; }

        [Option("testing", Required = false)]
        public bool Testing { get; set; }

        [Option("create-admin-account-username", Required = false)]
        public string CreateAdminAccountUserName { get; set; } = "";

        [Option("create-admin-account-password", Required = false)]
        public string CreateAdminAccountPassword { get; set; } = "";

        public static IEnumerable<Example> Examples
        {
            get
            {
                return new List<Example>()
                {
                    new Example("Runs FIOAPI server", new CommandLineOptions {})
                };
            }
        }

        internal static int RunOptions(CommandLineOptions o)
        {
            Globals.ApplyMigration = o.ApplyMigration;
            Globals.RunAfterMigrate = o.RunAfterMigrate;
            Globals.DebugDatabase = o.DebugDatabase;
            Globals.IsTesting = o.Testing;
            Globals.OverrideDBType = o.OverrideDBType;

            var DebugOut = new StringBuilder();
            DebugOut.AppendLine("Options:");
            DebugOut.AppendLine($"\t Migrate: {Globals.ApplyMigration}");
            DebugOut.AppendLine($"\t RunAfterMigrate: {Globals.RunAfterMigrate}");
            DebugOut.AppendLine($"\t DebugDatabase: {Globals.DebugDatabase}");
            DebugOut.AppendLine($"\t OverrideDBType: {Globals.OverrideDBType}");
            Log.Debug(DebugOut.ToString());

            return 0;
        }

        internal static int HandleParseError(IEnumerable<CommandLine.Error> errs)
        {
            Log.Debug("CmdLine parse errors {0}", errs.Count());

            if (errs.Any(x => x is HelpRequestedError || x is VersionRequestedError))
            {
                return -1;
            }
                
            if (errs.All(x => x.Tag == ErrorType.UnknownOptionError))
            {
                return 0;
            }

            foreach (var err in errs)
            {
                Log.Error($"CmdLine parse error: {err}: {err.GetType()}");
            }

            Log.Debug("Parse error encountered.");
            return -2;
        }
    }
}
#pragma warning restore 1591
