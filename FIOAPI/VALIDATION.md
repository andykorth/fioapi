# Validation
Validation is a process of ensuring that the data for a class meets expectations. The idea is to use as many attributes as possible on each field to ensure the data matches expectations.

## Validation 
There are a number of attributes available for use that help ensure that the values of a model (and payloads) are properly set. You should set as many attributes as possible where it makes sense. 

### Validation Attributes
Any class that derives off IValidation can use these attributes:

- **APEXID** - Indicates that this string must be 32 characters in length and lowercase hexadecimal (matching the APEXID format).
   - Usage: `[APEXID]`
   - NOTE: When doing APEXID on a Model, make sure to specify `[StringLength(32, MinimumLength = 32)]` as well, otherwise the database won't be optimized for the appropriate size
- **Ticker** - Indicates that this string is between 1 and 4 characters and is all uppercase letters
   - Usage: `[Ticker]`
   - NOTE: When doing Ticker on a Model, make sure to specify `[StringLength(4, MinimumLength = 1)]` as well, otherwise the database won't be optimized for the appropriate size
- **NaturalID** - Indicates that this string is a NaturalId of the format `HRT`, `ZZ-123`, or `ZZ-123z`
   - NOTE: When doing NaturalID on a Model, make sure to specify `[StringLength(7, MinimumLength = 3)]` as well, otherwise the database won't be optimized for the appropriate size
- **GuidValid** - Indicates that this guid must be valid (not full of 0s)
   - Usage: `[GuidValid]`
- **Lowercase** - Indicates that the string must be all lowercase. Only applicable to strings.
   - Usage: `[Lowercase]`
- **Uppercase** - Indicates that the string must be all uppercase. only applicable to strings.
   - Usage: `[Uppercase]`
- **CurrencyCode** - Indicates that the string must be a currency code.  `AIC`, `CIS`, `ICA`, `NCC`, `ECD`
   - Usage: `[CurrencyCode]`
- **FXTicker** - Indicates that the string is a FX ticker. `XXX/YYY`
   - Usage: `[FXTicker]`
- **StringLength** - Indicates that the string must be within the StringLength maximum size. Can optionally specify a MinimumLength.  *This also enforces it so that the string column in the database cannot exceed the maximum size.*
   - Usage: `[StringLength(100)]`, `[StringLength(32, MinimumLength=32)]`, `[StringLength(50, MinimumLength=10)]`
- **Range** - Indicates that the primitive type must be within the specified range.
   - Usage: `[Range(1,4)]`, `[Range(0, int.MaxValue)]`
- **NoValidate** - Indicates that the property in question shouldn't be validated. If specified on an object type deriving from IValidation, that object's children will also not be validated.
   - Usage: `[NoValidate]`

**NOTE**: Any object fields which are not nullable by suffixing the type with `?` are assumed to be non-null! If set to null, they will **fail validation**. Any type that is not specified as nullable will be marked as `NOT NULL` in the database as well.

### Other Attributes
- **JsonIgnore** - Indicates that this field won't be serialized out to json.
   - Usage: `[JsonIgnore]`
- **DefaultValue** - Indicates to documentation that this value is defaulted to the provided value. You should generally only use this for payloads the client sends to the server.
   - Usage: `[DefaultValue(true)]`, `[DefaultValue("ApplicationName")]`


### Custom Validation
To add custom validation to your class:

- Add a method `public override void CustomValidation(ref List<string> Errors)`
- Do the custom validation, adding entries to `Errors` on failures

Example:
```csharp
public class Widget : IValidation 
{
	public int Value { get; set; } = 42;
	
   /// <summary>
   /// An overridable CustomValidation step for an IValidation
   /// </summary>
   /// <param name="Errors">Errors to fill out</param>
   /// <param name="Context">The context (path of the variable)</param>
	public override void CustomValidation(ref List<string> Errors, string Context)
	{
		if (Value < 0)
		{
			Errors.Add($"{Context} cannot be negative. Current value: {Value})");
		}
		else
		{
			if (Value >= 43 && Value <= 53)
			{
				Errors.Add($"{Context} cannot be in the range [43,53]. Current value: {Value}");
			}
		}
	}
}
```
   
### Running Validation
Validation should be run under various scenarios:
- On input payloads, to ensure that the data provided by the client is valid
- On a created/modified model just prior to inserting/updating data
- Whenever you need to verify the data is valid

Options:

- `bool bPassesValidation = MyWidgetObj.RunValidation(ThrowOnValidationFailure: false);`
   - If ThrowOnValidationFailure is true: Runs validation and throws an InvalidOperationException if any validation failures occur
   - If ThrowOnValidationFailure is false: Runs validation and returns false if any validation failures occur, true otherwise   
- `MyWidgetObj.RunValidation_Throw();`
   - An alias for `RunValidation(true)`
- `MyWidgetObj.PassesValidation()`
   - An alias for `RunValidation(false)`
- `MyWidgetObj.Validate(ref ErrorsList)`
   - Runs validation and returns a `List<string>` of the errors.  If the list count is 0, no validation errors occurred. 
   - Also returns true when ErrorsList.Count is 0.