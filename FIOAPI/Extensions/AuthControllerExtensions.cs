﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// Helper extensions for AuthControllers
    /// </summary>
    public static class AuthControllerExtensions
    {
        /// <summary>
        /// Determines if the user is logged in
        /// </summary>
        /// <param name="controller">The implicit ControllerBase</param>
        /// <returns>true if logged in, false otherwise</returns>
        public static bool IsLoggedIn(this ControllerBase controller)
        {
            return GetUserName(controller) != null;
        }

        /// <summary>
        /// Retrieves the logged in user
        /// </summary>
        /// <param name="controller">The implict ControllerBase</param>
        /// <returns>The username, null if not logged in</returns>
        public static string? GetUserName(this ControllerBase controller)
        {
            var identity = controller.HttpContext.User.Identity as ClaimsIdentity;
            if (identity != null)
            {
                return identity.FindFirst("username")?.Value;
            }

            return null;
        }

        /// <summary>
        /// Returns the validation error string
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <returns>A string to pass into BadRequest</returns>
        public static string GetValidationErrorString(this List<string> Errors)
        {
            return $"Failed to validate payload. Errors: {Environment.NewLine}{string.Join($"{Environment.NewLine}\t", Errors)}";
        }
    }
}
