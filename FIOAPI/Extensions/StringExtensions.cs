﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// Helper string extensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Conditionally wraps the given string in quotation marks
        /// </summary>
        /// <param name="str">the string to wrap</param>
        /// <returns>the resultant quoted string</returns>
        public static string QuoteWrap(this string str)
        {
            if (!str.StartsWith("\"") || !str.EndsWith("\""))
            {
                return $"\"{str}\"";
            }

            return str;
        }
    }
}
