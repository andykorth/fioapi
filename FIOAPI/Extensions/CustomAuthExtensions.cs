﻿using System.Configuration;

namespace FIOAPI.Extensions
{
    /// <summary>
    /// CustomAuthExtension
    /// </summary>
    public static class CustomAuthExtensions
    {
        /// <summary>
        /// AddFIOCustomAuthentication
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="configuration">configuration</param>
        /// <returns>IServiceCollection</returns>
        public static IServiceCollection AddFIOCustomAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtSettings = JwtSettings.FromConfiguration(configuration);
            services.AddSingleton(jwtSettings);
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => options.TokenValidationParameters = jwtSettings.TokenValidationParameters);
            services
                .AddAuthentication(APIKeyAuthDefaults.AuthenticationScheme)
                .AddScheme<APIKeyAuthOptions, APIKeyAuthHandler>(APIKeyAuthDefaults.AuthenticationScheme, opt => { });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthPolicy.UserRead, policy => 
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.AuthenticationSchemes.Add(APIKeyAuthDefaults.AuthenticationScheme);
                    policy.RequireClaim("usertype");
                });

                options.AddPolicy(AuthPolicy.UserWrite, policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.AuthenticationSchemes.Add(APIKeyAuthDefaults.AuthenticationScheme);
                    policy.RequireClaim("usertype", new string[] { "write" });
                });

                options.AddPolicy(AuthPolicy.AdminRead, policy => 
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.AuthenticationSchemes.Add(APIKeyAuthDefaults.AuthenticationScheme);
                    policy.RequireClaim("admintype");
                });

                options.AddPolicy(AuthPolicy.AdminWrite, policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.AuthenticationSchemes.Add(APIKeyAuthDefaults.AuthenticationScheme);
                    policy.RequireClaim("admintype", new string[] { "write" });
                });
            });

            return services;
        }
    }
}
