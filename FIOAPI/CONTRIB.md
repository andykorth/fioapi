# Contributing

Contributions are taken using Pull Requests (or as GitLab calls them, "Merge Requests"). Effectively, you should create a fork of FIOAPI, make changes in that fork, and then submit a merge request back into FIOAPI.

## Platforms/OS
Any platform/OS that supports .NET 7 should be able to develop for FIOAPI. Documentation is currently written with Windows in mind since that is what the core developers are using.

You will need to install Entity Framework Core tools to create database migrations. Use: `dotnet tool install --global dotnet-ef` 

## Code Conventions
In most cases, following what VS does is sufficient. To be explicit, however, the following list should be followed:

- Use braces for all conditions/loops. Braces should be in in the [Allman](https://en.wikipedia.org/wiki/Indentation_style#Allman_style) style
- Conditionals, loops, control keywords should be suffixed with a space prior to the parenthesis.  Examples: `for (...)`, `if (...)`, `using (...)`
- Prefer async/await where possible
- Try to limit lines to 150 characters
- Prefer easy to understand code which is slower over faster complex code
- All fields and public methods should be documented using C# documentation comments
   - The only exception to this rule is APEX payloads
   - For those, wrap the entire namespace/class in `#pragma warning disable 1591` and `#pragma warning restore 1591`
- `using` statements should be alphabetically ordered, however, they should be grouped in this order:
   - System.\* at the top
   - Microsoft.\* below them
   - FIOAPI.\* below that
   - Everything else after
- Unless there is a single LINQ statement, each LINQ statement should be on its own line prefixed with `.` and indented once. Examples:
```csharp
// Single line example:
var Widget = ListOfWidgets.FirstOrDefault(w => w.Name == "Foo");

// Multi-line example:
var MatchingWidgets = await reader.DB.ListOfWidgets
	.Where(w => w.Name.StartsWith("Widget#"))			// Only grab widgets that are explicitly labeled with a #
	.Where(w => w.Id > 500)								// Only ids greater than 500
	.Include(w => w.Customers)							// Bring in the customers sub-object
		.ThenInclude(w => w.Companies)					// Bring in the companies from the customers subobject
	.ToListAsync();										// Convert to list				
```