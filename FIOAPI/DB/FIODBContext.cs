﻿namespace FIOAPI.DB
{
    /// <summary>
    /// The main DBContext
    /// </summary>
    public class FIODBContext : DbContext
    {
        /// <summary>
        /// APIKeys
        /// </summary>
        public DbSet<Model.APIKey> APIKeys { get; set; }

        /// <summary>
        /// Buildings
        /// </summary>
        public DbSet<Model.Building> Buildings { get; set; }

        /// <summary>
        /// BuildingCosts
        /// </summary>
        public DbSet<Model.BuildingCost> BuildingCosts { get; set; }

        /// <summary>
        /// BuildingRecipes
        /// </summary>
        public DbSet<Model.BuildingRecipe> BuildingRecipes { get; set; }

        /// <summary>
        /// BuildingRecipeInputs
        /// </summary>
        public DbSet<Model.BuildingRecipeInput> BuildingRecipeInputs { get; set; }

        /// <summary>
        /// BuildingRecipeOutputs
        /// </summary>
        public DbSet<Model.BuildingRecipeOutput> BuildingRecipeOutputs { get; set; }

        /// <summary>
        /// ChatChannels
        /// </summary>
        public DbSet<Model.ChatChannel> ChatChannels { get; set; }

        /// <summary>
        /// ChatMessages
        /// </summary>
        public DbSet<Model.ChatMessage> ChatMessages { get; set; }

        /// <summary>
        /// ComexExchanges
        /// </summary>
        public DbSet<Model.ComexExchange> ComexExchanges { get; set; }

        /// <summary>
        /// Companies
        /// </summary>
        public DbSet<Model.Company> Companies { get; set; }

        /// <summary>
        /// Companies have a list of planets they have bases on.
        /// </summary>
        public DbSet<Model.CompanyPlanet> CompanyPlanets { get; set; }

        /// <summary>
        /// Countries
        /// </summary>
        public DbSet<Model.Country> Countries { get; set; }

        /// <summary>
        /// FX
        /// </summary>
        public DbSet<Model.FX> FX { get; set; }

        /// <summary>
        /// FXBuyOrders
        /// </summary>
        public DbSet<Model.FXBuyOrder> FXBuyOrders { get; set; }

        /// <summary>
        /// FXSellOrders
        /// </summary>
        public DbSet<Model.FXSellOrder> FXSellOrders { get; set; }

        /// <summary>
        /// Groups
        /// </summary>
        public DbSet<Model.Group> Groups { get; set; }

        /// <summary>
        /// GroupAdmins
        /// </summary>
        public DbSet<Model.GroupAdmin> GroupAdmins { get; set; }

        /// <summary>
        /// GroupPendingInvites
        /// </summary>
        public DbSet<Model.GroupPendingInvite> GroupPendingInvites { get; set; }

        /// <summary>
        /// GroupUsers
        /// </summary>
        public DbSet<Model.GroupUser> GroupUsers { get; set; }

        /// <summary>
        /// LocalMarketAds
        /// </summary>
        public DbSet<Model.LocalMarketAd> LocalMarketAds { get; set; }

        /// <summary>
        /// Materials
        /// </summary>
        public DbSet<Model.Material> Materials { get; set; }

        /// <summary>
        /// MaterialCategories
        /// </summary>
        public DbSet<Model.MaterialCategory> MaterialCategories { get; set; }

        /// <summary>
        /// Permissions
        /// </summary>
        public DbSet<Model.Permission> Permissions { get; set; }

        /// <summary>
        /// Users
        /// </summary>
        public DbSet<Model.User> Users { get; set; }

        /// <summary>
        /// Registrations
        /// </summary>
        public DbSet<Model.Registration> Registrations { get; set; }

        /// <summary>
        /// SimulationData
        /// </summary>
        public DbSet<Model.SimulationData> SimulationData { get; set; }

        /// <summary>
        /// Stations
        /// </summary>
        public DbSet<Model.Station> Stations { get; set; }

        /// <summary>
        /// WorkforceRequirements
        /// </summary>
        public DbSet<Model.WorkforceRequirement> WorkforceRequirements { get; set; }

        /// <summary>
        /// If this context should restrict saves (used with DBWriter)
        /// </summary>
        public bool ShouldRestrictSaves { get; set; } = false;

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public FIODBContext()
        { }

        /// <summary>
        /// DbContextOptions constructor so ASP.Net Core can inject the configuration for tests
        /// </summary>
        /// <param name="options">Options</param>
        public FIODBContext(DbContextOptions<FIODBContext> options)
        : base(options)
        { }


        /// <summary>
        /// Retrieves a new context
        /// </summary>
        /// <param name="bShouldRestrictSaves">If saves should be restricted</param>
        /// <returns>The appropriate DBContext</returns>
        /// <exception cref="InvalidOperationException">If the database type is not defined</exception>
        public static FIODBContext GetNewContext(bool bShouldRestrictSaves = false)
        {
            switch (Globals.DatabaseType)
            {
                case DatabaseType.Postgres:
                    return new PostgresDataContext()
                    {
                        ShouldRestrictSaves = bShouldRestrictSaves
                    };
                case DatabaseType.Sqlite:
                    return new SqliteDataContext()
                    {
                        ShouldRestrictSaves = bShouldRestrictSaves
                    };
                default:
                    throw new InvalidOperationException("No database type defined.");
            }
        }

        /// <summary>
        /// LogDebug DbContext hook
        /// </summary>
        /// <param name="options">options</param>
        public static void LogDebug(DbContextOptionsBuilder options)
        {
            if (Globals.DebugDatabase)
            {
                options.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
            }
        }

        /// <summary>
        /// ApplyOptions hooks
        /// </summary>
        /// <param name="options">options</param>
        /// <returns>DbContextOptionsBuilder</returns>
        /// <exception cref="InvalidOperationException">If the database type is not defined</exception>
        public static DbContextOptionsBuilder ApplyOptions(DbContextOptionsBuilder options)
        {
            LogDebug(options);
            switch (Globals.DatabaseType)
            {
                case DatabaseType.Postgres:
                    return options.UseNpgsql(Globals.DatabaseConnectionString, o => o.CommandTimeout(Globals.CommandTimeout));
                case DatabaseType.Sqlite:
                    return options.UseSqlite(Globals.DatabaseConnectionString, o => o.CommandTimeout(Globals.CommandTimeout));
                default:
                    throw new InvalidOperationException("No database type defined.");
            }
        }

#region Override DbContext
        /// <summary>
        /// Overrides SaveChanges to enforce save restrictions
        /// </summary>
        /// <returns>Number of changes made</returns>
        public override int SaveChanges() => SaveChanges(true);

        /// <summary>
        /// Overrides SaveChanges to enforce save restrictions
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">acceptAllChangesOnSuccess</param>
        /// <returns>Number of changes made</returns>
        /// <exception cref="InvalidOperationException">If saves are not permitted</exception>
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            if (ShouldRestrictSaves)
            {
                throw new InvalidOperationException("Saves not permitted");
            }

            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        /// <summary>
        /// Overrides SaveChangesAsync to enforce save restrictions
        /// </summary>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns>Number of changes made</returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) => SaveChangesAsync(true, cancellationToken);

        /// <summary>
        /// Overrides SaveChangesAsync to enforce save restrictions
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">acceptAllChangesOnSuccess</param>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns>Number of changes made</returns>
        /// <exception cref="InvalidOperationException">If saves are not permitted</exception>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            if (ShouldRestrictSaves)
            {
                throw new InvalidOperationException("Saves not permitted");
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
#endregion
    }
}
