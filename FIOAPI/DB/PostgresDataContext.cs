﻿namespace FIOAPI.DB
{
    /// <summary>
    /// PostgresDataContext
    /// </summary>
    public class PostgresDataContext : FIODBContext
    {
        /// <summary>
        /// OnConfiguring override
        /// </summary>
        /// <param name="options">options</param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            LogDebug(options);
            options
                .UseNpgsql(Globals.DatabaseConnectionString)
                .UseSnakeCaseNamingConvention();
        }
    }
}
