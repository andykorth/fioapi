﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// BuildingCost
    /// </summary>
    public class BuildingCost : IValidation
    {
        /// <summary>
        /// BuildingCostId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(37, MinimumLength = 35)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingCostId { get; set; } = "";

        /// <summary>
        /// The name of the material
        /// </summary>
        [StringLength(64, MinimumLength = 2)]
        public string Name { get; set; } = "";

        /// <summary>
        /// The ticker of the material
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string Ticker { get; set; } = "";

        /// <summary>
        /// Weight of the material
        /// </summary>
        [Range(0.00001, 1000.0)]
        public double Weight { get; set; } = 0.0;

        /// <summary>
        /// Volume of the material
        /// </summary>
        [Range(0.00001, 1000.0)]
        public double Volume { get; set; } = 0.0;

        /// <summary>
        /// The amount of the material required
        /// </summary>
        [Range(1, 5000)]
        public int Amount { get; set; } = 0;

        /// <summary>
        /// BuildingId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(32, MinimumLength = 32)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// Building
        /// </summary>
        [JsonIgnore]
        public virtual Building Building { get; set; } = null!;
    }
}
