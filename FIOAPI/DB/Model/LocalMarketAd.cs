﻿#pragma warning disable 1591
namespace FIOAPI.DB.Model
{
    /// <summary>
    /// LocalMarketAd
    /// </summary>
    [Index(nameof(NaturalId))]
    [Index(nameof(LocalMarketId))]
    public class LocalMarketAd : IValidation
    {
        /// <summary>
        /// LocalMarketAdId
        /// </summary>
        [Key]
        [APEXID]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string LocalMarketAdId { get; set; } = null!;

        /// <summary>
        /// LocalMarketId
        /// </summary>
        [APEXID]
        public string LocalMarketId { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [NaturalID]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// AdNaturalId
        /// </summary>
        [Range(0, int.MaxValue)]
        public int AdNaturalId { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [Uppercase]
        [StringLength(20, MinimumLength = 3)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Weight
        /// </summary>
        [Range(0.00001, 100000)]
        public double? Weight { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Range(0.00001, 100000)]
        public double? Volume { get; set; }

        /// <summary>
        /// CreatorId
        /// </summary>
        [APEXID]
        public string CreatorId { get; set; } = null!;

        /// <summary>
        /// CreatorName
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string CreatorName { get; set; } = null!;

        /// <summary>
        /// CreatorCode
        /// </summary>
        [StringLength(4, MinimumLength = 1)]
        public string CreatorCode { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        public string? MaterialTicker { get; set; }

        /// <summary>
        /// MaterialAmount
        /// </summary>
        [Range(1, int.MaxValue)]
        public int? MaterialAmount { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        [Range(0.000001, double.MaxValue)]
        public double Price { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        [CurrencyCode]
        public string Currency { get; set; } = null!;

        /// <summary>
        /// FulfillmentDays
        /// </summary>
        [Range(1, 100)]
        public int FulfillmentDays { get; set; }

        /// <summary>
        /// MinimumRating
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        public string MinimumRating { get; set; } = null!;

        /// <summary>
        /// CreationTime
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Expiry
        /// </summary>
        public DateTime Expiry { get; set; }

        /// <summary>
        /// OriginNaturalId
        /// </summary>
        [NaturalID]
        public string? OriginNaturalId { get; set; }

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [NaturalID]
        public string? DestinationNaturalId { get; set; }
    }
}
#pragma warning disable 1591