﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Country
    /// </summary>
    [Index(nameof(Code), IsUnique = true)]
    public class Country : IValidation
    {
        /// <summary>
        /// CountryId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CountryId { get; set; } = null!;

        /// <summary>
        /// Name of the country
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Country code
        /// </summary>
        [Uppercase]
        [StringLength(2, MinimumLength = 2)]
        public string Code { get; set; } = null!;

        /// <summary>
        /// Currency numeric code
        /// </summary>
        [Range(1,5)]
        public int NumericCode { get; set; }

        /// <summary>
        /// Currency code
        /// </summary>
        [Uppercase]
        [StringLength(3, MinimumLength = 3)]
        public string CurrenyCode { get; set; } = null!;

        /// <summary>
        /// Name of the currency
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string CurrencyName { get; set; } = null!;

        /// <summary>
        /// Decimals to show for this currency
        /// </summary>
        public int CurrencyDecimals { get; set; }
    }
}
