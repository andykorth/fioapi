﻿using FIOAPI.Payloads;
#pragma warning disable 1591
namespace FIOAPI.DB.Model
{

    //[Index(nameof(code), IsUnique = true)]
    //[Index(nameof(id), IsUnique = true)]
    public class Company : IValidation
    {
        [Key]
        [APEXID]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string id { get; set; } = null!;

        public string name { get; set; } = null!;

        [Uppercase]
        [StringLength(4, MinimumLength = 1)]
        public string code { get; set; } = null!;

        public DateTime founded { get; set; }
        public string UserName { get; set; } = null!;

        [APEXID]
        public string CountryId { get; set; } = null!;

        [StringLength(4, MinimumLength = 1)]
        public string CountryCode { get; set; } = null!;
        [StringLength(64, MinimumLength = 3)]
        public string CountryName { get; set; } = null!;

        [APEXID]
        public string? CorporationId { get; set; } = null!;
        [StringLength(64, MinimumLength = 3)]
        public string? CorporationName { get; set; } = null!;
        [StringLength(4, MinimumLength = 1)]
        public string? CorporationCode { get; set; } = null!;

        public int currentAPEXRepresentationLevel { get; set; }

        // Rating report items don't need to be a sub object in our database model.
        [StringLength(12, MinimumLength = 1)]
        public string overallRating { get; set; } = null!;

        public DateTime? ratingsEarliestContract { get; set; } = null!;
        [Range(0, 1000)]
        public int ratingContractCount { get; set; }

        public string? reputationEntityName { get; set; } = null!;

        [Range(-1000, 10000)]
        public int reputation { get; set; }

        public virtual List<CompanyPlanet> Planets { get; set; } = new List<CompanyPlanet>();

        // TODO governor Offices
    }

    //[Index(nameof(CompanyPlanetId), IsUnique = true)]
    public class CompanyPlanet : IValidation
    {
        // set up as a unique combination of the company id and the planet id.
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CompanyPlanetId { get; set; } = null!;

        public string PlanetId { get; set; } = null!;
        public string PlanetName { get; set; } = null!;
        public string PlanetNaturalId { get; set; } = null!;

        // these two have a role in magically connecting this model with the company model
        [APEXID]
        [JsonIgnore]
        public string CompanyId { get; set; } = null!;

        [JsonIgnore]
        public virtual Company Company { get; set; } = null!;
    }
}
#pragma warning restore 1591