﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// FXBuyOrder
    /// </summary>
    [Index(nameof(UserName))]
    [Index(nameof(UserCompanyCode))]
    public class FXBuyOrder : IValidation
    {
        /// <summary>
        /// FXBuyOrderId
        /// </summary>
        [Key]
        [APEXID]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FXBuyOrderId { get; set; } = null!;

        /// <summary>
        /// UserId
        /// </summary>
        [APEXID]
        public string UserId { get; set; } = null!;

        /// <summary>
        /// UserName
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// UserCompanyCode
        /// </summary>
        [StringLength(4, MinimumLength = 1)]
        public string UserCompanyCode { get; set; } = null!;

        /// <summary>
        /// FXID
        /// </summary>
        [APEXID]
        [JsonIgnore]
        public string FXID { get; set; } = null!;

        /// <summary>
        /// FX
        /// </summary>
        [JsonIgnore]
        public virtual FX FX { get; set; } = null!;
    }
}
