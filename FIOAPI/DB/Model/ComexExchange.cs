﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ComexExchange
    /// </summary>
    [Index(nameof(Code))]
    public class ComexExchange : IValidation
    {
        /// <summary>
        /// Exchange Id
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ComexExchangeId { get; set; } = null!;

        /// <summary>
        /// Code
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string Code { get; set; } = null!;

        /// <summary>
        /// Name of the exchange
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Decimals to show
        /// </summary>
        [Range(1, int.MaxValue)]
        public int Decimals { get; set; }

        /// <summary>
        /// NumericCode
        /// </summary>
        [Range(1, int.MaxValue)]
        public int NumericCode { get; set; }

        /// <summary>
        /// Exchange NaturalId
        /// </summary>
        [NaturalID]
        [StringLength(7, MinimumLength = 3)]
        public string NaturalId { get; set; } = null!;
    }
}
