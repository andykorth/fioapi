﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Building Model
    /// </summary>
    [Index(nameof(Ticker))]
    public class Building : IValidation
    {
        /// <summary>
        /// BuildingId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// The name of the building
        /// </summary>
        [StringLength(64, MinimumLength = 2)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// The ticker of the building
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// The expertise of this building
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string? Expertise { get; set; } = null;

        /// <summary>
        /// The number of pioneers this building uses
        /// </summary>
        [Range(0, 200)]
        public int Pioneers { get; set; } = 0;

        /// <summary>
        /// The number of settlers this building uses
        /// </summary>
        [Range(0, 200)]
        public int Settlers { get; set; } = 0;

        /// <summary>
        /// The number of technicians this building uses
        /// </summary>
        [Range(0, 200)]
        public int Technicians { get; set; } = 0;

        /// <summary>
        /// The number of engineers this building uses
        /// </summary>
        [Range(0, 200)]
        public int Engineers { get; set; } = 0;

        /// <summary>
        /// The number of scientists this building uses
        /// </summary>
        [Range(0, 200)]
        public int Scientists { get; set; } = 0;

        /// <summary>
        /// The area cost of this building
        /// </summary>
        [Range(1, 2000)]
        public int AreaCost { get; set; } = 0;

        /// <summary>
        /// The costs of the building
        /// </summary>
        public virtual List<BuildingCost> Costs { get; set; } = new List<BuildingCost>();

        /// <summary>
        /// Recipes for the building
        /// </summary>
        public virtual List<BuildingRecipe> Recipes { get; set; } = new List<BuildingRecipe>();

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(32, MinimumLength = 3)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
