﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// MaterialCategory
    /// </summary>
    public class MaterialCategory : IValidation
    {
        /// <summary>
        /// MaterialCategoryId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MaterialCategoryId { get; set; } = null!;

        /// <summary>
        /// CategoryName
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string CategoryName { get; set; } = null!;
    }
}
