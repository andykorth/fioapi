﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Station
    /// </summary>
    public class Station : IValidation
    {
        /// <summary>
        /// StationId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StationId { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [NaturalID]
        [StringLength(7, MinimumLength = 3)]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// Station name
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Currency Code
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string CurrencyCode { get; set; } = null!;

        /// <summary>
        /// Country Id
        /// </summary>
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string CountryId { get; set; } = null!;

        /// <summary>
        /// Country code
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string CountryCode { get; set; } = null!;

        /// <summary>
        /// Country name
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string CountryName { get; set; } = null!;

        /// <summary>
        /// Governing entity id
        /// </summary>
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string GoverningEntityId { get; set; } = null!;

        /// <summary>
        /// Governing entity code
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string GoverningEntityCode { get; set; } = null!;

        /// <summary>
        /// Governing entity name
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string GoverningEntityName { get; set; } = null!;
    }
}
