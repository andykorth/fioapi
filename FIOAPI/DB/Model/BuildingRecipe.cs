﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// BuidlingRecipe
    /// </summary>
    public class BuildingRecipe : IValidation
    {
        /// <summary>
        /// BuildingRecipeId
        /// </summary>
        [Key]
        [StringLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingRecipeId { get; set; } = null!;

        /// <summary>
        /// Duration of the recipe in milliseconds
        /// </summary>
        [Range(1, int.MaxValue)]
        public int DurationMs { get; set; } = 0;

        /// <summary>
        /// Inputs for a recipe
        /// </summary>
        public virtual List<BuildingRecipeInput> Inputs { get; set; } = new List<BuildingRecipeInput>();

        /// <summary>
        /// Outputs for a recipe
        /// </summary>
        public virtual List<BuildingRecipeOutput> Outputs { get; set; } = new List<BuildingRecipeOutput>();

        /// <summary>
        /// BuildingId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(32, MinimumLength = 32)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// Building
        /// </summary>
        [JsonIgnore]
        public virtual Building Building { get; set; } = null!;

        /// <summary>
        /// Retrives the recipe
        /// </summary>
        /// <returns>The standardized RecipeName</returns>
        public string GetRecipeId()
        {
            if (Building == null) throw new ArgumentNullException(nameof(Building));

            var inputs = Inputs.OrderBy(i => i.Ticker).Select(i => $"{i.Amount}x{i.Ticker}");
            var outputs = Outputs.OrderBy(o => o.Ticker).Select(o => $"{o.Amount}x{o.Ticker}");
            return $"{Building.Ticker}:{String.Join("-", inputs)}=>{String.Join("-", outputs)}";
        }

        /// <summary>
        /// Generates the recipe name
        /// </summary>
        public void GenerateRecipeId()
        {
            BuildingRecipeId = GetRecipeId();
        }
    }
}
