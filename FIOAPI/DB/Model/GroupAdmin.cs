﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// GroupAdmin model
    /// </summary>
    [Index(nameof(UserName))]
    [Index(nameof(GroupId))]
    public class GroupAdmin : IValidation
    {
        /// <summary>
        /// GroupAdminId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupAdminId { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [Lowercase]
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// GroupId
        /// </summary>
        [JsonIgnore]
        [Range(0, Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// Group
        /// </summary>
        [JsonIgnore]
        public virtual Group Group { get; set; } = null!;
    }
}
