﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Group Model
    /// </summary>
    [Index(nameof(GroupName))]
    public class Group : IValidation
    {
        /// <summary>
        /// The largest GroupId possible
        /// </summary>
        public const int LargestGroupId = 999999;

        /// <summary>
        /// GroupId
        /// </summary>
        [Key]
        [Range(1, LargestGroupId)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupId { get; set; }

        /// <summary>
        /// GroupName
        /// </summary>
        [StringLength(16, MinimumLength = 3)]
        public string GroupName { get; set; } = null!;

        /// <summary>
        /// GroupOwner
        /// </summary>
        [Lowercase]
        [StringLength(32, MinimumLength = 3)]
        public string GroupOwner { get; set; } = null!;

        /// <summary>
        /// PendingInvites
        /// </summary>
        public virtual List<GroupPendingInvite> PendingInvites { get; set; } = new List<GroupPendingInvite>();

        /// <summary>
        /// Admins
        /// </summary>
        public virtual List<GroupAdmin> Admins { get; set; } = new List<GroupAdmin>();

        /// <summary>
        /// Users
        /// </summary>
        public virtual List<GroupUser> Users { get; set; } = new List<GroupUser>();
    }
}
