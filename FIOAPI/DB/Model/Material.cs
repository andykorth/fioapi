﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Material
    /// </summary>
    [Index(nameof(Ticker))]
    [Index(nameof(MaterialCategoryId))]
    public class Material : IValidation
    {
        /// <summary>
        /// The APEX MaterialId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// Name of the material
        /// </summary>
        [StringLength(64)]
        public string Name { get; set;} = null!;

        /// <summary>
        /// The material's ticker
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// The material's category id
        /// </summary>
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string MaterialCategoryId { get; set; } = null!;

        /// <summary>
        /// Weight of the material in tons
        /// </summary>
        [Range(0.000001, double.MaxValue)]
        public double Weight { get; set; }

        /// <summary>
        /// Volume of the material in m^3
        /// </summary>
        [Range(0.000001, double.MaxValue)]
        public double Volume { get; set; }

        /// <summary>
        /// If it's a resource extractable from a planet
        /// </summary>
        public bool IsResource { get; set; }

        /// <summary>
        /// The resource type o
        /// </summary>
        public string? ResourceType { get; set; } = null;

        /// <summary>
        /// If this material is used in infrastructure
        /// </summary>
        public bool InfrastructureUsage { get; set; }

        /// <summary>
        /// If this material is used in COGC
        /// </summary>
        public bool COGCUsage { get; set; }

        /// <summary>
        /// If this material is used for workforces
        /// </summary>
        public bool WorkforceUsage { get; set; }

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(32, MinimumLength = 3)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
