﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Global Simulation Data
    /// </summary>
    public class SimulationData : IValidation
    {
        /// <summary>
        /// SimulationDataId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SimulationDataId { get; set; }

        /// <summary>
        /// How fast the game simulations
        /// </summary>
        [Range(1, int.MaxValue)]
        public int SimulationInterval { get; set; }

        /// <summary>
        /// How fast ships fly through STL space
        /// </summary>
        [Range(1, int.MaxValue)]
        public int FlightSTLFactor { get; set; }

        /// <summary>
        /// How fast ships fly through FTL space
        /// </summary>
        [Range(1, int.MaxValue)]
        public int FlightFTLFactor { get; set; }

        /// <summary>
        /// How fast planets move
        /// </summary>
        [Range(1, int.MaxValue)]
        public int PlanetaryMotionFactor { get; set; }

        /// <summary>
        /// How long a single parsec is
        /// </summary>
        [Range(1, int.MaxValue)]
        public int ParsecLength { get; set; }
    }
}
