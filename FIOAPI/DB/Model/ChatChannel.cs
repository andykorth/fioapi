﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ChatChannel
    /// </summary>
    public class ChatChannel : IValidation
    {
        /// <summary>
        /// The ChannelId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ChatChannelId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(10, MinimumLength = 5)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [StringLength(10)]
        public string? NaturalId { get; set; } = null;

        /// <summary>
        /// DisplayName
        /// </summary>
        [StringLength(64)]
        public string? DisplayName { get; set; } = null;

        /// <summary>
        /// UserCount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int UserCount { get; set; } = 0;

        /// <summary>
        /// CreationTimestamp
        /// </summary>
        public long CreationTimestamp { get; set; } = 0;

        /// <summary>
        /// LastActivityTimestamp
        /// </summary>
        public long LastActivityTimestamp { get; set; } = 0;

        /// <summary>
        /// Messages
        /// </summary>
        public virtual List<ChatMessage> Messages { get; set; } = new List<ChatMessage>();

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(32, MinimumLength = 3)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
