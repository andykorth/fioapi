﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// GroupPendingInvite model
    /// </summary>
    [Index(nameof(UserName))]
    [Index(nameof(GroupId))]
    public class GroupPendingInvite : IValidation
    {
        /// <summary>
        /// GroupPendingInviteId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupPendingInviteId { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [Lowercase]
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// If the invite is for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;

        /// <summary>
        /// GroupId
        /// </summary>
        [JsonIgnore]
        [Range(0, Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// Group
        /// </summary>
        [JsonIgnore]
        public virtual Group Group { get; set; } = null!;
    }
}
