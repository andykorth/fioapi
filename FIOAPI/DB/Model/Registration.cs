﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// RegistrationModel
    /// </summary>
    [Index(nameof(UserName))]
    public class Registration : IValidation
    {
        /// <summary>
        /// RegistrationId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RegistrationId { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [Lowercase]
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// DisplayUserName
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string DisplayUserName { get; set; } = null!;

        /// <summary>
        /// RegistrationGuid
        /// </summary>
        [GuidValid]
        public Guid RegistrationGuid { get; set; }

        /// <summary>
        /// RegistrationTime
        /// </summary>
        public DateTime RegistrationTime { get; set; }
    }
}
