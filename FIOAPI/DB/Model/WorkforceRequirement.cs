﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// WorkforceRequirement
    /// </summary>
    [Index(nameof(Level))]
    public class WorkforceRequirement : IValidation
    {
        /// <summary>
        /// WorkforceRequirementId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkforceRequirementId { get; set; }

        /// <summary>
        /// The "level" or name of the workforce tier (PIONEER, SETTLER, etc)
        /// </summary>
        [StringLength(16, MinimumLength = 3)]
        public string Level { get; set; } = null!;

        /// <summary>
        /// The workforce needs for this tier of workers
        /// </summary>
        [NotEmpty]
        public virtual List<WorkforceRequirementNeed> Needs { get; set; } = new List<WorkforceRequirementNeed>();
    }

    /// <summary>
    /// WorkforceRequirementNeed
    /// </summary>
    public class WorkforceRequirementNeed : IValidation
    {
        /// <summary>
        /// WorkforceRequirementNeedId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkforceRequirementNeedId { get; set; }

        /// <summary>
        /// If the need is essential
        /// </summary>
        public bool Essential { get; set; }

        /// <summary>
        /// The name of the material
        /// </summary>
        [StringLength(64, MinimumLength = 3)]
        public string MaterialName { get; set; } = null!;

        /// <summary>
        /// The need's ticker
        /// </summary>
        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// The material id
        /// </summary>
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// The amount consumed per 100 workers
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double UnitsPer100 { get; set; }

        /// <summary>
        /// WorkforceRequirementId
        /// </summary>
        [JsonIgnore]
        public int WorkforceRequirementId { get; set; }

        /// <summary>
        /// WorkforceRequirement
        /// </summary>
        [JsonIgnore]
        public virtual WorkforceRequirement WorkforceRequirement { get; set; } = null!;
    }
}
