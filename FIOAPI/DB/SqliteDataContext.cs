﻿namespace FIOAPI.DB
{
    /// <summary>
    /// SqliteDataContext
    /// </summary>
    public class SqliteDataContext : FIODBContext
    {
        /// <summary>
        /// OnConfiguring override
        /// </summary>
        /// <param name="options">options</param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            LogDebug(options);
            options
                .UseSqlite(Globals.DatabaseConnectionString)
                .UseSnakeCaseNamingConvention();
        }
    }
}
