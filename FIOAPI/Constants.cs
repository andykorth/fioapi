﻿namespace FIOAPI
{
    /// <summary>
    /// Constants used across FIOAPI
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// List of valid currencies
        /// </summary>
        public static readonly List<string> ValidCurrencies = new List<string>
        {
            "AIC", "CIS", "ICA", "NCC", "ECD"
        };

        /// <summary>
        /// List of valid localmarket types
        /// </summary>
        public static List<string> LocalMarketValidTypes = new List<string>
        {
            "COMMODITY_SHIPPING",
            "COMMODITY_BUYING",
            "COMMODITY_SELLING"
        };

        /// <summary>
        /// List of valid ratings
        /// </summary>
        public static List<string> ValidRatings = new List<string>
        {
            "PENDING", "A", "B", "C", "D", "E", "F", "UNRATED"
        };
    }
}
