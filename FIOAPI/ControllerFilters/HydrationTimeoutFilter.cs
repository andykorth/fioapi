﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FIOAPI.ControllerFilters
{
    /// <summary>
    /// A HydrationTimeoutFilter which will return Ok() early if the payload contains the "Hydration Timeout [" string
    /// </summary>
    public class HydrationTimeoutFilter : ActionFilterAttribute
    {
        /// <summary>
        /// The Hydration Timeout string to search for
        /// </summary>
        public const string HydrationTimeoutStr = "Hydration Timeout [";

        /// <summary>
        /// The response of the payload when this has been encountered
        /// </summary>
        public const string HydrationTimeoutHeader = "HydrationTimeout";

        /// <summary>
        /// OnActionExecuting
        /// </summary>
        /// <param name="context">context</param>
        /// <param name="next">next</param>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (next == null) throw new ArgumentNullException(nameof(next));

            var request = context.HttpContext.Request;

            var bodyStr = await request.GetBodyAsync();
            if (bodyStr.Contains(HydrationTimeoutStr))
            {
                context.HttpContext.Response.Headers.Add(HydrationTimeoutHeader, "");
                context.Result = new OkObjectResult("");
            }

            await base.OnActionExecutionAsync(context, next);
        }
    }
}
