﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Material
{
    public class MESG_WORLD_MATERIAL_CATEGORIES : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_WORLD_MATERIAL_CATEGORIES_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_CATEGORIES_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_WORLD_MATERIAL_CATEGORIES_MESSAGE message { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_CATEGORIES_MESSAGE : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_WORLD_MATERIAL_CATEGORIES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_CATEGORIES_PAYLOAD_INNER : IValidation
    {
        public MESG_WORLD_MATERIAL_CATEGORIES_CATEGORY[] categories { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_CATEGORIES_CATEGORY : IValidation
    {
        public object[] children { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string id { get; set; } = null!;

        public APEX_COMMON_MATERIAL[] materials { get; set; } = null!;
    }
}
#pragma warning restore 1591
