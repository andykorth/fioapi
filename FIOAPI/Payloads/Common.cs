﻿#pragma warning disable 1591
using FIOAPI.Payloads.Global;

namespace FIOAPI.Payloads
{
    public class APEX_COMMON_MATERIAL : IValidation
    {
        [StringLength(64, MinimumLength = 2)]
        public string name { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

        [Ticker]
        public string ticker { get; set; } = null!;

        [APEXID]
        public string category { get; set; } = null!;

        [Range(0.000001, 1000.0)]
        public double weight { get; set; }

        [Range(0.000001, 1000.0)]
        public double volume { get; set; }

        public bool resource { get; set; }
    }

    public class APEX_COMMON_DURATION : IValidation
    {
        [Range(1, int.MaxValue)]
        public int millis { get; set; }
    }

    public class APEX_COMMON_MATERIAL_AND_AMOUNT : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(1, int.MaxValue)]
        public int amount { get; set; }
    }

    public class APEX_COMMON_AMOUNT_AND_CURRENCY : IValidation
    {
        [Range(0.0, double.MaxValue)]
        public double amount { get; set; }

        [CurrencyCode]
        public string currency { get; set; } = null!;
    }

    public class APEX_COMMON_TIMESTAMP : IValidation
    {
        [APEXTimestamp]
        public long timestamp { get; set; }
    }

    public class APEX_COMMON_ADDRESS : IValidation
    {
        [NotEmpty]
        public List<APEX_COMMON_ADDRESS_LINE> lines { get; set; } = null!;
    }

    public class APEX_COMMON_ADDRESS_LINE : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY entity { get; set; } = null!;

        public string type { get; set; } = null!;
    }

    public class APEX_COMMON_ADDRESS_ENTITY : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [NaturalID]
        public string naturalId { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }

    public class APEX_COMMON_USER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(32, MinimumLength = 3)]
        public string username { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }

    public class APEX_COMMON_USER_AND_CODE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(32, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [StringLength(4, MinimumLength = 1)]
        public string code { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }
}
#pragma warning restore 1591