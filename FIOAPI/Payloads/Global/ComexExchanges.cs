﻿#pragma warning disable 1591

namespace FIOAPI.Payloads.Global
{
    public class PATH_COMMODITY_EXCHANGES : IValidation
    {
        public string messageType { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_COMMODITY_EXCHANGES_MESSAGE message { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_MESSAGE : IValidation
    {
        public string messageType { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_PAYLOAD_INNER : IValidation
    {
        [NotEmpty]
        public List<PATH_COMMODITY_EXCHANGES_BODY> body { get; set; } = null!;

        public string[] path { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_BODY : IValidation
    {
        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [Ticker]
        public string code { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_CURRENCY currency { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_ADDRESS address { get; set; } = null!;

        public string id { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_OPERATOR : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [Ticker]
        public string code { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public string _type { get; set; } = null!;

        [APEXID]
        public string _proxy_key { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_CURRENCY : IValidation
    {
        [Range(1, int.MaxValue)]
        public int numericCode { get; set; }

        [Ticker]
        public string code { get; set; } = null!;

        [StringLength(32, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [Range(1, int.MaxValue)]
        public int decimals { get; set; }
    }

    public class PATH_COMMODITY_EXCHANGES_ADDRESS : IValidation
    {
        [NotEmpty]
        public List<PATH_COMMODITY_EXCHANGES_LINE> lines { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_LINE : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY entity { get; set; } = null!;

        public string type { get; set; } = null!;
    }
}
#pragma warning restore 1591