﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Global
{
    public class MESG_SIMULATION_DATA : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_SIMULATION_DATA_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_SIMULATION_DATA_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_SIMULATION_DATA_PAYLOAD_MESSAGE message { get; set; } = null!;
    }

    public class MESG_SIMULATION_DATA_PAYLOAD_MESSAGE : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_SIMULATION_DATA_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_SIMULATION_DATA_PAYLOAD_INNER : IValidation
    {
        public int simulationInterval { get; set; }
        public int flightSTLFactor { get; set; }
        public int flightFTLFactor { get; set; }
        public int planetaryMotionFactor { get; set; }
        public int parsecLength { get; set; }
    }
}
#pragma warning restore 1591