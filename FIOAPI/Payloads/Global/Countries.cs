﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Global
{
    public class MESG_COUNTRY_REGISTRY_COUNTRIES : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_COUNTRY_REGISTRY_COUNTRIES_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class MESG_COUNTRY_REGISTRY_COUNTRIES_PAYLOAD_OUTER : IValidation
    {
        [APEXID]
        public string? actionId { get; set; } = null;

        public int status { get; set; }

        public MESG_COUNTRY_REGISTRY_COUNTRIES_PAYLOAD_OUTER_MESSAGE message { get; set; } = null!;
    }

    public class MESG_COUNTRY_REGISTRY_COUNTRIES_PAYLOAD_OUTER_MESSAGE : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_COUNTRY_REGISTRY_COUNTRIES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_COUNTRY_REGISTRY_COUNTRIES_PAYLOAD_INNER : IValidation
    {
        [NotEmpty]
        public List<MESG_COUNTRY_REGISTRY_COUNTRIES_COUNTRY> countries { get; set; } = null!;
    }

    public class MESG_COUNTRY_REGISTRY_COUNTRIES_COUNTRY : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(5, MinimumLength = 1)]
        public string code { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public MESG_COUNTRY_REGISTRY_COUNTRIES_CURRENCY currency { get; set; } = null!;
    }

    public class MESG_COUNTRY_REGISTRY_COUNTRIES_CURRENCY : IValidation
    {
        public int numericCode { get; set; }

        [Ticker]
        public string code { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public int decimals { get; set; }
    }

}
#pragma warning restore 1591