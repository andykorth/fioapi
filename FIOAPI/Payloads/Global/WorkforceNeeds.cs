﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Global
{
    public class MESG_WORKFORCE_WORKFORCES : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_WORKFORCE_WORKFORCES_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_WORKFORCE_WORKFORCES_MESSAGE message { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_MESSAGE : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_WORKFORCE_WORKFORCES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_PAYLOAD_INNER : IValidation
    {
        public MESG_WORKFORCE_WORKFORCES_ADDRESS address { get; set; } = null!;

        [APEXID]
        public string siteId { get; set; } = null!;

        public MESG_WORKFORCE_WORKFORCES_WORKFORCE[] workforces { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_ADDRESS : IValidation
    {
        public MESG_WORKFORCE_WORKFORCES_LINE[] lines { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_LINE : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY entity { get; set; } = null!;

        public string type { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_WORKFORCE : IValidation
    {
        [StringLength(32, MinimumLength = 3)]
        public string level { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int population { get; set; }

        [Range(0, int.MaxValue)]
        public int reserve { get; set; }

        [Range(0, int.MaxValue)]
        public int capacity { get; set; }

        [Range(0, int.MaxValue)]
        public int required { get; set; }

        [Range(0, int.MaxValue)]
        public int satisfaction { get; set; }

        public MESG_WORKFORCE_WORKFORCES_NEED[] needs { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_NEED : IValidation
    {
        [StringLength(64, MinimumLength = 3)]
        public string category { get; set; } = null!;

        public bool essential { get; set; }

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int satisfaction { get; set; }

        [Range(0.0, double.MaxValue)]
        public double unitsPerInterval { get; set; }

        [Range(0.0, double.MaxValue)]
        public double unitsPer100 { get; set; }
    }
}
#pragma warning restore 1591