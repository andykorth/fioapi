﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Currency
{
    public class MESG_FOREX_BROKER_DATA : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_FOREX_BROKER_DATA_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_FOREX_BROKER_DATA_MESSAGE message { get; set; } = null!;
    }

    public class MESG_FOREX_BROKER_DATA_MESSAGE : IValidation
    {
        public string messageType { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_FOREX_BROKER_DATA_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [FXTicker]
        public string ticker { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_PAIR pair { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_PRICE price { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA? bid { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA? ask { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA? spread { get; set; } = null!;

        public List<MESG_FOREX_BROKER_DATA_SELLINGORDER> sellingOrders { get; set; } = null!;

        public List<MESG_FOREX_BROKER_DATA_BUYINGORDER> buyingOrders { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY lotSize { get; set; } = null!;

        [Range(0.00001, 100.0)]
        public double feesFactor { get; set; }
    }

    public class MESG_FOREX_BROKER_DATA_PAIR : IValidation
    {
        public MESG_FOREX_BROKER_DATA_PAIR_CURRENCY @base { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_PAIR_CURRENCY quote { get; set; } = null!;

        [Range(1, 100)]
        public int decimals { get; set; }
    }

    public class MESG_FOREX_BROKER_DATA_PAIR_CURRENCY : IValidation
    {
        [Range(0, 100)]
        public int numericCode { get; set; }

        [CurrencyCode]
        public string code { get; set; } = null!;

        [StringLength(32, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [Range(1, 100)]
        public int decimals { get; set; }
    }

    public class MESG_FOREX_BROKER_DATA_PRICE : IValidation
    {
        public MESG_FOREX_BROKER_DATA_DATA open { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA low { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA high { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA previous { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA close { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY traded { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_VOLUME volume { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;
    }

    public class MESG_FOREX_BROKER_DATA_VOLUME : IValidation
    {
        [Range(0, double.MaxValue)]
        public double amount { get; set; }

        [CurrencyCode]
        public string currency { get; set; } = null!;
    }

    public class MESG_FOREX_BROKER_DATA_DATA : IValidation
    {
        [CurrencyCode]
        public string @base { get; set; } = null!;

        [CurrencyCode]
        public string quote { get; set; } = null!;

        [Range(0.0001, double.MaxValue)]
        public double rate { get; set; }

        [Range(1, 10)]
        public int decimals { get; set; }
    }

    public class MESG_FOREX_BROKER_DATA_SELLINGORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE trader { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY amount { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA limit { get; set; } = null!;
    }

    public class MESG_FOREX_BROKER_DATA_BUYINGORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE trader { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY amount { get; set; } = null!;

        public MESG_FOREX_BROKER_DATA_DATA limit { get; set; } = null!;
    }
}
#pragma warning restore 1591