﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Chat
{
    public class CHANNEL_MESSAGE_DELETED : IValidation
    {
        public string messageType { get; set; } = null!;

        public CHANNEL_MESSAGE_DELETED_PAYLOAD payload { get; set; } = null!;
    }

    public class CHANNEL_MESSAGE_DELETED_PAYLOAD : IValidation
    {
        [APEXID]
        public string messageId { get; set; } = null!;

        [StringLength(32, MinimumLength = 2)]
        public string type { get; set; } = null!;

        public APEX_COMMON_USER sender { get; set; } = null!;

        [StringLength(1024)]
        public string message { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        [APEXID]
        public string channelId { get; set; } = null!;

        public APEX_COMMON_USER deletingUser { get; set; } = null!;
    }
}
#pragma warning restore 1591