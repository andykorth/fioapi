﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// The DeleteAccount payload
    /// </summary>
    public class DeleteAccount : IValidation
    {
        /// <summary>
        /// Your password
        /// </summary>
        /// <example>Hunter2</example>
        [StringLength(256, MinimumLength = 3)]
        public string Password { get; set; } = "";
    }
}
