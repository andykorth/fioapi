﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// The login payload
    /// </summary>
    public class Login : IValidation
    {
        /// <summary>
        /// Your username
        /// </summary>
        /// <example>Saganaki</example>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// Your password
        /// </summary>
        /// <example>Hunter2</example>
        [StringLength(256, MinimumLength = 3)]
        public string Password { get; set; } = "";
    }
}
