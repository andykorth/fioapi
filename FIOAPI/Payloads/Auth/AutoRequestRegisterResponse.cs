﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// Response for AutoRequestRgister
    /// </summary>
    public class AutoRequestRegisterResponse : IValidation
    {
        /// <summary>
        /// The username (case-sensitive) for the request
        /// </summary>
        /// <example>Saganaki</example>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// The registration guid to use
        /// </summary>
        /// <example>a568f64a66414c959dace10e47294855</example>
        [GuidValid]
        public Guid RegistrationGuid { get; set; }

        /// <summary>
        /// The time when registration was requested
        /// </summary>
        public DateTime RegistrationTime { get; set; }
    }
}
