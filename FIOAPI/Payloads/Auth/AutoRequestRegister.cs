﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// AutoRequestRegister
    /// </summary>
    /// <remarks>
    /// This payload is used by the extension when it determines you are not logged in.<br/>
    /// It sends the raw APEX game payload to the api and then based on the result, will prompt you to register.<br/>
    /// </remarks>
    public class AutoRequestRegister : IValidation
    {
        /// <summary>
        /// messageType
        /// </summary>
        public string messageType { get; set; } = "";

        /// <summary>
        /// payload
        /// </summary>
        public AutoRequestRegisterPayload payload { get; set; } = null!;
    }

    /// <summary>
    /// Inner payload
    /// </summary>
    public class AutoRequestRegisterPayload : IValidation
    {
        /// <summary>
        /// username
        /// </summary>
        /// <example>Saganaki</example>
        [StringLength(32, MinimumLength = 3)]
        public string username { get; set; } = "";

        /// <summary>
        /// admin
        /// </summary>
        /// <remark>
        /// This is unused--merely here to match the game payload.
        /// </remark>
        [DefaultValue(false)]
        public bool admin { get; set; }
    }
}
