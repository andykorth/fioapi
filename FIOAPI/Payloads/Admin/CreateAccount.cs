﻿namespace FIOAPI.Payloads.Admin
{
    /// <summary>
    /// The payload for admin account creation
    /// </summary>
    public class CreateAccount : IValidation
    {
        /// <summary>
        /// The UserName
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// The password
        /// </summary>
        [StringLength(256, MinimumLength = 3)]
        public string Password { get; set; } = "";

        /// <summary>
        /// If the user should be admin
        /// </summary>
        [DefaultValue(false)]
        public bool Admin { get; set; } = false;
    }
}
