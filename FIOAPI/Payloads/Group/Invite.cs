﻿namespace FIOAPI.Payloads.Group
{
    /// <summary>
    /// Invite payload
    /// </summary>
    public class Invite : IValidation
    {
        /// <summary>
        /// GroupId
        /// </summary>
        [Range(1, Model.Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// Invites
        /// </summary>
        [NotEmpty]
        public List<UserInvite> Invites { get; set; } = new List<UserInvite>();
    }
}
