﻿namespace FIOAPI.Payloads.Group
{
    /// <summary>
    /// Invite response
    /// </summary>
    public class InviteResponse : IValidation
    {
        /// <summary>
        /// GroupId
        /// </summary>
        [Range(1, Model.Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// If this is an invite for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;
    }
}
