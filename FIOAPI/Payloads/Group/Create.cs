﻿using FIOAPI.Payloads.Permission;

namespace FIOAPI.Payloads.Group
{
    /// <summary>
    /// The Create payload
    /// </summary>
    public class Create : IValidation
    {
        /// <summary>
        /// An optionally requested Id number
        /// </summary>
        /// <remark>If the Id is taken, this will return NotAcceptable (HTTP 406)</remark>
        [Range(0, Model.Group.LargestGroupId)]
        public int RequestedId { get; set; } = 0;

        /// <summary>
        /// The name of the group
        /// </summary>
        
        [StringLength(16, MinimumLength = 3)]
        public string GroupName { get; set; } = "";

        /// <summary>
        /// All the invites
        /// </summary>
        public List<UserInvite> Invites { get; set; } = new List<UserInvite>();

        /// <summary>
        /// Permissions for this group
        /// </summary>
        
        public Permissions Permissions { get; set; } = new Permissions();
    }
}
