﻿namespace FIOAPI.Payloads.Group
{
    /// <summary>
    /// Response to group create
    /// </summary>
    public class CreateResponse : IValidation
    {
        /// <summary>
        /// The GroupId of the new group
        /// </summary>
        [Range(1, Model.Group.LargestGroupId)]
        public int GroupId { get; set; }
    }
}
