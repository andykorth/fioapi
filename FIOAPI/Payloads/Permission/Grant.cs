﻿using System.ComponentModel.DataAnnotations;

namespace FIOAPI.Payloads.Permission
{
    /// <summary>
    /// The payload for Granting permissions
    /// </summary>
    public class Grant : IValidation
    {
        /// <summary>
        /// The username (or * for all users) to grant permission to
        /// </summary>
        [StringLength(32, MinimumLength = 1)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// The permissions to grant to the username specified
        /// </summary>
        public Permissions Permissions { get; set; } = new Permissions();

        /// <summary>
        /// CustomValidation path for the GrantPayload
        /// </summary>
        /// <param name="Errors">Errors to fill</param>
        /// <param name="Context">The context from which this validation is being run</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (UserName != "*" && UserName.Length < 3)
            {
                Errors.Add("UserName specified is fewer than 3 characters");
            }
        }
    }
}
