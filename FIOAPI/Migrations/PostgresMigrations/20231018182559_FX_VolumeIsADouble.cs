﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class FX_VolumeIsADouble : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "volume",
                table: "fx",
                type: "double precision",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "volume",
                table: "fx",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "double precision");
        }
    }
}
