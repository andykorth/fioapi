﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddWorkforceRequirements : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "workforce_requirements",
                columns: table => new
                {
                    workforce_requirement_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    level = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_requirements", x => x.workforce_requirement_id);
                });

            migrationBuilder.CreateTable(
                name: "workforce_requirement_need",
                columns: table => new
                {
                    workforce_requirement_need_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    essential = table.Column<bool>(type: "boolean", nullable: false),
                    material_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    units_per100 = table.Column<double>(type: "double precision", nullable: false),
                    workforce_requirement_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_requirement_need", x => x.workforce_requirement_need_id);
                    table.ForeignKey(
                        name: "fk_workforce_requirement_need_workforce_requirements_workforce",
                        column: x => x.workforce_requirement_id,
                        principalTable: "workforce_requirements",
                        principalColumn: "workforce_requirement_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_workforce_requirement_need_workforce_requirement_id",
                table: "workforce_requirement_need",
                column: "workforce_requirement_id");

            migrationBuilder.CreateIndex(
                name: "ix_workforce_requirements_level",
                table: "workforce_requirements",
                column: "level");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "workforce_requirement_need");

            migrationBuilder.DropTable(
                name: "workforce_requirements");
        }
    }
}
