﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddMaterialAndMaterialCategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "material_categories",
                columns: table => new
                {
                    material_category_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    category_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_material_categories", x => x.material_category_id);
                });

            migrationBuilder.CreateTable(
                name: "materials",
                columns: table => new
                {
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_category_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    is_resource = table.Column<bool>(type: "boolean", nullable: false),
                    resource_type = table.Column<string>(type: "text", nullable: true),
                    infrastructure_usage = table.Column<bool>(type: "boolean", nullable: false),
                    cogc_usage = table.Column<bool>(type: "boolean", nullable: false),
                    workforce_usage = table.Column<bool>(type: "boolean", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_materials", x => x.material_id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_materials_material_category_id",
                table: "materials",
                column: "material_category_id");

            migrationBuilder.CreateIndex(
                name: "ix_materials_ticker",
                table: "materials",
                column: "ticker");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "material_categories");

            migrationBuilder.DropTable(
                name: "materials");
        }
    }
}
