﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddComexExchanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "comex_exchanges",
                columns: table => new
                {
                    comex_exchange_id = table.Column<string>(type: "text", nullable: false),
                    code = table.Column<string>(type: "text", nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    decimals = table.Column<int>(type: "integer", nullable: false),
                    numeric_code = table.Column<int>(type: "integer", nullable: false),
                    natural_id = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_comex_exchanges", x => x.comex_exchange_id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_comex_exchanges_code",
                table: "comex_exchanges",
                column: "code");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "comex_exchanges");
        }
    }
}
