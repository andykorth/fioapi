﻿// <auto-generated />
using System;
using FIOAPI.DB;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    [DbContext(typeof(PostgresDataContext))]
    [Migration("20230615173519_RemoveIdFromBuilding")]
    partial class RemoveIdFromBuilding
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("FIOAPI.DB.Model.APIKey", b =>
                {
                    b.Property<int>("APIKeyId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("api_key_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("APIKeyId"));

                    b.Property<bool>("AllowWrites")
                        .HasColumnType("boolean")
                        .HasColumnName("allow_writes");

                    b.Property<string>("Application")
                        .IsRequired()
                        .HasMaxLength(128)
                        .HasColumnType("character varying(128)")
                        .HasColumnName("application");

                    b.Property<DateTime>("CreateTime")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("create_time");

                    b.Property<Guid>("Key")
                        .HasColumnType("uuid")
                        .HasColumnName("key");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name");

                    b.HasKey("APIKeyId")
                        .HasName("pk_api_keys");

                    b.HasIndex("Key")
                        .HasDatabaseName("ix_api_keys_key");

                    b.HasIndex("UserName")
                        .HasDatabaseName("ix_api_keys_user_name");

                    b.ToTable("api_keys", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.Building", b =>
                {
                    b.Property<string>("BuildingId")
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("building_id");

                    b.Property<int>("AreaCost")
                        .HasColumnType("integer")
                        .HasColumnName("area_cost");

                    b.Property<int>("Engineers")
                        .HasColumnType("integer")
                        .HasColumnName("engineers");

                    b.Property<string>("Expertise")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("expertise");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)")
                        .HasColumnName("name");

                    b.Property<int>("Pioneers")
                        .HasColumnType("integer")
                        .HasColumnName("pioneers");

                    b.Property<int>("Scientists")
                        .HasColumnType("integer")
                        .HasColumnName("scientists");

                    b.Property<int>("Settlers")
                        .HasColumnType("integer")
                        .HasColumnName("settlers");

                    b.Property<int>("Technicians")
                        .HasColumnType("integer")
                        .HasColumnName("technicians");

                    b.Property<string>("Ticker")
                        .IsRequired()
                        .HasMaxLength(4)
                        .HasColumnType("character varying(4)")
                        .HasColumnName("ticker");

                    b.Property<DateTime>("Timestamp")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("timestamp");

                    b.Property<string>("UserNameSubmitted")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name_submitted");

                    b.HasKey("BuildingId")
                        .HasName("pk_buildings");

                    b.HasIndex("Ticker")
                        .HasDatabaseName("ix_buildings_ticker");

                    b.ToTable("buildings", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingCost", b =>
                {
                    b.Property<string>("BuildingCostId")
                        .HasMaxLength(37)
                        .HasColumnType("character varying(37)")
                        .HasColumnName("building_cost_id");

                    b.Property<int>("Amount")
                        .HasColumnType("integer")
                        .HasColumnName("amount");

                    b.Property<string>("BuildingId")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("building_id");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)")
                        .HasColumnName("name");

                    b.Property<string>("Ticker")
                        .IsRequired()
                        .HasMaxLength(4)
                        .HasColumnType("character varying(4)")
                        .HasColumnName("ticker");

                    b.Property<double>("Volume")
                        .HasColumnType("double precision")
                        .HasColumnName("volume");

                    b.Property<double>("Weight")
                        .HasColumnType("double precision")
                        .HasColumnName("weight");

                    b.HasKey("BuildingCostId")
                        .HasName("pk_building_costs");

                    b.HasIndex("BuildingId")
                        .HasDatabaseName("ix_building_costs_building_id");

                    b.ToTable("building_costs", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipe", b =>
                {
                    b.Property<string>("BuildingRecipeId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("building_recipe_id");

                    b.Property<string>("BuildingId")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("building_id");

                    b.Property<int>("DurationMs")
                        .HasColumnType("integer")
                        .HasColumnName("duration_ms");

                    b.HasKey("BuildingRecipeId")
                        .HasName("pk_building_recipes");

                    b.HasIndex("BuildingId")
                        .HasDatabaseName("ix_building_recipes_building_id");

                    b.ToTable("building_recipes", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipeInput", b =>
                {
                    b.Property<string>("BuildingRecipeInputId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("building_recipe_input_id");

                    b.Property<int>("Amount")
                        .HasColumnType("integer")
                        .HasColumnName("amount");

                    b.Property<string>("BuildingRecipeId")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("building_recipe_id");

                    b.Property<string>("Id")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("id");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)")
                        .HasColumnName("name");

                    b.Property<string>("Ticker")
                        .IsRequired()
                        .HasMaxLength(4)
                        .HasColumnType("character varying(4)")
                        .HasColumnName("ticker");

                    b.Property<double>("Volume")
                        .HasColumnType("double precision")
                        .HasColumnName("volume");

                    b.Property<double>("Weight")
                        .HasColumnType("double precision")
                        .HasColumnName("weight");

                    b.HasKey("BuildingRecipeInputId")
                        .HasName("pk_building_recipe_inputs");

                    b.HasIndex("BuildingRecipeId")
                        .HasDatabaseName("ix_building_recipe_inputs_building_recipe_id");

                    b.ToTable("building_recipe_inputs", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipeOutput", b =>
                {
                    b.Property<string>("BuildingRecipeOutputId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("building_recipe_output_id");

                    b.Property<int>("Amount")
                        .HasColumnType("integer")
                        .HasColumnName("amount");

                    b.Property<string>("BuildingRecipeId")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("building_recipe_id");

                    b.Property<string>("Id")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("id");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(64)
                        .HasColumnType("character varying(64)")
                        .HasColumnName("name");

                    b.Property<string>("Ticker")
                        .IsRequired()
                        .HasMaxLength(4)
                        .HasColumnType("character varying(4)")
                        .HasColumnName("ticker");

                    b.Property<double>("Volume")
                        .HasColumnType("double precision")
                        .HasColumnName("volume");

                    b.Property<double>("Weight")
                        .HasColumnType("double precision")
                        .HasColumnName("weight");

                    b.HasKey("BuildingRecipeOutputId")
                        .HasName("pk_building_recipe_outputs");

                    b.HasIndex("BuildingRecipeId")
                        .HasDatabaseName("ix_building_recipe_outputs_building_recipe_id");

                    b.ToTable("building_recipe_outputs", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.Group", b =>
                {
                    b.Property<int>("GroupId")
                        .HasColumnType("integer")
                        .HasColumnName("group_id");

                    b.Property<string>("GroupName")
                        .IsRequired()
                        .HasMaxLength(16)
                        .HasColumnType("character varying(16)")
                        .HasColumnName("group_name");

                    b.Property<string>("GroupOwner")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("group_owner");

                    b.HasKey("GroupId")
                        .HasName("pk_groups");

                    b.HasIndex("GroupName")
                        .HasDatabaseName("ix_groups_group_name");

                    b.ToTable("groups", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.GroupAdmin", b =>
                {
                    b.Property<int>("GroupAdminId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("group_admin_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("GroupAdminId"));

                    b.Property<int>("GroupId")
                        .HasColumnType("integer")
                        .HasColumnName("group_id");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name");

                    b.HasKey("GroupAdminId")
                        .HasName("pk_group_admins");

                    b.HasIndex("GroupId")
                        .HasDatabaseName("ix_group_admins_group_id");

                    b.HasIndex("UserName")
                        .HasDatabaseName("ix_group_admins_user_name");

                    b.ToTable("group_admins", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.GroupPendingInvite", b =>
                {
                    b.Property<int>("GroupPendingInviteId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("group_pending_invite_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("GroupPendingInviteId"));

                    b.Property<bool>("Admin")
                        .HasColumnType("boolean")
                        .HasColumnName("admin");

                    b.Property<int>("GroupId")
                        .HasColumnType("integer")
                        .HasColumnName("group_id");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name");

                    b.HasKey("GroupPendingInviteId")
                        .HasName("pk_group_pending_invites");

                    b.HasIndex("GroupId")
                        .HasDatabaseName("ix_group_pending_invites_group_id");

                    b.HasIndex("UserName")
                        .HasDatabaseName("ix_group_pending_invites_user_name");

                    b.ToTable("group_pending_invites", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.GroupUser", b =>
                {
                    b.Property<int>("GroupUserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("group_user_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("GroupUserId"));

                    b.Property<int>("GroupId")
                        .HasColumnType("integer")
                        .HasColumnName("group_id");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name");

                    b.HasKey("GroupUserId")
                        .HasName("pk_group_users");

                    b.HasIndex("GroupId")
                        .HasDatabaseName("ix_group_users_group_id");

                    b.HasIndex("UserName")
                        .HasDatabaseName("ix_group_users_user_name");

                    b.ToTable("group_users", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.Permission", b =>
                {
                    b.Property<int>("PermissionId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("permission_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("PermissionId"));

                    b.Property<string>("GranteeUserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("grantee_user_name");

                    b.Property<string>("GrantorUserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("grantor_user_name");

                    b.Property<int>("GroupId")
                        .HasColumnType("integer")
                        .HasColumnName("group_id");

                    b.Property<bool>("MiscLiquidCurrency")
                        .HasColumnType("boolean")
                        .HasColumnName("misc_liquid_currency");

                    b.Property<bool>("MiscShipmentTracking")
                        .HasColumnType("boolean")
                        .HasColumnName("misc_shipment_tracking");

                    b.Property<bool>("ShipFlight")
                        .HasColumnType("boolean")
                        .HasColumnName("ship_flight");

                    b.Property<bool>("ShipFuelInventory")
                        .HasColumnType("boolean")
                        .HasColumnName("ship_fuel_inventory");

                    b.Property<bool>("ShipInformation")
                        .HasColumnType("boolean")
                        .HasColumnName("ship_information");

                    b.Property<bool>("ShipInventory")
                        .HasColumnType("boolean")
                        .HasColumnName("ship_inventory");

                    b.Property<bool>("ShipRepair")
                        .HasColumnType("boolean")
                        .HasColumnName("ship_repair");

                    b.Property<bool>("SitesBuildings")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_buildings");

                    b.Property<bool>("SitesExperts")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_experts");

                    b.Property<bool>("SitesLocation")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_location");

                    b.Property<bool>("SitesProductionLines")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_production_lines");

                    b.Property<bool>("SitesReclaimable")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_reclaimable");

                    b.Property<bool>("SitesRepair")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_repair");

                    b.Property<bool>("SitesWorkforces")
                        .HasColumnType("boolean")
                        .HasColumnName("sites_workforces");

                    b.Property<bool>("StorageContractItems")
                        .HasColumnType("boolean")
                        .HasColumnName("storage_contract_items");

                    b.Property<bool>("StorageInformation")
                        .HasColumnType("boolean")
                        .HasColumnName("storage_information");

                    b.Property<bool>("StorageItems")
                        .HasColumnType("boolean")
                        .HasColumnName("storage_items");

                    b.Property<bool>("StorageLocation")
                        .HasColumnType("boolean")
                        .HasColumnName("storage_location");

                    b.Property<bool>("TradeCXOS")
                        .HasColumnType("boolean")
                        .HasColumnName("trade_cxos");

                    b.Property<bool>("TradeContract")
                        .HasColumnType("boolean")
                        .HasColumnName("trade_contract");

                    b.HasKey("PermissionId")
                        .HasName("pk_permissions");

                    b.HasIndex("GranteeUserName")
                        .HasDatabaseName("ix_permissions_grantee_user_name");

                    b.HasIndex("GrantorUserName")
                        .HasDatabaseName("ix_permissions_grantor_user_name");

                    b.HasIndex("GroupId")
                        .HasDatabaseName("ix_permissions_group_id");

                    b.HasIndex("GrantorUserName", "GranteeUserName")
                        .IsUnique()
                        .HasDatabaseName("ix_permissions_grantor_user_name_grantee_user_name");

                    b.ToTable("permissions", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.Registration", b =>
                {
                    b.Property<int>("RegistrationId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("registration_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("RegistrationId"));

                    b.Property<string>("DisplayUserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("display_user_name");

                    b.Property<Guid>("RegistrationGuid")
                        .HasColumnType("uuid")
                        .HasColumnName("registration_guid");

                    b.Property<DateTime>("RegistrationTime")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("registration_time");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name");

                    b.HasKey("RegistrationId")
                        .HasName("pk_registrations");

                    b.HasIndex("UserName")
                        .HasDatabaseName("ix_registrations_user_name");

                    b.ToTable("registrations", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("UserId"));

                    b.Property<string>("DiscordName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("discord_name");

                    b.Property<string>("DisplayUserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("display_user_name");

                    b.Property<bool>("IsAdmin")
                        .HasColumnType("boolean")
                        .HasColumnName("is_admin");

                    b.Property<string>("PasswordHash")
                        .IsRequired()
                        .HasMaxLength(128)
                        .HasColumnType("character varying(128)")
                        .HasColumnName("password_hash");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(32)
                        .HasColumnType("character varying(32)")
                        .HasColumnName("user_name");

                    b.HasKey("UserId")
                        .HasName("pk_users");

                    b.HasIndex("UserName")
                        .IsUnique()
                        .HasDatabaseName("ix_users_user_name");

                    b.ToTable("users", (string)null);
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingCost", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.Building", "Building")
                        .WithMany("Costs")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_building_costs_buildings_building_id");

                    b.Navigation("Building");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipe", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.Building", "Building")
                        .WithMany("Recipes")
                        .HasForeignKey("BuildingId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_building_recipes_buildings_building_id");

                    b.Navigation("Building");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipeInput", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.BuildingRecipe", "BuildingRecipe")
                        .WithMany("Inputs")
                        .HasForeignKey("BuildingRecipeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_building_recipe_inputs_building_recipes_building_recipe_id");

                    b.Navigation("BuildingRecipe");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipeOutput", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.BuildingRecipe", "BuildingRecipe")
                        .WithMany("Outputs")
                        .HasForeignKey("BuildingRecipeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_building_recipe_outputs_building_recipes_building_recipe_id");

                    b.Navigation("BuildingRecipe");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.GroupAdmin", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.Group", "Group")
                        .WithMany("Admins")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_group_admins_groups_group_id");

                    b.Navigation("Group");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.GroupPendingInvite", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.Group", "Group")
                        .WithMany("PendingInvites")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_group_pending_invites_groups_group_id");

                    b.Navigation("Group");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.GroupUser", b =>
                {
                    b.HasOne("FIOAPI.DB.Model.Group", "Group")
                        .WithMany("Users")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_group_users_groups_group_id");

                    b.Navigation("Group");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.Building", b =>
                {
                    b.Navigation("Costs");

                    b.Navigation("Recipes");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.BuildingRecipe", b =>
                {
                    b.Navigation("Inputs");

                    b.Navigation("Outputs");
                });

            modelBuilder.Entity("FIOAPI.DB.Model.Group", b =>
                {
                    b.Navigation("Admins");

                    b.Navigation("PendingInvites");

                    b.Navigation("Users");
                });
#pragma warning restore 612, 618
        }
    }
}
