﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddFXData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "fx",
                columns: table => new
                {
                    fxid = table.Column<string>(type: "text", nullable: false),
                    ticker = table.Column<string>(type: "text", nullable: false),
                    base_currency_numeric_code = table.Column<int>(type: "integer", nullable: false),
                    base_currency_code = table.Column<string>(type: "text", nullable: false),
                    base_currency_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    base_currency_decimals = table.Column<int>(type: "integer", nullable: false),
                    quote_currency_numeric_code = table.Column<int>(type: "integer", nullable: false),
                    quote_currency_code = table.Column<string>(type: "text", nullable: false),
                    quote_currency_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    quote_currency_decimals = table.Column<int>(type: "integer", nullable: false),
                    decimals = table.Column<int>(type: "integer", nullable: false),
                    open = table.Column<double>(type: "double precision", nullable: false),
                    close = table.Column<double>(type: "double precision", nullable: false),
                    low = table.Column<double>(type: "double precision", nullable: false),
                    high = table.Column<double>(type: "double precision", nullable: false),
                    previous = table.Column<double>(type: "double precision", nullable: false),
                    traded = table.Column<int>(type: "integer", nullable: false),
                    volume = table.Column<int>(type: "integer", nullable: false),
                    bid = table.Column<double>(type: "double precision", nullable: false),
                    ask = table.Column<double>(type: "double precision", nullable: false),
                    spread = table.Column<double>(type: "double precision", nullable: false),
                    lot_size_amount = table.Column<int>(type: "integer", nullable: false),
                    lot_currency = table.Column<string>(type: "text", nullable: false),
                    fees_factor = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fx", x => x.fxid);
                });

            migrationBuilder.CreateTable(
                name: "fx_buy_orders",
                columns: table => new
                {
                    fx_buy_order_id = table.Column<string>(type: "text", nullable: false),
                    user_id = table.Column<string>(type: "text", nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    fxid = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fx_buy_orders", x => x.fx_buy_order_id);
                    table.ForeignKey(
                        name: "fk_fx_buy_orders_fx_fxid",
                        column: x => x.fxid,
                        principalTable: "fx",
                        principalColumn: "fxid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "fx_sell_orders",
                columns: table => new
                {
                    fx_sell_order_id = table.Column<string>(type: "text", nullable: false),
                    user_id = table.Column<string>(type: "text", nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    fxid = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fx_sell_orders", x => x.fx_sell_order_id);
                    table.ForeignKey(
                        name: "fk_fx_sell_orders_fx_fxid",
                        column: x => x.fxid,
                        principalTable: "fx",
                        principalColumn: "fxid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_fx_ticker",
                table: "fx",
                column: "ticker",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_fx_buy_orders_fxid",
                table: "fx_buy_orders",
                column: "fxid");

            migrationBuilder.CreateIndex(
                name: "ix_fx_buy_orders_user_company_code",
                table: "fx_buy_orders",
                column: "user_company_code");

            migrationBuilder.CreateIndex(
                name: "ix_fx_buy_orders_user_name",
                table: "fx_buy_orders",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_fx_sell_orders_fxid",
                table: "fx_sell_orders",
                column: "fxid");

            migrationBuilder.CreateIndex(
                name: "ix_fx_sell_orders_user_company_code",
                table: "fx_sell_orders",
                column: "user_company_code");

            migrationBuilder.CreateIndex(
                name: "ix_fx_sell_orders_user_name",
                table: "fx_sell_orders",
                column: "user_name");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "fx_buy_orders");

            migrationBuilder.DropTable(
                name: "fx_sell_orders");

            migrationBuilder.DropTable(
                name: "fx");
        }
    }
}
