﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddLocalMarketAds : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "traded",
                table: "fx",
                type: "double precision",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<double>(
                name: "lot_size_amount",
                table: "fx",
                type: "double precision",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.CreateTable(
                name: "local_market_ads",
                columns: table => new
                {
                    local_market_ad_id = table.Column<string>(type: "text", nullable: false),
                    local_market_id = table.Column<string>(type: "text", nullable: false),
                    natural_id = table.Column<string>(type: "text", nullable: false),
                    type = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    creator_id = table.Column<string>(type: "text", nullable: false),
                    creator_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    creator_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_ticker = table.Column<string>(type: "text", nullable: true),
                    material_amount = table.Column<int>(type: "integer", nullable: true),
                    price = table.Column<double>(type: "double precision", nullable: false),
                    currency = table.Column<string>(type: "text", nullable: false),
                    fulfillment_days = table.Column<int>(type: "integer", nullable: false),
                    minimum_rating = table.Column<string>(type: "character varying(1)", maxLength: 1, nullable: false),
                    creation_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    expiry = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    origin_natural_id = table.Column<string>(type: "text", nullable: false),
                    destination_natural_id = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_local_market_ads", x => x.local_market_ad_id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_local_market_id",
                table: "local_market_ads",
                column: "local_market_id");

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_natural_id",
                table: "local_market_ads",
                column: "natural_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "local_market_ads");

            migrationBuilder.AlterColumn<int>(
                name: "traded",
                table: "fx",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "double precision");

            migrationBuilder.AlterColumn<int>(
                name: "lot_size_amount",
                table: "fx",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "double precision");
        }
    }
}
