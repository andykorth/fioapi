﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddBuildingModels : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "buildings",
                columns: table => new
                {
                    building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    expertise = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    pioneers = table.Column<int>(type: "integer", nullable: false),
                    settlers = table.Column<int>(type: "integer", nullable: false),
                    technicians = table.Column<int>(type: "integer", nullable: false),
                    engineers = table.Column<int>(type: "integer", nullable: false),
                    scientists = table.Column<int>(type: "integer", nullable: false),
                    area_cost = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_buildings", x => x.building_id);
                });

            migrationBuilder.CreateTable(
                name: "building_costs",
                columns: table => new
                {
                    building_cost_id = table.Column<string>(type: "character varying(37)", maxLength: 37, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_costs", x => x.building_cost_id);
                    table.ForeignKey(
                        name: "fk_building_costs_buildings_building_id",
                        column: x => x.building_id,
                        principalTable: "buildings",
                        principalColumn: "building_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "building_recipes",
                columns: table => new
                {
                    building_recipe_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    duration_ms = table.Column<long>(type: "bigint", nullable: false),
                    building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_recipes", x => x.building_recipe_id);
                    table.ForeignKey(
                        name: "fk_building_recipes_buildings_building_id",
                        column: x => x.building_id,
                        principalTable: "buildings",
                        principalColumn: "building_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "building_recipe_inputs",
                columns: table => new
                {
                    building_recipe_input_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    building_recipe_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_recipe_inputs", x => x.building_recipe_input_id);
                    table.ForeignKey(
                        name: "fk_building_recipe_inputs_building_recipes_building_recipe_id",
                        column: x => x.building_recipe_id,
                        principalTable: "building_recipes",
                        principalColumn: "building_recipe_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "building_recipe_outputs",
                columns: table => new
                {
                    building_recipe_output_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    building_recipe_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_recipe_outputs", x => x.building_recipe_output_id);
                    table.ForeignKey(
                        name: "fk_building_recipe_outputs_building_recipes_building_recipe_id",
                        column: x => x.building_recipe_id,
                        principalTable: "building_recipes",
                        principalColumn: "building_recipe_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_building_costs_building_id",
                table: "building_costs",
                column: "building_id");

            migrationBuilder.CreateIndex(
                name: "ix_building_recipe_inputs_building_recipe_id",
                table: "building_recipe_inputs",
                column: "building_recipe_id");

            migrationBuilder.CreateIndex(
                name: "ix_building_recipe_outputs_building_recipe_id",
                table: "building_recipe_outputs",
                column: "building_recipe_id");

            migrationBuilder.CreateIndex(
                name: "ix_building_recipes_building_id",
                table: "building_recipes",
                column: "building_id");

            migrationBuilder.CreateIndex(
                name: "ix_buildings_ticker",
                table: "buildings",
                column: "ticker");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "building_costs");

            migrationBuilder.DropTable(
                name: "building_recipe_inputs");

            migrationBuilder.DropTable(
                name: "building_recipe_outputs");

            migrationBuilder.DropTable(
                name: "building_recipes");

            migrationBuilder.DropTable(
                name: "buildings");
        }
    }
}
