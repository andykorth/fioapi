﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddCompanies : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "companies",
                columns: table => new
                {
                    id = table.Column<string>(type: "text", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    founded = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    user_name = table.Column<string>(type: "text", nullable: false),
                    country_id = table.Column<string>(type: "text", nullable: false),
                    country_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    country_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    corporation_id = table.Column<string>(type: "text", nullable: true),
                    corporation_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    corporation_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    current_apex_representation_level = table.Column<int>(type: "integer", nullable: false),
                    overall_rating = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: false),
                    ratings_earliest_contract = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    rating_contract_count = table.Column<int>(type: "integer", nullable: false),
                    reputation_entity_name = table.Column<string>(type: "text", nullable: true),
                    reputation = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_companies", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "company_planets",
                columns: table => new
                {
                    company_planet_id = table.Column<string>(type: "text", nullable: false),
                    planet_id = table.Column<string>(type: "text", nullable: false),
                    planet_name = table.Column<string>(type: "text", nullable: false),
                    planet_natural_id = table.Column<string>(type: "text", nullable: false),
                    company_id = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_company_planets", x => x.company_planet_id);
                    table.ForeignKey(
                        name: "fk_company_planets_companies_company_id",
                        column: x => x.company_id,
                        principalTable: "companies",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_company_planets_company_id",
                table: "company_planets",
                column: "company_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "company_planets");

            migrationBuilder.DropTable(
                name: "companies");
        }
    }
}
