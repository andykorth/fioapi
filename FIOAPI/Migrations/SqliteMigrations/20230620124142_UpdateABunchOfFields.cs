﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class UpdateABunchOfFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "chat_channels",
                columns: table => new
                {
                    chat_channel_id = table.Column<string>(type: "TEXT", nullable: false),
                    type = table.Column<string>(type: "TEXT", maxLength: 10, nullable: false),
                    natural_id = table.Column<string>(type: "TEXT", maxLength: 10, nullable: true),
                    display_name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    user_count = table.Column<int>(type: "INTEGER", nullable: false),
                    creation_timestamp = table.Column<long>(type: "INTEGER", nullable: false),
                    last_activity_timestamp = table.Column<long>(type: "INTEGER", nullable: false),
                    user_name_submitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chat_channels", x => x.chat_channel_id);
                });

            migrationBuilder.CreateTable(
                name: "chat_messages",
                columns: table => new
                {
                    chat_message_id = table.Column<string>(type: "TEXT", nullable: false),
                    type = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    sender_id = table.Column<string>(type: "TEXT", nullable: false),
                    sender_user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    message_text = table.Column<string>(type: "TEXT", maxLength: 1024, nullable: true),
                    message_timestamp = table.Column<long>(type: "INTEGER", nullable: false),
                    message_deleted = table.Column<bool>(type: "INTEGER", nullable: false),
                    deleted_by_user_id = table.Column<string>(type: "TEXT", nullable: true),
                    deleted_by_user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    user_name_submitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "TEXT", nullable: false),
                    chat_channel_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chat_messages", x => x.chat_message_id);
                    table.ForeignKey(
                        name: "fk_chat_messages_chat_channels_chat_channel_id",
                        column: x => x.chat_channel_id,
                        principalTable: "chat_channels",
                        principalColumn: "chat_channel_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_chat_messages_chat_channel_id",
                table: "chat_messages",
                column: "chat_channel_id");

            migrationBuilder.CreateIndex(
                name: "ix_chat_messages_message_timestamp",
                table: "chat_messages",
                column: "message_timestamp");

            migrationBuilder.CreateIndex(
                name: "ix_chat_messages_timestamp",
                table: "chat_messages",
                column: "timestamp");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "chat_messages");

            migrationBuilder.DropTable(
                name: "chat_channels");
        }
    }
}
