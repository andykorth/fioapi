﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class AddLocalMarketAds : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "traded",
                table: "fx",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AlterColumn<double>(
                name: "lot_size_amount",
                table: "fx",
                type: "REAL",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.CreateTable(
                name: "local_market_ads",
                columns: table => new
                {
                    local_market_ad_id = table.Column<string>(type: "TEXT", nullable: false),
                    local_market_id = table.Column<string>(type: "TEXT", nullable: false),
                    natural_id = table.Column<string>(type: "TEXT", nullable: false),
                    type = table.Column<string>(type: "TEXT", maxLength: 20, nullable: false),
                    weight = table.Column<double>(type: "REAL", nullable: false),
                    volume = table.Column<double>(type: "REAL", nullable: false),
                    creator_id = table.Column<string>(type: "TEXT", nullable: false),
                    creator_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    creator_code = table.Column<string>(type: "TEXT", maxLength: 4, nullable: false),
                    material_ticker = table.Column<string>(type: "TEXT", nullable: true),
                    material_amount = table.Column<int>(type: "INTEGER", nullable: true),
                    price = table.Column<double>(type: "REAL", nullable: false),
                    currency = table.Column<string>(type: "TEXT", nullable: false),
                    fulfillment_days = table.Column<int>(type: "INTEGER", nullable: false),
                    minimum_rating = table.Column<string>(type: "TEXT", maxLength: 1, nullable: false),
                    creation_time = table.Column<DateTime>(type: "TEXT", nullable: false),
                    expiry = table.Column<DateTime>(type: "TEXT", nullable: false),
                    origin_natural_id = table.Column<string>(type: "TEXT", nullable: false),
                    destination_natural_id = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_local_market_ads", x => x.local_market_ad_id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_local_market_id",
                table: "local_market_ads",
                column: "local_market_id");

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_natural_id",
                table: "local_market_ads",
                column: "natural_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "local_market_ads");

            migrationBuilder.AlterColumn<int>(
                name: "traded",
                table: "fx",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL");

            migrationBuilder.AlterColumn<int>(
                name: "lot_size_amount",
                table: "fx",
                type: "INTEGER",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "REAL");
        }
    }
}
