﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "api_keys",
                columns: table => new
                {
                    api_key_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    key = table.Column<Guid>(type: "TEXT", nullable: false),
                    user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    application = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    allow_writes = table.Column<bool>(type: "INTEGER", nullable: false),
                    create_time = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_api_keys", x => x.api_key_id);
                });

            migrationBuilder.CreateTable(
                name: "groups",
                columns: table => new
                {
                    group_id = table.Column<int>(type: "INTEGER", nullable: false),
                    group_name = table.Column<string>(type: "TEXT", maxLength: 16, nullable: false),
                    group_owner = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_groups", x => x.group_id);
                });

            migrationBuilder.CreateTable(
                name: "registrations",
                columns: table => new
                {
                    registration_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    display_user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    registration_guid = table.Column<Guid>(type: "TEXT", nullable: false),
                    registration_time = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registrations", x => x.registration_id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    display_user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    password_hash = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    is_admin = table.Column<bool>(type: "INTEGER", nullable: false),
                    discord_id = table.Column<string>(type: "TEXT", maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_users", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "group_admins",
                columns: table => new
                {
                    group_admin_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_group_admins", x => x.group_admin_id);
                    table.ForeignKey(
                        name: "fk_group_admins_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "group_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "group_pending_invites",
                columns: table => new
                {
                    group_pending_invite_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_group_pending_invites", x => x.group_pending_invite_id);
                    table.ForeignKey(
                        name: "fk_group_pending_invites_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "group_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "group_users",
                columns: table => new
                {
                    group_user_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_group_users", x => x.group_user_id);
                    table.ForeignKey(
                        name: "fk_group_users_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "group_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_api_keys_key",
                table: "api_keys",
                column: "key");

            migrationBuilder.CreateIndex(
                name: "ix_api_keys_user_name",
                table: "api_keys",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_group_admins_group_id",
                table: "group_admins",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_admins_user_name",
                table: "group_admins",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_group_pending_invites_group_id",
                table: "group_pending_invites",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_pending_invites_user_name",
                table: "group_pending_invites",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_group_users_group_id",
                table: "group_users",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_users_user_name",
                table: "group_users",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_groups_group_name",
                table: "groups",
                column: "group_name");

            migrationBuilder.CreateIndex(
                name: "ix_registrations_user_name",
                table: "registrations",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_users_user_name",
                table: "users",
                column: "user_name",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "api_keys");

            migrationBuilder.DropTable(
                name: "group_admins");

            migrationBuilder.DropTable(
                name: "group_pending_invites");

            migrationBuilder.DropTable(
                name: "group_users");

            migrationBuilder.DropTable(
                name: "registrations");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "groups");
        }
    }
}
