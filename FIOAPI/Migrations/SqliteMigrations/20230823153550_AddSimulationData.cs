﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class AddSimulationData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "simulation_data",
                columns: table => new
                {
                    simulation_data_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    simulation_interval = table.Column<int>(type: "INTEGER", nullable: false),
                    flight_stl_factor = table.Column<int>(type: "INTEGER", nullable: false),
                    flight_ftl_factor = table.Column<int>(type: "INTEGER", nullable: false),
                    planetary_motion_factor = table.Column<int>(type: "INTEGER", nullable: false),
                    parsec_length = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_simulation_data", x => x.simulation_data_id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "simulation_data");
        }
    }
}
