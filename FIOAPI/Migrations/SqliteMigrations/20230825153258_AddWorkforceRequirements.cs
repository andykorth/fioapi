﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class AddWorkforceRequirements : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "workforce_requirements",
                columns: table => new
                {
                    workforce_requirement_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    level = table.Column<string>(type: "TEXT", maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_requirements", x => x.workforce_requirement_id);
                });

            migrationBuilder.CreateTable(
                name: "workforce_requirement_need",
                columns: table => new
                {
                    workforce_requirement_need_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    essential = table.Column<bool>(type: "INTEGER", nullable: false),
                    material_name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    material_ticker = table.Column<string>(type: "TEXT", maxLength: 4, nullable: false),
                    material_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    units_per100 = table.Column<double>(type: "REAL", nullable: false),
                    workforce_requirement_id = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_requirement_need", x => x.workforce_requirement_need_id);
                    table.ForeignKey(
                        name: "fk_workforce_requirement_need_workforce_requirements_workforce_requirement_id",
                        column: x => x.workforce_requirement_id,
                        principalTable: "workforce_requirements",
                        principalColumn: "workforce_requirement_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_workforce_requirement_need_workforce_requirement_id",
                table: "workforce_requirement_need",
                column: "workforce_requirement_id");

            migrationBuilder.CreateIndex(
                name: "ix_workforce_requirements_level",
                table: "workforce_requirements",
                column: "level");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "workforce_requirement_need");

            migrationBuilder.DropTable(
                name: "workforce_requirements");
        }
    }
}
