﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class UpdatedFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "discord_id",
                table: "users");

            migrationBuilder.AddColumn<string>(
                name: "discord_name",
                table: "users",
                type: "TEXT",
                maxLength: 32,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "permissions",
                columns: table => new
                {
                    permission_id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    grantor_user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    grantee_user_name = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "INTEGER", nullable: false),
                    ship_information = table.Column<bool>(type: "INTEGER", nullable: false),
                    ship_repair = table.Column<bool>(type: "INTEGER", nullable: false),
                    ship_flight = table.Column<bool>(type: "INTEGER", nullable: false),
                    ship_inventory = table.Column<bool>(type: "INTEGER", nullable: false),
                    ship_fuel_inventory = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_location = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_workforces = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_experts = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_buildings = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_repair = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_reclaimable = table.Column<bool>(type: "INTEGER", nullable: false),
                    sites_production_lines = table.Column<bool>(type: "INTEGER", nullable: false),
                    storage_location = table.Column<bool>(type: "INTEGER", nullable: false),
                    storage_information = table.Column<bool>(type: "INTEGER", nullable: false),
                    storage_items = table.Column<bool>(type: "INTEGER", nullable: false),
                    storage_contract_items = table.Column<bool>(type: "INTEGER", nullable: false),
                    trade_contract = table.Column<bool>(type: "INTEGER", nullable: false),
                    trade_cxos = table.Column<bool>(type: "INTEGER", nullable: false),
                    misc_shipment_tracking = table.Column<bool>(type: "INTEGER", nullable: false),
                    misc_liquid_currency = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permissions", x => x.permission_id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_permissions_grantee_user_name",
                table: "permissions",
                column: "grantee_user_name");

            migrationBuilder.CreateIndex(
                name: "ix_permissions_grantor_user_name",
                table: "permissions",
                column: "grantor_user_name");

            migrationBuilder.CreateIndex(
                name: "ix_permissions_group_id",
                table: "permissions",
                column: "group_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "permissions");

            migrationBuilder.DropColumn(
                name: "discord_name",
                table: "users");

            migrationBuilder.AddColumn<string>(
                name: "discord_id",
                table: "users",
                type: "TEXT",
                maxLength: 40,
                nullable: false,
                defaultValue: "");
        }
    }
}
