﻿namespace FIOAPI.Integration.Test
{
    public enum CharacterSet
    {
        UserNameCharacters,
        PasswordCharacters
    }

    public static class Utils
    {
        private const string UserNameCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.";
        private const string PasswordCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_-=+/\\'\"<>,.`~";

        private static Random Rnd = new Random((int)DateTime.Now.Ticks);
        
        private static string GetCharacterSet(CharacterSet set)
        {
            switch(set)
            {
                case CharacterSet.UserNameCharacters:
                    return UserNameCharacters;
                case CharacterSet.PasswordCharacters:
                    return PasswordCharacters;
                default:
                    throw new InvalidOperationException();
            }
        }

        public static string GenerateRandomString(int Length, CharacterSet set)
        {
            string ValidCharacters = GetCharacterSet(set);

            var ResultChars = new char[Length];
            for (int i = 0; i < Length; ++i)
            {
                ResultChars[i] = ValidCharacters[Rnd.Next(ValidCharacters.Length)];
            }

            return new string(ResultChars);
        }

        public static string GetCSProjDirectory()
        {
            return Directory.GetParent(Environment.CurrentDirectory)!.Parent!.Parent!.FullName;
        }
    }
}
