﻿using System.IO;

namespace FIOAPI.Integration.Test
{
    public enum AuthType
    {
        Bearer,
        APIKey
    }

    public class AuthDef
    {
        public AuthType AuthType { get; set; }
        public string Token { get; set; } = "";
    }

    public class Request
    {
        public HttpClient HttpClient
        {
            get; private set;
        }

        public HttpMethod Method
        {
            get; set;
        } = HttpMethod.Get;

        public string EndPoint
        {
            get; set;
        }

        public AuthDef? AuthDef
        {
            get; set;
        } = null;

        public string? Payload
        {
            get; private set;
        } = null;

        public string? ContentType
        {
            get; set;
        } = null;

        public HttpStatusCode? StatusCode
        {
            get; private set;
        } = null;

        public string? ResponsePayload
        {
            get; private set;
        } = null;

        public HttpResponseHeaders? ResponseHeaders
        {
            get; private set;
        } = null;

        public Exception? LastException
        {
            get; private set;
        } = null;

        /// <summary>
        /// Constructs a new request
        /// </summary>
        /// <param name="Method">HttpMethod</param>
        /// <param name="EndPoint">Endpoint</param>
        /// <param name="AuthDef">AuthToken to use</param>
        /// <param name="Payload"></param>
        /// <param name="ContentType"></param>
        public Request(HttpClient Client, HttpMethod Method, string EndPoint, AuthDef? AuthDef = null, string ContentType = "text/plain")
        {
            if (String.IsNullOrWhiteSpace(EndPoint))
            {
                throw new ArgumentNullException(nameof(EndPoint));
            }

            if (AuthDef != null && String.IsNullOrWhiteSpace(AuthDef.Token))
            {
                throw new ArgumentException($"{nameof(AuthDef)}'s {nameof(AuthDef.Token)} is not set.");
            }

            HttpClient = Client;
            this.Method = Method;
            this.EndPoint = EndPoint;
            this.AuthDef = AuthDef;
            this.ContentType = ContentType;
        }

        private void Reset()
        {
            StatusCode = null;
            ResponsePayload = null;
            LastException = null;
        }

        public void SetPayloadFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new ArgumentException($"File path {filePath} does not exist.");
            }
            
            if (filePath.ToLower().EndsWith(".json"))
            {
                SetPayload(File.ReadAllText(filePath), "application/json");
            }
            else
            {
                SetPayload(File.ReadAllText(filePath));
            }
        }

        public void SetPayload<PayloadObj>(PayloadObj payloadObj) where PayloadObj : class
        {
            if (payloadObj is null)
            {
                throw new ArgumentNullException(nameof(PayloadObj));
            }

            SetPayload(JsonSerializer.Serialize(payloadObj), "application/json");
        }

        public void SetPayload(string Payload, string ContentType = "text/plain")
        {
            if (string.IsNullOrWhiteSpace(Payload))
            {
                throw new ArgumentException($"{nameof(Payload)} is null or whitespace"); 
            }

            this.Payload = Payload;
            this.ContentType = ContentType;
        }

        public async Task<JsonRepr?> GetResponseAsync<JsonRepr>() => await GetResponseAsync<JsonRepr>(CancellationToken.None).ConfigureAwait(false);
        public async Task<JsonRepr?> GetResponseAsync<JsonRepr>(CancellationToken ct)
        {
            var stringResponse = await GetStringResponseAsync(ct).ConfigureAwait(false);
            if (StatusCode != HttpStatusCode.InternalServerError && !String.IsNullOrWhiteSpace(stringResponse))
            {
                try
                {
                    return JsonSerializer.Deserialize<JsonRepr>(stringResponse);
                }
                catch (Exception ex)
                {
                    if (ex is NotSupportedException || ex is JsonException)
                    {
                        // Bad json data
                        LastException = ex;
                    }
                    else
                    {
                        // Throw on all other cases
                        throw;
                    }
                }
            }

            return default;
        }

        public async Task<string?> GetStringResponseAsync() => await GetStringResponseAsync(CancellationToken.None).ConfigureAwait(false);
        public async Task<string?> GetStringResponseAsync(CancellationToken ct)
        {
            Reset();

            try
            {
                using (var message = new HttpRequestMessage(Method, EndPoint))
                {
                    message.Headers.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));

                    if (AuthDef != null)
                    {
                        switch (AuthDef.AuthType)
                        {
                            case AuthType.Bearer:
                                message.Headers.Add("Authorization", $"Bearer {AuthDef.Token}");
                                break;
                            case AuthType.APIKey:
                                message.Headers.Add("Authorization", $"FIOAPIKey {AuthDef.Token}");
                                break;
                            default:
                                Trace.Assert(false, "Undefined AuthType");
                                break;
                        }
                    }

                    if (!String.IsNullOrWhiteSpace(Payload))
                    {
                        message.Content = new StringContent(Payload, Encoding.UTF8, ContentType ?? "text/plain");
                    }

                    using var response = await HttpClient.SendAsync(message, HttpCompletionOption.ResponseContentRead, ct).ConfigureAwait(false);
                    StatusCode = response.StatusCode;
                    ResponsePayload = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    ResponseHeaders = response.Headers;
                    return ResponsePayload;
                }
            }
            catch (HttpRequestException e)
            {
                LastException = e;
                if (e.StatusCode != null)
                {
                    StatusCode = (HttpStatusCode)e.StatusCode;
                }
            }
            catch (TaskCanceledException)
            {
                StatusCode = HttpStatusCode.RequestTimeout;
            }

            return ResponsePayload;
        }

        public async Task GetNoResultResponseAsync() => await GetNoResultResponseAsync(CancellationToken.None).ConfigureAwait(false);
        public async Task GetNoResultResponseAsync(CancellationToken ct)
        {
            Reset();
            await GetStringResponseAsync(ct).ConfigureAwait(false);
        }
    }
}
