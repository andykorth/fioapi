﻿using FIOAPI.Test;

namespace FIOAPI.Integration.Test.XUnitHelpers
{
    public class TestUserAccount : UserAccount
    {
        public TestUserAccount(bool Admin = false)
            : base(Utils.GenerateRandomString(16, CharacterSet.UserNameCharacters), Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters), Admin)
        {

        }

        public AuthDef GetReadDef() => GetAuthDef(false);

        public AuthDef GetWriteDef() => GetAuthDef(true);

        public AuthDef GetAuthDef(bool Writable)
        {
            return new AuthDef()
            {
                AuthType = AuthType.APIKey,
                Token = Writable ? UserWriteAPIKey : UserReadOnlyAPIKey,
            };
        }

        public static TestUserAccount MakeAccount()
        {
            return new TestUserAccount(false);
        }

        public static TestUserAccount MakeAdminAccount()
        {
            return new TestUserAccount(true);
        }

        public static List<TestUserAccount> MakeAccounts(int Count)
        {
            return Enumerable.Range(0, Count)
                .Select(_ => new TestUserAccount(false))
                .ToList();
        }

        public static List<TestUserAccount> MakeAdminAccounts(int Count)
        {
            return Enumerable.Range(0, Count)
                .Select(_ => new TestUserAccount(true))
                .ToList();
        }
    }
}
