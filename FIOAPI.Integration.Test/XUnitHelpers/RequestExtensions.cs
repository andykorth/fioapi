﻿namespace FIOAPI.Integration.Test.XUnitHelpers
{
    public static class RequestExtensions
    {
        private static void AssertBaseline(this Request request)
        {
            Assert.NotNull(request);
            Assert.NotNull(request.StatusCode);
        }

        private static void NotExpectingBadRequest(this Request request)
        {
            if (request.StatusCode == HttpStatusCode.BadRequest && !string.IsNullOrEmpty(request.ResponsePayload))
            {
#pragma warning disable xUnit2003
                // Explicitly equality failure so xunit emits the BadRequest payload
                Assert.Equal(null, request.ResponsePayload);
#pragma warning restore xUnit2003
            }
        }

        public static void AssertOK(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            if (request.ResponseHeaders != null)
            {
                Assert.DoesNotContain(request.ResponseHeaders, h => h.Key == ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutHeader);
            }
        }

        public static void AssertBadRequest(this Request request)
        {
            request.AssertBaseline();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);
        }

        public static void AssertNotAcceptable(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.NotAcceptable, request.StatusCode);
        }

        public static void AssertNotFound(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);
        }

        public static void AssertForbidden(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);
        }

        public static void AssertUnauthorized(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);
        }

        public static void AssertNoContent(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.NoContent, request.StatusCode);
        }

        public static void AssertHydrationTimeout(this Request request)
        {
            request.NotExpectingBadRequest();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(request.ResponseHeaders);
            Assert.Contains(request.ResponseHeaders, h => h.Key == ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutHeader);
        }

        public static void AssertStatusCode(this Request request, int StatusCode)
        {
            request.AssertBaseline();
            if (StatusCode != 400)
            {
                request.NotExpectingBadRequest();
            }
            Assert.Equal(StatusCode, (int)request.StatusCode!);
        }

        public static async Task AssertEnforceAuth(this Request request)
        {
            Assert.Null(request.AuthDef);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();
        }

        public static async Task AssertEnforceWrite(this Request request, bool RequireAdmin = false)
        {
            var userAccount = RequireAdmin ? TestUserAccount.MakeAdminAccount() : TestUserAccount.MakeAccount();
            request.AuthDef = userAccount.GetReadDef();
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Also test user-write
            if (RequireAdmin)
            {
                userAccount = TestUserAccount.MakeAccount();
                request.AuthDef = userAccount.GetWriteDef();
                await request.GetNoResultResponseAsync();
                Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);
            }
        }
    }
}
