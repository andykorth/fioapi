﻿using System.IO;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace FIOAPI.Integration.Test.XUnitHelpers
{
    public class FIOAPIWebApplicationFactory<TProgram> : WebApplicationFactory<TProgram> where TProgram : class
    {
        private static EventWaitHandle deleteDatabaseEwh = new EventWaitHandle(false, EventResetMode.ManualReset);
        private static object deleteDatabaseLockObj = new object();

        private static EventWaitHandle createHostEwh = new EventWaitHandle(false, EventResetMode.ManualReset);
        private static object createHostLockObj = new object();

        private static IHost host = null!;

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            if (Monitor.TryEnter(deleteDatabaseLockObj))
            {
                if (File.Exists("fioapi.db"))
                {
                    File.Delete("fioapi.db");
                }

                deleteDatabaseEwh.Set();
            }

            deleteDatabaseEwh.WaitOne();

            base.ConfigureWebHost(builder);

            builder.UseSetting("migrate", "true");
            builder.UseSetting("run-after-migrate", "true");
            builder.UseSetting("testing", "true");

            builder.UseEnvironment("Development");

        }
        
        protected override IHost CreateHost(IHostBuilder builder)
        {
            if (Monitor.TryEnter(createHostLockObj))
            {
                host = base.CreateHost(builder);

                createHostEwh.Set();
            }

            createHostEwh.WaitOne();
            return host;
        }
    }
}
