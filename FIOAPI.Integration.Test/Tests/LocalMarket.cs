﻿using FIOAPI.DB.Model;
using Xunit.Sdk;

namespace FIOAPI.Integration.Test.Tests
{
    [TestCaseOrderer("FIOAPI.Integration.Test.XUnitHelpers.PriorityOrderer", "FIOAPI.Integration.Test")]
    public class LocalMarket : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        private string LocalMarketTestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "LocalMarket", "PATH_localmarkets-_-ads");
            }
        }

        public LocalMarket(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact, TestPriority(0)]
        public async Task PutLocalMarketAds()
        {
            var Account = TestUserAccount.MakeAccount();

            var TestFiles = Directory.GetFiles(LocalMarketTestDataFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var localMarketData = JsonSerializer.Deserialize<Payloads.LocalMarket.PATH_LOCALMARKET_AD>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(localMarketData);
            Assert.True(localMarketData.PassesValidation());

            // Test not logged in & read-only api key
            request = new Request(client, HttpMethod.Put, "/localmarket");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // Test hydration timeout
            var previousCreatorName = localMarketData.payload.message.payload.body.First().creator.name;
            localMarketData.payload.message.payload.body.First().creator.name = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/localmarket", Account.GetWriteDef());
            request.SetPayload(localMarketData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            localMarketData.payload.message.payload.body.First().creator.name = previousCreatorName;

            // Test bad payload
            var prevId = localMarketData.payload.message.payload.body.First().id;
            localMarketData.payload.message.payload.body.First().id = "5";
            Assert.False(localMarketData.PassesValidation());
            request.SetPayload(localMarketData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            localMarketData.payload.message.payload.body.First().id = prevId;

            // Success
            request.SetPayload(localMarketData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            foreach (var TestFile in TestFiles)
            {
                var testLocalMarketData = JsonSerializer.Deserialize<Payloads.LocalMarket.PATH_LOCALMARKET_AD>(File.ReadAllText(TestFile));
                Assert.NotNull(testLocalMarketData);
                bool PassesValidation = testLocalMarketData.PassesValidation();
                Assert.True(PassesValidation);

                request.SetPayload(testLocalMarketData);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task GetLocalMarketAds()
        {
            var TestFiles = Directory.GetFiles(LocalMarketTestDataFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            var localMarketData = JsonSerializer.Deserialize<Payloads.LocalMarket.PATH_LOCALMARKET_AD>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(localMarketData);
            Assert.True(localMarketData.PassesValidation());

            //var lmNaturalId = localMarketData.payload.message.payload.body[0].address.lines.Last().entity.naturalId;
            var lmNaturalId = "KW-688c";
            request = new Request(client, HttpMethod.Get, $"/localmarket/{lmNaturalId}");
            var results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.NotNull(results);

            request = new Request(client, HttpMethod.Get, $"/localmarket/{lmNaturalId}?include_buys=false&include_sells=false");
            results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.NotEmpty(results);

            results.ForEach(result =>
            {
                Assert.True(result.Type == "COMMODITY_SHIPPING");
            });

            request = new Request(client, HttpMethod.Get, $"/localmarket/{lmNaturalId}?include_buys=false&include_sells=false&include_shipments=false");
            results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.Empty(results);
        }
    }
}
