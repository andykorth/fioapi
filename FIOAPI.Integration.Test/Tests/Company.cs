﻿using FIOAPI.DB.Model;
using Xunit.Sdk;

namespace FIOAPI.Integration.Test.Tests
{
    [TestCaseOrderer("FIOAPI.Integration.Test.XUnitHelpers.PriorityOrderer", "FIOAPI.Integration.Test")]
    public class Company : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        private string TestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Company", "PATH_companies-_");
            }
        }

        public Company(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact, TestPriority(0)]
        public async Task PutCompanies()
        {
            var Account = TestUserAccount.MakeAccount();

            var TestFiles = Directory.GetFiles(TestDataFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            string fileAsText = File.ReadAllText(FirstTestFile);
            var companyData = JsonSerializer.Deserialize<Payloads.Company.PATH_COMPANY>(fileAsText);
            Assert.NotNull(companyData);
            Assert.True(companyData.PassesValidation_Throw(), "Does not pass Company validation.");

            // Test not logged in & read-only api key
            request = new Request(client, HttpMethod.Put, "/company");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // Test hydration timeout
            var previousCreatorName = companyData.payload.message.payload.body.name;
            companyData.payload.message.payload.body.name = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/company", Account.GetWriteDef());
            request.SetPayload(companyData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            companyData.payload.message.payload.body.name = previousCreatorName;

            // Test bad payload
            var prevId = companyData.payload.message.payload.body.id;
            companyData.payload.message.payload.body.id = "5";
            Assert.False(companyData.PassesValidation());
            request.SetPayload(companyData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            companyData.payload.message.payload.body.id = prevId;

            // Success
            request.SetPayload(companyData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            foreach (var TestFile in TestFiles)
            {
                var testCompanyData = JsonSerializer.Deserialize<Payloads.Company.PATH_COMPANY>(File.ReadAllText(TestFile));
                Assert.NotNull(testCompanyData);
                Assert.True(testCompanyData.PassesValidation_Throw(), "Does not pass Company validation.");

                request.SetPayload(testCompanyData);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task GetCompany()
        {
            var TestFiles = Directory.GetFiles(TestDataFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            var data = JsonSerializer.Deserialize<Payloads.Company.PATH_COMPANY>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(data);
            Assert.True(data.PassesValidation());

            var companyCode = "ARCL";
            request = new Request(client, HttpMethod.Get, $"/company/{companyCode}");
            var results = await request.GetResponseAsync<List<DB.Model.Company>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.NotEmpty(results);

            results.ForEach(result =>
            {
                Assert.Equal("ARCL", result.code);
            });

        }

    }
}
