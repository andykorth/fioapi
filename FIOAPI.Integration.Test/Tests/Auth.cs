namespace FIOAPI.Integration.Test.Tests
{
    public class AuthSharedState
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid RegistrationGuid { get; set; }
        public string? JWTToken { get; set; }
        public Guid ReadOnlyAPIKey { get; set; }
        public Guid WriteAPIKey { get; set; }

        public AuthSharedState()
        {
            UserName = Utils.GenerateRandomString(16, CharacterSet.UserNameCharacters);
            Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
        }
    }

    [TestCaseOrderer("FIOAPI.Integration.Test.XUnitHelpers.PriorityOrderer", "FIOAPI.Integration.Test")]
#pragma warning disable xUnit1033 // Test classes decorated with 'Xunit.IClassFixture<TFixture>' or 'Xunit.ICollectionFixture<TFixture>' should add a constructor argument of type TFixture
    public class Auth : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>, IClassFixture<AuthSharedState>
#pragma warning restore xUnit1033 // Test classes decorated with 'Xunit.IClassFixture<TFixture>' or 'Xunit.ICollectionFixture<TFixture>' should add a constructor argument of type TFixture
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private AuthSharedState _state;

        private readonly HttpClient client;

        private Request? request;

        private readonly string UserName;
        private string Password;
        private Guid RegistrationGuid;
        private string? JWTToken;
        private Guid ReadOnlyAPIKey;
        private Guid WriteAPIKey;

        public Auth(FIOAPIWebApplicationFactory<Program> factory, AuthSharedState state)
        {
            _factory = factory;
            _state = state;

            client = _factory.CreateClient();

            UserName = _state.UserName;
            Password = _state.Password;
            RegistrationGuid = _state.RegistrationGuid;
            JWTToken = _state.JWTToken;
            ReadOnlyAPIKey = _state.ReadOnlyAPIKey;
            WriteAPIKey = _state.WriteAPIKey;
        }

        private void StoreSharedState()
        {
            _state.UserName = UserName;
            _state.Password = Password;
            _state.RegistrationGuid = RegistrationGuid;
            _state.JWTToken = JWTToken;
            _state.ReadOnlyAPIKey = ReadOnlyAPIKey;
            _state.WriteAPIKey = WriteAPIKey;
        }

        [Fact, TestPriority(0)]
        public async Task AutoRequestRegister()
        {
            var autoRequestRegisterPayload = new Payloads.Auth.AutoRequestRegister();
            autoRequestRegisterPayload.payload = new Payloads.Auth.AutoRequestRegisterPayload();
            autoRequestRegisterPayload.payload.username = UserName;
            autoRequestRegisterPayload.payload.admin = false;
            Assert.True(autoRequestRegisterPayload.PassesValidation());

            request = new Request(client, HttpMethod.Post, "/auth/autorequestregister");
            Assert.Equal("text/plain", request.ContentType);
            request.SetPayload(autoRequestRegisterPayload);
            Assert.Equal("application/json", request.ContentType);

            var response = await request.GetResponseAsync<Payloads.Auth.AutoRequestRegisterResponse>();
            Assert.NotNull(response);
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            Assert.True(response.PassesValidation());
            Assert.Equal(UserName, response.UserName);
            Assert.NotEqual(Guid.Empty, response.RegistrationGuid);
            Assert.NotEqual(DateTime.MinValue, response.RegistrationTime);
            Assert.NotEqual(DateTime.MaxValue, response.RegistrationTime);

            Assert.True(response.PassesValidation());

            RegistrationGuid = response.RegistrationGuid;
            StoreSharedState();
        }

        [Fact, TestPriority(1)]
        public async Task Register()
        {
            request = new Request(client, HttpMethod.Post, "/auth/register");

            var registerPayload = new Payloads.Auth.Register();
            registerPayload.UserName = UserName;
            registerPayload.Password = Password;

            // Intentionally not including Guid
            Assert.False(registerPayload.PassesValidation());
            request.SetPayload(registerPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Not the right guid
            registerPayload.RegistrationGuid = Guid.NewGuid();
            Assert.True(registerPayload.PassesValidation());
            request.SetPayload(registerPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Success, the correct guid
            registerPayload.RegistrationGuid = RegistrationGuid;
            Assert.True(registerPayload.PassesValidation());
            request.SetPayload(registerPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
        }

        [Fact, TestPriority(2)]
        public async Task Login()
        {
            request = new Request(client, HttpMethod.Post, "/auth/login");

            var loginPayload = new Payloads.Auth.Login();
            loginPayload.UserName = "ab";
            loginPayload.Password = "cd";
            Assert.False(loginPayload.PassesValidation());

            // Intentional bad request
            request.SetPayload(loginPayload);
            var response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Intentional bad username & password
            loginPayload.UserName = Utils.GenerateRandomString(16, CharacterSet.UserNameCharacters);
            loginPayload.Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            Assert.True(loginPayload.PassesValidation());
            request.SetPayload(loginPayload);
            response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Good username, bad password
            loginPayload.UserName = UserName;
            Assert.True(loginPayload.PassesValidation());
            request.SetPayload(loginPayload);
            response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Good username & password
            loginPayload.Password = Password;
            Assert.True(loginPayload.PassesValidation());
            request.SetPayload(loginPayload);
            response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            Assert.NotNull(response);
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.True(response.PassesValidation());
            JWTToken = response.Token;
            StoreSharedState();
        }

        [Fact, TestPriority(3)]
        public async Task CreateAPIKey()
        {
            Assert.NotNull(JWTToken);
            AuthDef authDef = new()
            {
                AuthType = AuthType.Bearer,
                Token = JWTToken
            };

            request = new Request(client, HttpMethod.Post, "/auth/createapikey", authDef);

            var createAPIKeyPayload = new Payloads.Auth.CreateAPIKey();
            createAPIKeyPayload.UserName = "ab";
            createAPIKeyPayload.Password = "cd";
            createAPIKeyPayload.ApplicationName = "Integration Test";
            createAPIKeyPayload.AllowWrites = false;
            Assert.False(createAPIKeyPayload.PassesValidation());

            // Intentional bad request
            request.SetPayload(createAPIKeyPayload);
            var response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Bad password
            createAPIKeyPayload.UserName = UserName;
            createAPIKeyPayload.Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            Assert.True(createAPIKeyPayload.PassesValidation());
            request.SetPayload(createAPIKeyPayload);
            response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Good username & password
            createAPIKeyPayload.Password = Password;
            Assert.True(createAPIKeyPayload.PassesValidation());
            request.SetPayload(createAPIKeyPayload);
            response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            Assert.NotNull(response);
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.True(response.PassesValidation());
            ReadOnlyAPIKey = response.APIKey;

            createAPIKeyPayload.AllowWrites = true;
            Assert.True(createAPIKeyPayload.PassesValidation());
            request.SetPayload(createAPIKeyPayload);
            response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            Assert.NotNull(response);
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.True(response.PassesValidation());
            WriteAPIKey = response.APIKey;
            StoreSharedState();
        }

        [Fact, TestPriority(4)]
        public async Task LoginCheck()
        {
            // Test JWTToken
            Assert.NotNull(JWTToken);
            AuthDef authDef = new()
            {
                AuthType = AuthType.Bearer,
                Token = JWTToken
            };
            request = new Request(client, HttpMethod.Get, "/auth", authDef);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Test APIKey
            Assert.NotEqual(Guid.Empty, ReadOnlyAPIKey);
            authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = ReadOnlyAPIKey.ToString("N")
            };
            request = new Request(client, HttpMethod.Get, "/auth", authDef);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Test not logged in
            request = new Request(client, HttpMethod.Get, "/auth");
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);
        }

        [Fact, TestPriority(5)]
        public async Task CreateAPIKeyWithReadOnlyAPIKey()
        {
            Assert.NotEqual(Guid.Empty, ReadOnlyAPIKey);
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = ReadOnlyAPIKey.ToString("N")
            };

            request = new Request(client, HttpMethod.Post, "/auth/createapikey", authDef);

            var createAPIKeyPayload = new Payloads.Auth.CreateAPIKey();
            createAPIKeyPayload.UserName = UserName;
            createAPIKeyPayload.Password = Password;
            createAPIKeyPayload.ApplicationName = "This Shouldn't Work";
            createAPIKeyPayload.AllowWrites = true;
            Assert.True(createAPIKeyPayload.PassesValidation());

            // This should not work because the APIKey we're using for auth doesn't have write
            var response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);
        }

        [Fact, TestPriority(6)]
        public async Task CreateTonsOfAPIKeys()
        {
            Assert.NotEqual(Guid.Empty, WriteAPIKey);
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            request = new Request(client, HttpMethod.Post, "/auth/createapikey", authDef);

            var createAPIKeyPayload = new Payloads.Auth.CreateAPIKey();
            createAPIKeyPayload.UserName = UserName;
            createAPIKeyPayload.Password = Password;
            createAPIKeyPayload.AllowWrites = false;
            Assert.True(createAPIKeyPayload.PassesValidation());

            // Keep creating APIKeys until we're no longer allowed to do so

            int APIKeyCount = 1;
            Payloads.Auth.CreateAPIKeyResponse? response;
            do
            {
                createAPIKeyPayload.ApplicationName = $"Iteration {APIKeyCount}";
                request.SetPayload(createAPIKeyPayload);
                response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
                Assert.True(HttpStatusCode.OK == request.StatusCode || HttpStatusCode.NotAcceptable == request.StatusCode);
            } while (HttpStatusCode.OK == request.StatusCode);
        }

        [Fact, TestPriority(7)]
        public async Task ListAPIKeys()
        {
            Assert.NotEqual(Guid.Empty, WriteAPIKey);
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            request = new Request(client, HttpMethod.Get, "/auth/listapikeys", authDef);
            var response = await request.GetResponseAsync<List<DB.Model.APIKey>>();
            Assert.NotNull(response);
            Assert.Equal(Controllers.AuthController.MaxAPIKeyCount, response.Count);
        }

        [Fact, TestPriority(8)]
        public async Task RevokeAPIKey()
        {
            Assert.NotEqual(Guid.Empty, ReadOnlyAPIKey);
            Assert.NotEqual(Guid.Empty, WriteAPIKey);
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = ReadOnlyAPIKey.ToString("N")
            };

            request = new Request(client, HttpMethod.Post, "/auth/revokeapikey", authDef);

            var payload = new Payloads.Auth.RevokeAPIKey();
            payload.UserName = UserName;
            payload.Password = Password;
            payload.APIKeyToRevoke = ReadOnlyAPIKey;
            Assert.True(payload.PassesValidation());

            // Attempt to use read-only APIKey
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Set the correct APIKey now
            authDef.Token = WriteAPIKey.ToString("N");
            request = new Request(client, HttpMethod.Post, "/auth/revokeapikey", authDef);

            // Intentional bad request
            payload.UserName = "ab";
            Assert.False(payload.PassesValidation());
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Intentionally bad password
            payload.UserName = UserName;
            payload.Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            payload.APIKeyToRevoke = ReadOnlyAPIKey;
            Assert.True(payload.PassesValidation());
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Intentionally bad APIKey
            payload.Password = Password;
            payload.APIKeyToRevoke = Guid.NewGuid();
            Assert.True(payload.PassesValidation());
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NoContent, request.StatusCode);

            payload.APIKeyToRevoke = ReadOnlyAPIKey;
            Assert.True(payload.PassesValidation());
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            ReadOnlyAPIKey = Guid.Empty;
            StoreSharedState();
        }

        [Fact, TestPriority(9)]
        public async Task ChangePassword()
        {
            Assert.NotEqual(Guid.Empty, WriteAPIKey);
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            var newPassword = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);

            request = new Request(client, HttpMethod.Post, "/auth/changepassword", authDef);

            var changePasswordPayload = new Payloads.Auth.ChangePassword();
            changePasswordPayload.OldPassword = "ab";
            changePasswordPayload.NewPassword = newPassword;
            Assert.False(changePasswordPayload.PassesValidation());

            // Bad request
            request.SetPayload(changePasswordPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Bad password
            changePasswordPayload.OldPassword = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            Assert.True(changePasswordPayload.PassesValidation());
            request.SetPayload(changePasswordPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Success
            changePasswordPayload.OldPassword = Password;
            Assert.True(changePasswordPayload.PassesValidation());
            request.SetPayload(changePasswordPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Password = newPassword;
            StoreSharedState();
        }

        [Fact, TestPriority(10)]
        public async Task DeleteAccount()
        {
            Assert.NotEqual(Guid.Empty, WriteAPIKey);
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            // Bad request (password must be >= 3 chars)
            var deletePayload = new Payloads.Auth.DeleteAccount { Password = "f" };
            Assert.False(deletePayload.PassesValidation());
            request = new Request(client, HttpMethod.Post, "/auth/deleteaccount", authDef);
            request.SetPayload(deletePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Bad password
            deletePayload.Password = "abcdefghij";
            request.SetPayload(deletePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Ok
            deletePayload.Password = Password;
            request.SetPayload(deletePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
        }
    }
}