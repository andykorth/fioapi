﻿using FIOAPI.Payloads.Group;

namespace FIOAPI.Integration.Test.Tests
{
    public class Group : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        public Group(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact]
        public async Task Create()
        {
            var accounts = TestUserAccount.MakeAccounts(5);

            // BadRequest
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = -1,
                GroupName = "AB",
            };
            Assert.False(createPayload.PassesValidation());
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Group name too large
            createPayload.RequestedId = 1234;
            createPayload.GroupName = "1234567890ABCDEFG";
            Assert.True(createPayload.GroupName.Length > 16);
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Success
            createPayload.RequestedId = 1234;
            createPayload.GroupName = "API Test";
            Assert.True(createPayload.PassesValidation());
            request.SetPayload(createPayload);
            var createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createRes);
            Assert.Equal(1234, createRes.GroupId);

            // Same Id specified
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotAcceptable, request.StatusCode);

            // Success with generated id
            createPayload.RequestedId = 0;
            Assert.True(createPayload.PassesValidation());
            request.SetPayload(createPayload);
            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createRes);
            Assert.InRange(createRes.GroupId, 1, DB.Model.Group.LargestGroupId);

            // Success with user invites.  Intentional duplicates (should be removed)
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[0].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[0].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[1].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[1].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[2].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[3].DisplayUserName,
                Admin = true
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[4].DisplayUserName,
                Admin = true
            });
            Assert.True(createPayload.PassesValidation());
            request.SetPayload(createPayload);
            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createRes);
            Assert.InRange(createRes.GroupId, 1, DB.Model.Group.LargestGroupId);

            var groupId = createRes.GroupId;
            
            // At this point, we've created 3 groups.  Create GroupController.MaxGroupCount - 3 more groups
            for (int i = 0; i < Controllers.GroupController.MaxGroupCount - 3; ++i)
            {
                createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
                Assert.Equal(HttpStatusCode.OK, request.StatusCode);
                Assert.NotNull(createRes);
                Assert.InRange(createRes.GroupId, 1, DB.Model.Group.LargestGroupId);
            }

            // Attempt to create one more--this should faild
            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.Equal(HttpStatusCode.NotAcceptable, request.StatusCode);

            request = new Request(client, HttpMethod.Get, $"/group/{groupId}", accounts[0].GetReadDef());
            var groupRes = await request.GetResponseAsync<DB.Model.Group>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(groupRes);
            Assert.True(groupRes.PendingInvites.Any());
        }

        [Fact]
        public async Task Delete()
        {
            var account = TestUserAccount.MakeAccount();
            var ReadDef = account.GetReadDef();
            var WriteDef = account.GetWriteDef();

            // Create a group
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Delete Test"
            };
            Assert.True(createPayload.PassesValidation());
            request = new Request(client, HttpMethod.Post, "/group/create", WriteDef);
            request.SetPayload(createPayload);
            var createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>(); 
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createRes);
            Assert.InRange(createRes.GroupId, 1, DB.Model.Group.LargestGroupId);

            // Try to use a read def
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createRes.GroupId}", ReadDef);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Try to specify a bad id
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{DB.Model.Group.LargestGroupId + 2}", WriteDef);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Try to specify an id that we don't own
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createRes.GroupId + 1}", WriteDef);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Actually delete the correct group
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createRes.GroupId}", WriteDef);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Try to delete again and fail
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);
        }

        [Fact]
        public async Task List()
        {
            var account = TestUserAccount.MakeAccount();
            var ReadDef = account.GetReadDef();
            var WriteDef = account.GetWriteDef();

            request = new Request(client, HttpMethod.Get, "/group/list", ReadDef);
            var listResult = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.NotNull(listResult);
            Assert.Empty(listResult);

            // Create GroupController.MaxGroupCount groups
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Test"
            };
            request = new Request(client, HttpMethod.Post, "/group/create", WriteDef);
            request.SetPayload(createPayload);
            for(int i = 0; i < Controllers.GroupController.MaxGroupCount; ++i)
            {
                await request.GetNoResultResponseAsync();
                Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            }

            // Make sure we have MaxGroupCount entries
            request = new Request(client, HttpMethod.Get, "/group/list", ReadDef);
            listResult = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.NotNull(listResult);
            Assert.NotEmpty(listResult);
            Assert.Equal(Controllers.GroupController.MaxGroupCount, listResult.Count);
        }

        [Fact]
        public async Task ListOwner()
        {
            var account = TestUserAccount.MakeAccount();
            var ReadDef = account.GetReadDef();
            var WriteDef = account.GetWriteDef();

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Test"
            };
            request = new Request(client, HttpMethod.Post, "/group/create", WriteDef);
            request.SetPayload(createPayload);
            var createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createRes);
            var groupId1 = createRes.GroupId;

            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createRes);
            var groupId2 = createRes.GroupId;

            request = new Request(client, HttpMethod.Get, "/group/list/owner", ReadDef);
            var listOwner = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(listOwner);
            Assert.Equal(2, listOwner.Count);
            Assert.Contains(listOwner, lo => lo.GroupId == groupId1);
            Assert.Contains(listOwner, lo => lo.GroupId == groupId2);
            Assert.Equal(2, listOwner.Count(lo => lo.GroupName == createPayload.GroupName));
        }

        [Fact]
        public async Task ListAdmin()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            int Group1Id = 0;
            int Group2Id = 0;

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Admin",
                Invites = new List<UserInvite>()
                {
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                        Admin = true
                    }
                }
            };
            
            // Create first group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            Group1Id = createResponse.GroupId;

            // Create second group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[1].GetWriteDef());
            request.SetPayload(createPayload);
            createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            Group2Id = createResponse.GroupId;

            // Accept first admin invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group1Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Accept second admin invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group2Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Make sure list admin returns 2 results
            request = new Request(client, HttpMethod.Get, "/group/list/admin", accounts[2].GetReadDef());
            var adminList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(adminList);
            Assert.Equal(2, adminList.Count);
        }

        [Fact]
        public async Task ListMember()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            int Group1Id = 0;
            int Group2Id = 0;

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Admin",
                Invites = new List<UserInvite>()
                {
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    }
                }
            };

            // Create first group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            Group1Id = createResponse.GroupId;

            // Create second group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[1].GetWriteDef());
            request.SetPayload(createPayload);
            createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            Group2Id = createResponse.GroupId;

            // Accept first invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group1Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Accept second invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group2Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Make sure member list returns 2 results
            request = new Request(client, HttpMethod.Get, "/group/list/member", accounts[2].GetReadDef());
            var adminList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(adminList);
            Assert.Equal(2, adminList.Count);
        }

        [Fact]
        public async Task InviteAcceptRejectList()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            int Group1Id;
            int Group2Id;

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Acc/Rej/List",
                Invites = new List<UserInvite>()
                {
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                        Admin = true
                    }
                }
            };

            // Create first group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            Group1Id = createResponse.GroupId;

            // Create second group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[1].GetWriteDef());
            request.SetPayload(createPayload);
            createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            Group2Id = createResponse.GroupId;

            // Verify we have 2 invites
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            var inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(inviteList);
            Assert.Equal(2, inviteList.Count);

            // Accept the first invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group1Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Verify we only have 1 invite
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(inviteList);
            Assert.Single(inviteList);
            Assert.Equal(Group2Id, inviteList[0].GroupId);

            // Reject the remaining invite
            request = new Request(client, HttpMethod.Put, $"group/invite/reject/{Group2Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(inviteList);
            Assert.Empty(inviteList);
        }

        [Fact]
        public async Task Invite()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Invite",
            };

            // Create the group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);

            // Invite self
            var invitePayload = new Invite()
            {
                GroupId = createResponse.GroupId,
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[0].UserName
                    }
                }
            };

            // Test wrong groupid
            var badInvitePayload = new Invite()
            {
                GroupId = createResponse.GroupId + 1,
                Invites = invitePayload.Invites
            };
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetWriteDef());
            request.SetPayload(badInvitePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Test read-only def
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetReadDef());
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Ensure we get BadRequest when inviting self
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetWriteDef());
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Add first user
            invitePayload.Invites.Add(new UserInvite { UserName = accounts[1].UserName, Admin = true });
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Try again, we should get BadRequest
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Have first user accept
            request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Should still fail, despite an accepted invite
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetWriteDef());
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Try to invite last user as admin (from an admin). This isn't allowed (BadRequest)
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[1].GetWriteDef());
            invitePayload.Invites.Add(new UserInvite { UserName = accounts[2].UserName, Admin = true });
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);
        }

        [Fact]
        public async Task Kick()
        {
            var accounts = TestUserAccount.MakeAccounts(6);

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Kick",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[3].UserName
                    },
                    new UserInvite
                    {
                        UserName = accounts[4].UserName
                    }
                }
            };

            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            
            // Have each user accept the invite
            for(int UserIdx = 1; UserIdx < 5; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            }

            // Test input validation - too large an int
            request = new Request(client, HttpMethod.Put, $"/group/kick/{DB.Model.Group.LargestGroupId + 1}/Foo", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Test input validation - too small a username
            request = new Request(client, HttpMethod.Put, $"/group/kick/1/AB", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // User not found
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/ABC", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Non-members return NotFound
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[3].UserName}", accounts[5].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Members can't kick
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[3].UserName}", accounts[4].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Can't kick the GroupOwner
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[0].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Can't kick another Admin
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[2].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Can't kick yourself
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[1].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Can kick member
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[4].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Owner can kick admin
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[1].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Kicked admin has no power now
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[3].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);
        }

        [Fact]
        public async Task Leave()
        {
            var accounts = TestUserAccount.MakeAccounts(3);
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Leave",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    },
                }
            };

            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);
            
            // Each user should accept
            for(int UserIdx = 1; UserIdx < 3; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            }

            // Specify an invite GroupId
            request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{DB.Model.Group.LargestGroupId + 1}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Try to leave w/o auth
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}");
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Try to leave with a read only key
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[2].GetReadDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Have the owner try to leave
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Have user leave
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Have the user try to leave again
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Have the admin leave
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Ensure there's zero admins and zero users
            request = new Request(client, HttpMethod.Get, $"/group/{createResponse.GroupId}", accounts[0].GetReadDef());
            var group = await request.GetResponseAsync<DB.Model.Group>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(group);
            Assert.Empty(group.Admins);
            Assert.Empty(group.Users);
        }

        [Fact]
        public async Task Promote()
        {
            var accounts = TestUserAccount.MakeAccounts(3);
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Leave",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    },
                }
            };
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);

            // Have each user accept
            for (int UserIdx = 1; UserIdx < 3; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            }

            // Try to promote unauth'd
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}");
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Try to use read auth
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}", accounts[0].GetReadDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Try to specify a GroupId we're not owner of
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId+1}/{accounts[2].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Specify a bad GroupId
            request = new Request(client, HttpMethod.Put, $"/group/promote/{DB.Model.Group.LargestGroupId+1}/{accounts[2].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Make sure admin can't promote another user
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Try to promote an existing admin
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[1].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Try to promote an non-existent user
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/FooBar", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Successfully promote
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Try again to make sure it returns BadRequest
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Make sure the group has 2 admins
            request = new Request(client, HttpMethod.Get, $"/group/{createResponse.GroupId}", accounts[0].GetReadDef());
            var group = await request.GetResponseAsync<DB.Model.Group>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(group);
            Assert.Equal(2, group.Admins.Count);
            Assert.Empty(group.Users);
        }

        [Fact]
        public async Task DeleteGroup()
        {
            var accounts = TestUserAccount.MakeAccounts(4);
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Leave",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    },
                    new UserInvite
                    {
                        UserName = accounts[3].UserName,
                    }
                }
            };
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(createResponse);

            // Have UserIdx 1 & 2 accept
            for (int UserIdx = 1; UserIdx < 3; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            }

            // Try to delete unauthorized
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}");
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Unauthorized, request.StatusCode);

            // Try to delete w/ a read def
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}", accounts[0].GetReadDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // Give a bad id
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{DB.Model.Group.LargestGroupId+1}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.BadRequest, request.StatusCode);

            // Have an admin try to delete the group
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // Have the owner actually delete the group
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);

            // Try to delete it again
            await request.GetNoResultResponseAsync();
            Assert.Equal(HttpStatusCode.NotFound, request.StatusCode);

            // We shouldn't be able to retrieve the group
            request = new Request(client, HttpMethod.Get, $"/group/{createResponse.GroupId}", accounts[0].GetWriteDef());
            var group = await request.GetResponseAsync<DB.Model.Group>();
            Assert.Equal(HttpStatusCode.Forbidden, request.StatusCode);

            // User 1 shouldn't be an admin for any group
            request = new Request(client, HttpMethod.Get, "/group/list/admin", accounts[1].GetReadDef());
            var adminList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(adminList);
            Assert.Empty(adminList);

            // User 2 shouldn't be a user for any group
            request = new Request(client, HttpMethod.Get, "/group/list/member", accounts[2].GetReadDef());
            var memberList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(memberList);
            Assert.Empty(memberList);

            // User 3 shouldn't have any invites
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            var inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.Equal(HttpStatusCode.OK, request.StatusCode);
            Assert.NotNull(inviteList);
            Assert.Empty(inviteList);
        }
    }
}
