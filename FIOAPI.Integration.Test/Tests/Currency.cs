﻿using FIOAPI.DB.Model;

namespace FIOAPI.Integration.Test.Tests
{
    [TestCaseOrderer("FIOAPI.Integration.Test.XUnitHelpers.PriorityOrderer", "FIOAPI.Integration.Test")]
    public class Currency : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        private string CurrencyTestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Currency", "MESG_FOREX_BROKER_DATA");
            }
        }

        public Currency(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact, TestPriority(0)]
        public async Task PutBrokerData()
        {
            var Account = TestUserAccount.MakeAccount();

            var TestFiles = Directory.GetFiles(CurrencyTestDataFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var brokerData = JsonSerializer.Deserialize<Payloads.Currency.MESG_FOREX_BROKER_DATA>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(brokerData);
            Assert.True(brokerData.PassesValidation());

            // Test not logged in & read-only api key
            request = new Request(client, HttpMethod.Put, "/fx");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // Test hydration timeout
            var prevMsgType = brokerData.payload.message.messageType;
            brokerData.payload.message.messageType = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/fx", Account.GetWriteDef());
            request.SetPayload(brokerData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            brokerData.payload.message.messageType = prevMsgType;

            // Test bad payload
            var prevId = brokerData.payload.message.payload.id;
            brokerData.payload.message.payload.id = "5";
            Assert.False(brokerData.PassesValidation());
            request.SetPayload(brokerData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            brokerData.payload.message.payload.id = prevId;

            // Success
            request.SetPayload(brokerData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Currency.MESG_FOREX_BROKER_DATA>(File.ReadAllText(file));
                Assert.NotNull(data);
                bool bDataPassesValidation = data.PassesValidation();
                Assert.True(bDataPassesValidation);
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task GetBrokerData()
        {
            // We should only have n*(n-1) total groupings
            int TotalFXTickers = Constants.ValidCurrencies.Count * (Constants.ValidCurrencies.Count - 1);

            request = new Request(client, HttpMethod.Get, "/fx?include_buys=false&include_sells=false");
            var results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.Equal(TotalFXTickers, results.Count);

            // Make sure with a plain query we don't have buys and sells
            results.ForEach(result =>
            {
                Assert.Empty(result.BuyOrders);
                Assert.Empty(result.SellOrders);
            });

            // Test including buys
            request = new Request(client, HttpMethod.Get, "/fx?include_sells=false");
            results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.Equal(TotalFXTickers, results.Count);

            bool HasAtLeastOneBuy = false;
            foreach (var result in results)
            {
                Assert.Empty(result.SellOrders);
                HasAtLeastOneBuy |= result.BuyOrders.Any();
            }
            Assert.True(HasAtLeastOneBuy);

            // Test including sells
            request = new Request(client, HttpMethod.Get, "/fx?include_buys=false");
            results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.Equal(TotalFXTickers, results.Count);

            bool HasAtLeastOneSell = false;
            foreach (var result in results)
            {
                Assert.Empty(result.BuyOrders);
                HasAtLeastOneSell |= result.SellOrders.Any();
            }
            Assert.True(HasAtLeastOneSell);

            // Test including both buys and sells
            request = new Request(client, HttpMethod.Get, "/fx");
            results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.Equal(TotalFXTickers, results.Count);

            HasAtLeastOneBuy = false;
            HasAtLeastOneSell = false;
            foreach (var result in results)
            {
                HasAtLeastOneBuy |= result.BuyOrders.Any();
                HasAtLeastOneSell |= result.SellOrders.Any();
            }
            Assert.True(HasAtLeastOneBuy);
            Assert.True(HasAtLeastOneSell);
        }
    }
}
