﻿namespace FIOAPI.Integration.Test.Tests
{
    [TestCaseOrderer("FIOAPI.Integration.Test.XUnitHelpers.PriorityOrderer", "FIOAPI.Integration.Test")]
    public class Chat : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        private string TestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Chat");
            }
        }

        public Chat(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact, TestPriority(0)]
        public async Task PutMesgChannelData()
        {
            var Account = TestUserAccount.MakeAccount();

            var ChannelPayloadsFolder = Path.Combine(TestDataFolder, "MESG_CHANNEL_DATA");
            var TestFiles = Directory.GetFiles(ChannelPayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var msgChannelData = JsonSerializer.Deserialize<Payloads.Chat.MESG_CHANNEL_DATA>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(msgChannelData);
            Assert.True(msgChannelData.PassesValidation());

            // Test not logged in & read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/channel");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // Test hydration timeout
            var prevDisplayName = msgChannelData.payload.message.payload.displayName;
            msgChannelData.payload.message.payload.displayName = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/channel", Account.GetWriteDef());
            request.SetPayload(msgChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            msgChannelData.payload.message.payload.displayName = prevDisplayName;

            // Test bad payload
            var prevChannelId = msgChannelData.payload.message.payload.channelId;
            msgChannelData.payload.message.payload.channelId = "5";
            Assert.False(msgChannelData.PassesValidation());
            request.SetPayload(msgChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            msgChannelData.payload.message.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(msgChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/channel", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.MESG_CHANNEL_DATA>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(0)]
        public async Task PutRootChannelData()
        {
            var Account = TestUserAccount.MakeAccount();

            var ChannelPayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_DATA");
            var TestFiles = Directory.GetFiles(ChannelPayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var rootChannelData = JsonSerializer.Deserialize<Payloads.Chat.ROOT_CHANNEL_DATA>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(rootChannelData);
            Assert.True(rootChannelData.PassesValidation());

            // Test not logged in
            request = new Request(client, HttpMethod.Put, "/chat/channel/root");
            request.SetPayload(rootChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/channel/root", Account.GetReadDef());
            request.SetPayload(rootChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevDisplayName = rootChannelData.payload.displayName;
            rootChannelData.payload.displayName = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/channel/root", Account.GetWriteDef());
            request.SetPayload(rootChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            rootChannelData.payload.displayName = prevDisplayName;

            // Test bad payload
            var prevChannelId = rootChannelData.payload.channelId;
            rootChannelData.payload.channelId = "5";
            Assert.False(rootChannelData.PassesValidation());
            request.SetPayload(rootChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            rootChannelData.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(rootChannelData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/channel/root", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.ROOT_CHANNEL_DATA>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task PutMessageAddedSelf()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "MESG_CHANNEL_MESSAGE_ADDED");
            var TestFiles = Directory.GetFiles(MessagePayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var firstMessageData = JsonSerializer.Deserialize<Payloads.Chat.MESG_CHANNEL_MESSAGE_ADDED>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(firstMessageData);
            Assert.True(firstMessageData.PassesValidation());

            request = new Request(client, HttpMethod.Put, "/chat/messageaddedself");
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/messageaddedself", Account.GetReadDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevMessage = firstMessageData.payload.message.payload.message;
            firstMessageData.payload.message.payload.message = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/messageaddedself", Account.GetWriteDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            firstMessageData.payload.message.payload.message = prevMessage;

            // Test bad payload
            var prevChannelId = firstMessageData.payload.message.payload.channelId;
            firstMessageData.payload.message.payload.channelId = "5";
            Assert.False(firstMessageData.PassesValidation());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            firstMessageData.payload.message.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/messageaddedself", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.MESG_CHANNEL_MESSAGE_ADDED>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task PutMessageAdded()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_MESSAGE_ADDED");
            var TestFiles = Directory.GetFiles(MessagePayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var firstMessageData = JsonSerializer.Deserialize<Payloads.Chat.ROOT_CHANNEL_MESSAGE_ADDED>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(firstMessageData);
            Assert.True(firstMessageData.PassesValidation());

            request = new Request(client, HttpMethod.Put, "/chat/messageadded");
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/messageadded", Account.GetReadDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevMessage = firstMessageData.payload.message;
            firstMessageData.payload.message = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/messageadded", Account.GetWriteDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            firstMessageData.payload.message = prevMessage;

            // Test bad payload
            var prevChannelId = firstMessageData.payload.channelId;
            firstMessageData.payload.channelId = "5";
            Assert.False(firstMessageData.PassesValidation());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            firstMessageData.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/messageadded", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.ROOT_CHANNEL_MESSAGE_ADDED>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task PutMessageList()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "MESG_CHANNEL_MESSAGE_LIST");
            var TestFiles = Directory.GetFiles(MessagePayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var firstMessageData = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_MESSAGE_LIST>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(firstMessageData);
            Assert.True(firstMessageData.PassesValidation());

            request = new Request(client, HttpMethod.Put, "/chat/messagelist");
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/messagedeleted", Account.GetReadDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevMessage = firstMessageData.payload.message.payload.messages[0].message;
            firstMessageData.payload.message.payload.messages[0].message = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/messagelist", Account.GetWriteDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            firstMessageData.payload.message.payload.messages[0].message = prevMessage;

            // Test bad payload
            var prevChannelId = firstMessageData.payload.message.payload.messages[0].channelId;
            firstMessageData.payload.message.payload.messages[0].channelId = "5";
            Assert.False(firstMessageData.PassesValidation());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            firstMessageData.payload.message.payload.messages[0].channelId = prevChannelId;

            // Success
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/messagelist", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_MESSAGE_LIST>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(2)]
        public async Task PutMessageDeleted()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_MESSAGE_DELETED");
            var TestFiles = Directory.GetFiles(MessagePayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var firstMessageData = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_MESSAGE_DELETED>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(firstMessageData);
            Assert.True(firstMessageData.PassesValidation());

            request = new Request(client, HttpMethod.Put, "/chat/messagedeleted");
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/messagedeleted", Account.GetReadDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevMessage = firstMessageData.payload.message;
            firstMessageData.payload.message = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/messagedeleted", Account.GetWriteDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            firstMessageData.payload.message = prevMessage;

            // Test bad payload
            var prevChannelId = firstMessageData.payload.channelId;
            firstMessageData.payload.channelId = "5";
            Assert.False(firstMessageData.PassesValidation());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            firstMessageData.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/messagedeleted", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_MESSAGE_DELETED>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(3)]
        public async Task PutUserJoined()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_USER_JOINED");
            var TestFiles = Directory.GetFiles(MessagePayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var firstMessageData = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_USER_JOINED>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(firstMessageData);
            Assert.True(firstMessageData.PassesValidation());

            request = new Request(client, HttpMethod.Put, "/chat/userjoined");
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/userjoined", Account.GetReadDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevMessageType = firstMessageData.messageType;
            firstMessageData.messageType = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/userjoined", Account.GetWriteDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            firstMessageData.messageType = prevMessageType;

            // Test bad payload
            var prevChannelId = firstMessageData.payload.channelId;
            firstMessageData.payload.channelId = "5";
            Assert.False(firstMessageData.PassesValidation());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            firstMessageData.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/userjoined", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_USER_JOINED>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(3)]
        public async Task PutUserLeft()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_USER_LEFT");
            var TestFiles = Directory.GetFiles(MessagePayloadsFolder).ToList();
            Assert.NotEmpty(TestFiles);

            var FirstTestFile = TestFiles.First();
            TestFiles = TestFiles.Skip(1).ToList();

            var firstMessageData = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_USER_LEFT>(File.ReadAllText(FirstTestFile));
            Assert.NotNull(firstMessageData);
            Assert.True(firstMessageData.PassesValidation());

            request = new Request(client, HttpMethod.Put, "/chat/userleft");
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Test read-only api key
            request = new Request(client, HttpMethod.Put, "/chat/userleft", Account.GetReadDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test hydration timeout
            var prevMessageType = firstMessageData.messageType;
            firstMessageData.messageType = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/chat/userleft", Account.GetWriteDef());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            firstMessageData.messageType = prevMessageType;

            // Test bad payload
            var prevChannelId = firstMessageData.payload.channelId;
            firstMessageData.payload.channelId = "5";
            Assert.False(firstMessageData.PassesValidation());
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            firstMessageData.payload.channelId = prevChannelId;

            // Success
            request.SetPayload(firstMessageData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            request = new Request(client, HttpMethod.Put, "/chat/userleft", Account.GetWriteDef());
            foreach (var file in TestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.Chat.CHANNEL_USER_LEFT>(File.ReadAllText(file));
                Assert.NotNull(data);
                Assert.True(data.PassesValidation());
                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(4)]
        public async Task GetChannelList()
        {
            // Force a bad request
            request = new Request(client, HttpMethod.Get, "/chat/list?types=abcdefghjijklmnopqrstuvwxyz");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Force a non-existent type
            request = new Request(client, HttpMethod.Get, "/chat/list?types=OMGWTF");
            var chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.NotNull(chatChannels);
            Assert.Empty(chatChannels);

            // Grab only public
            request = new Request(client, HttpMethod.Get, "/chat/list?types=pUbLiC");
            chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.NotNull(chatChannels);
            Assert.Equal(3, chatChannels.Count); // 3: Global, Help, UFO

            // Grab public and group
            request = new Request(client, HttpMethod.Get, "/chat/list?types=PUBLIC&types=GROUP");
            chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.NotNull(chatChannels);
            Assert.True(chatChannels.Count > 3);

            // Test no params
            request = new Request(client, HttpMethod.Get, "/chat/list");
            chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.NotNull(chatChannels);
            Assert.NotEmpty(chatChannels);
        }

        [Fact, TestPriority(4)]
        public async Task GetMessages()
        {
            request = new Request(client, HttpMethod.Get, "/chat/list?types=PUBLIC");
            var publicChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.NotNull(publicChannels);
            Assert.NotEmpty(publicChannels);

            Assert.True(publicChannels.Count == 3);
            var global = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Global"));
            var help = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Help"));
            var ufo = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Operations"));

            // Bad request testing
            request = new Request(client, HttpMethod.Get, "/chat/messages?top=1001");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, "/chat/messages?skip=-1");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, $"/chat/messages?channel_names={global.DisplayName}&channel_names={help.DisplayName}&channel_names={ufo.DisplayName}");
            var messages = await request.GetResponseAsync<List<DB.Model.ChatMessage>>();
            request.AssertOK();
            Assert.NotNull(messages);
            Assert.NotEmpty(messages);
            Assert.True(messages.Count <= 1000);
        }

        [Fact, TestPriority(4)]
        public async Task GetUserMessages()
        {
            request = new Request(client, HttpMethod.Get, "/chat/list?types=PUBLIC");
            var publicChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.NotNull(publicChannels);
            Assert.NotEmpty(publicChannels);

            Assert.True(publicChannels.Count == 3);
            var help = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Help"));

            // Bad request testing
            request = new Request(client, HttpMethod.Get, "/chat/user/Saganaki/1234");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, "/chat/user/S/00000000000000000000000000000001");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, $"/chat/user/Saganaki/{help.ChatChannelId}");
            var messages = await request.GetResponseAsync<List<DB.Model.ChatMessage>>();
            request.AssertOK();
            Assert.NotNull(messages);
            Assert.NotEmpty(messages);
            Assert.True(messages.Count <= 1000);
        }
    }
}
