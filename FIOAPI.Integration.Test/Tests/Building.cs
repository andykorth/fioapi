﻿using System.IO;

namespace FIOAPI.Integration.Test.Tests
{
    [TestCaseOrderer("FIOAPI.Integration.Test.XUnitHelpers.PriorityOrderer", "FIOAPI.Integration.Test")]
    public class Building : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        private string TestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Building", "MESG_WORLD_REACTOR_DATA");
            }
        }

        private List<string> TestFiles
        {
            get
            {
                if (!_TestFiles.Any())
                {
                    _TestFiles = Directory.GetFiles(TestDataFolder).ToList();
                }

                return _TestFiles;
            }
        }
        private List<string> _TestFiles = new List<string>();

        public Building(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact, TestPriority(0)]
        public async Task PutAll()
        {
            var account = TestUserAccount.MakeAccount();

            var TestFiles = Directory.GetFiles(TestDataFolder).ToList();

            Assert.NotEmpty(TestFiles);

            // Ensure we only allow logged in & write
            request = new Request(client, HttpMethod.Put, "/building");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // Payload has "Hydration Timeout [" somewhere in it
            var hydrationTimeoutReactorData = JsonSerializer.Deserialize<Payloads.Building.MESG_WORLD_REACTOR_DATA>(File.ReadAllText(TestFiles[0]));
            Assert.NotNull(hydrationTimeoutReactorData);
            Assert.NotNull(hydrationTimeoutReactorData.payload);
            Assert.NotNull(hydrationTimeoutReactorData.payload.message);
            Assert.NotNull(hydrationTimeoutReactorData.payload.message.payload);
            hydrationTimeoutReactorData.payload.message.payload.name = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/building", account.GetWriteDef());
            request.SetPayload(hydrationTimeoutReactorData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            // Success
            request.SetPayloadFromFile(TestFiles[0]);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            foreach (var TestFile in TestFiles.Skip(1))
            {
                request.SetPayloadFromFile(TestFile);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Fact, TestPriority(1)]
        public async Task GetAll()
        {
            request = new Request(client, HttpMethod.Get, "/building");
            var results = await request.GetResponseAsync<List<DB.Model.Building>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.NotEmpty(results);
            Assert.Equal(TestFiles.Count, results.Count);

            request = new Request(client, HttpMethod.Get, "/building?include_costs=false&include_recipes=false");
            results = await request.GetResponseAsync<List<DB.Model.Building>>();
            request.AssertOK();
            Assert.NotNull(results);
            Assert.NotEmpty(results);
            Assert.Equal(TestFiles.Count, results.Count);

            foreach (var result in results)
            {
                Assert.Empty(result.Costs);
                Assert.Empty(result.Recipes);
            }
        }
    }
}
