﻿using FIOAPI.DB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIOAPI.Integration.Test.Tests
{
    public class Material : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        private string MaterialTestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Material", "MESG_WORLD_MATERIAL_DATA");
            }
        }

        private List<string> MaterialTestFiles
        {
            get
            {
                if (!_MaterialTestFiles.Any())
                {
                    _MaterialTestFiles = Directory.GetFiles(MaterialTestDataFolder).ToList();
                }

                return _MaterialTestFiles;
            }
        }
        private List<string> _MaterialTestFiles = new();

        private string MaterialCategoryTestDataFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Material", "MESG_WORLD_MATERIAL_CATEGORIES");
            }
        }

        private List<string> MaterialCategoryTestFiles
        {
            get
            {
                if (!_MaterialCategoryTestFiles.Any())
                {
                    _MaterialCategoryTestFiles = Directory.GetFiles(MaterialCategoryTestDataFolder).ToList();
                }

                return _MaterialCategoryTestFiles;
            }
        }
        private List<string> _MaterialCategoryTestFiles = new();

        public Material(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact]
        public async Task MaterialTest()
        {
            var account = TestUserAccount.MakeAccount();
            Assert.NotEmpty(MaterialTestFiles);

            // Ensure we only allow logged in & write
            request = new Request(client, HttpMethod.Put, "/material");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // No results initially
            request = new Request(client, HttpMethod.Get, "/material");
            var materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.NotNull(materials);
            Assert.Empty(materials);

            // Ensure all of the test data goes through
            request = new Request(client, HttpMethod.Put, "/material", account.GetWriteDef());
            foreach(var mtf in MaterialTestFiles)
            {
                request.SetPayloadFromFile(mtf);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }

            // Make sure it works
            request = new Request(client, HttpMethod.Get, "/material");
            materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.NotNull(materials);
            Assert.NotEmpty(materials);

            // Test ticker
            request.EndPoint = "/material?ticker=RAT&ticker=H2O";
            materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.NotNull(materials);
            Assert.Equal(2, materials.Count);

            // Test combination
            request.EndPoint = "/material?ticker=RAT&name=water&id=b9640b0d66e7d0ca7e4d3132711c97fc&category=3f047ec3043bdd795fd7272d6be98799";
            materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.NotNull(materials);
            Assert.NotEmpty(materials);
        }

        [Fact]
        public async Task MaterialCategoryTest()
        {
            var account = TestUserAccount.MakeAccount();
            Assert.NotEmpty(MaterialCategoryTestFiles);

            // Ensure we only allow logged in & write
            request = new Request(client, HttpMethod.Put, "/material/category");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // No results initially
            request = new Request(client, HttpMethod.Get, "/material/categories");
            var categories = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.NotNull(categories);
            Assert.Empty(categories);

            // Ensure all of the test data goes through
            request = new Request(client, HttpMethod.Put, "/material/category", account.GetWriteDef());
            foreach (var mctf in MaterialCategoryTestFiles)
            {
                request.SetPayloadFromFile(mctf);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }

            // Make sure it works
            request = new Request(client, HttpMethod.Get, "/material/categories");
            categories = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.NotNull(categories);
            Assert.NotEmpty(categories);
        }
    }
}
