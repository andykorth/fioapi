﻿namespace FIOAPI.Integration.Test.Tests
{
    public class Admin : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        public Admin(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        [Fact]
        public async Task IsAdmin()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            // Not logged in
            request = new Request(client, HttpMethod.Get, "/admin");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();
            
            // User attempt
            request = new Request(client, HttpMethod.Get, "/admin", userAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Good
            request = new Request(client, HttpMethod.Get, "/admin", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Fact]
        public async Task IsUser()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var userAccountName = userAccount.UserName.ToUpper(); // Upper should be fine

            // Not logged in
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName}");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // User attempt
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName}", userAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // User account not found
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName + "X"}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();

            // User account found
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Fact]
        public async Task CreateAccount()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var createPayload = new Payloads.Admin.CreateAccount()
            {
                UserName = "AB",
                Password = "CD",
                Admin = true
            };
            Assert.False(createPayload.PassesValidation());

            // Not logged in
            request = new Request(client, HttpMethod.Post, "/admin/createaccount");
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Using admin read def
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetReadDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // User attempt
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", userAccount.GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Too small of username/password
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // New account creation
            createPayload.UserName = "ABC";
            createPayload.Password = "DEF";
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Confirm new account exists
            request = new Request(client, HttpMethod.Get, "/admin/isuser/ABC", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Existing account reset/upgrade
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());
            createPayload.UserName = userAccount.UserName.ToUpper();
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Make sure new useraccount is now admin
            request = new Request(client, HttpMethod.Get, "/admin", userAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Fact]
        public async Task DeleteAccount()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var createPayload = new Payloads.Admin.CreateAccount()
            {
                UserName = "DeleteTest",
                Password = "DeleteTest",
                Admin = true
            };
            Assert.True(createPayload.PassesValidation());

            // Create the account
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Not logged in
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // User account attempt
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}", userAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Admin read def attempt
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // User not found
            request = new Request(client, HttpMethod.Delete, "/admin/deleteaccount/UserNotFound", adminAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();

            // User deleted
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}", adminAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Ensure the user is no longer found
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{createPayload.UserName}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();
        }
    }
}
