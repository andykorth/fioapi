﻿using FIOAPI.Payloads.Global;

namespace FIOAPI.Integration.Test.Tests
{
    public class Global : IAssemblyFixture<FIOAPIWebApplicationFactory<Program>>
    {
        private FIOAPIWebApplicationFactory<Program> _factory;
        private readonly HttpClient client;

        private Request? request;

        public Global(FIOAPIWebApplicationFactory<Program> factory)
        {
            _factory = factory;
            client = _factory.CreateClient();
        }

        public string GlobalTestFolder
        {
            get
            {
                return Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Global");
            }
        }

        [Fact]
        public async Task Country()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var CountryRegistryFolder = Path.Combine(GlobalTestFolder, "MESG_COUNTRY_REGISTRY_COUNTRIES");
            var CountryRegistryTestData = Directory.GetFiles(CountryRegistryFolder).FirstOrDefault();
            Assert.NotNull(CountryRegistryTestData);
            Assert.True(File.Exists(CountryRegistryTestData));

            // Make sure we require auth and require write
            request = new Request(client, HttpMethod.Put, "/global/countries");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite(RequireAdmin: true);

            // Hydration timeout
            var testData = JsonSerializer.Deserialize<MESG_COUNTRY_REGISTRY_COUNTRIES>(File.ReadAllText(CountryRegistryTestData));
            Assert.NotNull(testData);
            Assert.NotNull(testData.payload);
            Assert.NotNull(testData.payload.message);
            Assert.NotNull(testData.payload.message.payload);
            Assert.True(testData.payload.message.payload.countries.Any());
            Assert.True(testData.PassesValidation());
            var previousName = testData.payload.message.payload.countries.First().name;
            testData.payload.message.payload.countries.First().name = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/global/countries", adminAccount.GetWriteDef());
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            // Bad request
            testData.payload.message.payload.countries.First().name = previousName;
            testData.payload.message.payload.countries.First().id += "test";
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test empty response
            request = new Request(client, HttpMethod.Get, "/global/countries");
            var countryList = await request.GetResponseAsync<List<DB.Model.Country>>();
            request.AssertOK();
            Assert.NotNull(countryList);
            Assert.Empty(countryList);

            // Successful put
            request = new Request(client, HttpMethod.Put, "/global/countries", adminAccount.GetWriteDef());
            request.SetPayloadFromFile(CountryRegistryTestData);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Successful retrieve
            request = new Request(client, HttpMethod.Get, "/global/countries");
            countryList = await request.GetResponseAsync<List<DB.Model.Country>>();
            request.AssertOK();
            Assert.NotNull(countryList);
            Assert.NotEmpty(countryList);
        }

        [Fact]
        public async Task SimulationData()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var SimulationDataFolder = Path.Combine(GlobalTestFolder, "MESG_SIMULATION_DATA");
            var SimulationTestDataFile = Directory.GetFiles(SimulationDataFolder).FirstOrDefault();
            Assert.NotNull(SimulationTestDataFile);
            Assert.True(File.Exists(SimulationTestDataFile));

            // Make sure we require auth and require write
            request = new Request(client, HttpMethod.Put, "/global/simulationdata");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite(RequireAdmin: true);

            // Test GET NoContent
            request = new Request(client, HttpMethod.Get, "/global/simulationdata");
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();

            // Success
            request = new Request(client, HttpMethod.Put, "/global/simulationdata", adminAccount.GetWriteDef());
            request.SetPayloadFromFile(SimulationTestDataFile);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Test GET Success
            request = new Request(client, HttpMethod.Get, "/global/simulationdata");
            var result = await request.GetResponseAsync<DB.Model.SimulationData>();
            request.AssertOK();
            Assert.NotNull(result);
        }

        [Fact]
        public async Task ComexExchanges()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var ComexExchangeDataFolder = Path.Combine(GlobalTestFolder, "PATH_commodityexchanges");
            var ComexExchangeDataFile = Directory.GetFiles(ComexExchangeDataFolder).FirstOrDefault();
            Assert.NotNull(ComexExchangeDataFile);
            Assert.True(File.Exists(ComexExchangeDataFile));

            // Make sure we require auth and require write
            request = new Request(client, HttpMethod.Put, "/global/comexexchanges");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite(RequireAdmin: true);

            // Hydration timeout
            var testData = JsonSerializer.Deserialize<PATH_COMMODITY_EXCHANGES>(File.ReadAllText(ComexExchangeDataFile));
            Assert.NotNull(testData);
            Assert.NotNull(testData.payload);
            Assert.NotNull(testData.payload.message);
            Assert.NotNull(testData.payload.message.payload);
            Assert.True(testData.payload.message.payload.body.Any());
            Assert.True(testData.PassesValidation());
            var previousName = testData.payload.message.payload.body.First().name;
            testData.payload.message.payload.body.First().name = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/global/comexexchanges", adminAccount.GetWriteDef());
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            // Test bad request
            testData.payload.message.payload.body.First().name = previousName;
            testData.payload.message.payload.body.First().id += "test";
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test empty response
            request = new Request(client, HttpMethod.Get, "/global/comexexchanges");
            var result = await request.GetResponseAsync<List<DB.Model.ComexExchange>>();
            request.AssertOK();
            Assert.NotNull(result);
            Assert.Empty(result);

            // Success PUT
            request = new Request(client, HttpMethod.Put, "/global/comexexchanges", adminAccount.GetWriteDef());
            request.SetPayloadFromFile(ComexExchangeDataFile);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Success retrieve
            request = new Request(client, HttpMethod.Get, "/global/comexexchanges");
            result = await request.GetResponseAsync<List<DB.Model.ComexExchange>>();
            request.AssertOK();
            Assert.NotNull(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public async Task Stations()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var StationsDataFolder = Path.Combine(GlobalTestFolder, "PATH_stations-_");
            var StationDataFiles = Directory.GetFiles(StationsDataFolder).ToList();
            Assert.NotNull(StationDataFiles);
            Assert.NotEmpty(StationDataFiles);

            StationDataFiles.ForEach(sd =>
            {
                Assert.True(File.Exists(sd));
            });

            var FirstStationDataFile = StationDataFiles.First();

            // Make sure we require auth and require write
            request = new Request(client, HttpMethod.Put, "/global/station");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite(RequireAdmin: true);

            // Hydration timeout
            var testData = JsonSerializer.Deserialize<PATH_STATION>(File.ReadAllText(FirstStationDataFile));
            Assert.NotNull(testData);
            Assert.NotNull(testData.payload);
            Assert.NotNull(testData.payload.message);
            Assert.NotNull(testData.payload.message.payload);
            Assert.True(testData.PassesValidation());
            var previousName = testData.payload.message.payload.body.name;
            testData.payload.message.payload.body.name = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/global/station", adminAccount.GetWriteDef());
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            // Test bad request
            testData.payload.message.payload.body.name = previousName;
            testData.payload.message.payload.body.id += "test";
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test empty response
            request = new Request(client, HttpMethod.Get, "/global/stations");
            var result = await request.GetResponseAsync<List<DB.Model.Station>>();
            request.AssertOK();
            Assert.NotNull(result);
            Assert.Empty(result);

            // Success PUT
            request = new Request(client, HttpMethod.Put, "/global/station", adminAccount.GetWriteDef());
            foreach (var StationDataFile in StationDataFiles)
            {
                request.SetPayloadFromFile(StationDataFile);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }

            // Success retrieve
            request = new Request(client, HttpMethod.Get, "/global/stations");
            result = await request.GetResponseAsync<List<DB.Model.Station>>();
            request.AssertOK();
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.Equal(StationDataFiles.Count, result.Count);
        }

        [Fact]
        public async Task WorkforceRequirements()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var WorkforceRequirementsFolder = Path.Combine(GlobalTestFolder, "MESG_WORKFORCE_WORKFORCES");
            var WorkforceRequirementsFile = Directory.GetFiles(WorkforceRequirementsFolder).FirstOrDefault();
            Assert.NotNull(WorkforceRequirementsFile);
            Assert.True(File.Exists(WorkforceRequirementsFile));

            // Make sure we require auth and admin write
            request = new Request(client, HttpMethod.Put, "/global/workforceneeds");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite(RequireAdmin: true);

            // Hydration timeout
            var testData = JsonSerializer.Deserialize<MESG_WORKFORCE_WORKFORCES>(File.ReadAllText(WorkforceRequirementsFile));
            Assert.NotNull(testData);
            Assert.NotNull(testData.payload);
            Assert.NotNull(testData.payload.message);
            Assert.NotNull(testData.payload.message.payload);
            Assert.NotNull(testData.payload.message.payload.workforces);
            Assert.NotEmpty(testData.payload.message.payload.workforces);
            var previousLevel = testData.payload.message.payload.workforces[0].level;
            testData.payload.message.payload.workforces[0].level = ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr;
            request = new Request(client, HttpMethod.Put, "/global/workforceneeds", adminAccount.GetWriteDef());
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertHydrationTimeout();

            // Test bad request
            testData.payload.message.payload.workforces[0].level = previousLevel;
            testData.payload.message.payload.siteId += "foo";
            request.SetPayload(testData);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test empty response
            request = new Request(client, HttpMethod.Get, "/global/workforceneeds");
            var result = await request.GetResponseAsync<List<DB.Model.WorkforceRequirement>>();
            request.AssertOK();
            Assert.NotNull(result);
            Assert.Empty(result);

            // Success PUT
            request = new Request(client, HttpMethod.Put, "/global/workforceneeds", adminAccount.GetWriteDef());
            request.SetPayloadFromFile(WorkforceRequirementsFile);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Success retrieve
            request = new Request(client, HttpMethod.Get, "/global/workforceneeds");
            result = await request.GetResponseAsync<List<DB.Model.WorkforceRequirement>>();
            request.AssertOK();
            Assert.NotNull(result);
            Assert.NotEmpty(result);
            Assert.NotEmpty(result[0].Needs);
        }
    }
}
